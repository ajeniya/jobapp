import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  constructor(private http: HttpClient) { }

  getSearch(pageNo?, pageSize?, name?, location?, category?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Company/searchcompanies?pageNo=${pageNo}&pageSize=${pageSize}&companyName=${name}&companyLocation=${location}&category=${category}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  searchJob(pageNo?, pageSize?, search?){
    return this.http
    .get<any>(`${environment.baseUrl}/Job/searchjobsList?pageNo=${pageNo}&pageSize=${pageSize}&occupation=${search.category}&type=${search.jobType}&companyType=${search.companysize}&title=${search.jobTitle}`, {})
    .pipe(map(result => {
        return result;
    }));
  }


}
