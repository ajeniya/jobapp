import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { SearchService } from './search.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  @Input() title: string;
  @Input() checkTabsType: string;
  navBar: void;
  subscriptions: Subscription[] = [];
  getJobSubscription: Subscription;
  searchJobSubscription: Subscription;
  isLoadingResults: boolean;
  searchFound: boolean;
  setPageIndex: number;
  recentJob: any[];
  pageSize = 10;
  loading = false;
  noRecord = false;
  isSearch = false;
  resultsLength: any;
  searchValue: any;
  clearValue = '';
  clear: boolean;
  companyId: any;
  clearSearch = false;

  public searchFieldJobType = {
    jobTitle: '',
    category: '',
    jobType: '',
    companysize: ''
  }
  jobLists: any;
  jobObject: any;

  constructor(public data: SearchService, private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  searchJobWithField() {
      this.isLoadingResults = true;
      this.searchFound = false;

      if(!this.setPageIndex) {
        this.setPageIndex = 1
      }

      this.getJobSubscription = this.data.searchJob(this.setPageIndex, this.pageSize, this.searchFieldJobType)
        .subscribe((data: any) => {
          this.sendJobNotification();
          this.jobObject = data;
        this.jobLists = data.result;
        this.resultsLength = data.paginationObject.totalRecord;
        this.searchFound = true;
        this.isLoadingResults = false;
        this.noRecord = true;
        this.clearSearch = true;
      }, (error) => {
        this.isLoadingResults = false;
        this.resultsLength = 0;
        this.toastr.error('No record found!', 'No Record!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      });

      this.subscriptions.push(this.getJobSubscription);
  }

  @Output() sendJobList: EventEmitter<any> = new EventEmitter();
  sendJobNotification() {
      this.sendJobList.emit(this.jobObject);
  }

  clearSearchJob() {
    this.searchFieldJobType = {
      jobTitle: '',
      category: '',
      jobType: '',
      companysize: ''
    }
    this.searchJobWithField();
  }

}
