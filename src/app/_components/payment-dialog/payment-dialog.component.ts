import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IPayPalConfig, ICreateOrderRequest } from 'ngx-paypal';
import { StripeToken } from "stripe-angular";
import { PaymentDialogService } from '../payment-dialog.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-dialog',
  templateUrl: './payment-dialog.component.html',
  styleUrls: ['./payment-dialog.component.scss']
})
export class PaymentDialogComponent implements OnInit {
  public payPalConfig?: IPayPalConfig;
  handler:any = null;

  title: string;
  message: string;
  showSuccess: boolean;
  amount: any;
  planName: any;
  planId: any;
  companyId: string;
  currencyCode: any;
  siteName: any;
  paypalClientId: any;
  constructor(public dialogRef: MatDialogRef<PaymentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private toastr: ToastrService,
              public DataServices: PaymentDialogService) {
                this.amount = data.amount_number;
                this.planName = data.plan_name;
                this.planId = data.plan_id;
               }

  ngOnInit() {
    this.setCompanyId();
    this.getAdminSetting();
    this.getPaymentGatewaySetting();
  }

  setCompanyId() {
    this.companyId = localStorage.getItem('companyId');
    console.log(this.companyId);
  }

  getAdminSetting() {
    this.DataServices.getSetting().subscribe((data) => {
      this.currencyCode = data.currencyCode;
      this.siteName = data.siteName;
    })
  }

  getPaymentGatewaySetting() {
    this.DataServices.getPaymentSetting().subscribe((data) => {
      const decoded = JSON.parse(JSON.stringify(data));
      const getPaypalDecoded = JSON.parse(decoded.paypal);
      this.paypalClientId = getPaypalDecoded.paypalClentId;

      this.initConfig();
    })
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  private initConfig(): void {
    this.payPalConfig = {
    currency: this.currencyCode,
    clientId: this.paypalClientId,
    createOrderOnClient: (data) => <ICreateOrderRequest>{
      intent: 'CAPTURE',
      purchase_units: [
        {
          amount: {
            currency_code: this.currencyCode,
            value: this.amount,
            breakdown: {
              item_total: {
                currency_code: this.currencyCode,
                value: this.amount
              }
            }
          },
          items: [
            {
              name: this.siteName,
              quantity: '1',
              category: 'DIGITAL_GOODS',
              unit_amount: {
                currency_code: this.currencyCode,
                value: this.amount,
              },
            }
          ]
        }
      ]
    },
    advanced: {
      commit: 'true'
    },
    style: {
      label: 'paypal',
      layout: 'vertical'
    },
    onApprove: (data, actions) => {
      actions.order.get().then(details => {
        // console.log(details);
      });
    },
    onClientAuthorization: (data) => {
      this.showSuccess = true;

      const paymentDetails = {
        'planId': this.planId,
        'companyId': this.companyId,
        'gatewayId': data.id,
        'receiptNo': data.payer
      }

      this.DataServices.addTransaction(paymentDetails).subscribe((data) => {
        this.toastr.success('Payment successful', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });

      });

    },
    onCancel: (data, actions) => {
    },
    onError: err => {
        this.toastr.error('Unable to process Payment try again', 'Error!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
    },
    onClick: (data, actions) => {
    },
  };
 }
}

