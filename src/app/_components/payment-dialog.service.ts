import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class PaymentDialogService {

  constructor(private http: HttpClient) { }

  addTransaction(formData){
    return this.http.post<any>(`${environment.baseUrl}/Transaction/addtransaction`,
    {
      'planId': formData.planId,
      'companyId': formData.companyId,
      'expiredDate': formData.expiredDate,
      'gatewayId': formData.gatewayId,
      'receiptNo': formData.receiptNo
    }).pipe(map(result => {
            return result;
      }));
  }

  getSetting() {
    return this.http.post<any>(`${environment.baseUrl}/SuperAdmin/getsitesetting`, {})
        .pipe(map(setting => {
            return setting;
        }));
  }

  getPaymentSetting() {
    return this.http.post<any>(`${environment.baseUrl}/Payment/getpaymentsetting`, {})
    .pipe(map(setting => {
        return setting;
    }));
   }
}
