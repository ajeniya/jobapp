import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

export function adminRole(router: Router, state: RouterStateSnapshot) {
  const getLoginDetails = JSON.parse(localStorage.getItem('currentUser'));
  if (getLoginDetails.account_type === 1) {
    return true
  } else {
    console.log(this.router.url);
    return false
  }
}


export function companyRole(router: Router, state: RouterStateSnapshot) {
  const getLoginDetails = JSON.parse(localStorage.getItem('currentUser'));
  if (getLoginDetails.account_type === 2) {
    return true
  } else {
    console.log(this.router.url);
    return false
  }
}


export function userRole(router: Router, state: RouterStateSnapshot) {
  const getLoginDetails = JSON.parse(localStorage.getItem('currentUser'));
  if (getLoginDetails.account_type === 3) {
    return true
  } else {
    console.log(this.router.url);
    return false
  }
}
