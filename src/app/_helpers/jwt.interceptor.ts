import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { AuthenticationService } from '../services/authentication.service';
import { SpinnerService } from '../services/spinnerService';
import { tap } from 'rxjs/internal/operators/tap';
import { CheckConnectionService } from '../services/check-connection.service';
import { ToastrService } from 'ngx-toastr';
import { delay } from 'rxjs/internal/operators/delay';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService,
      private network: CheckConnectionService,
      private toastr: ToastrService,
      private spinnerService: SpinnerService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      // if(this.network.online == false) {
      //   this.toastr.error('Connection error!', 'Connection!', {
      //     enableHtml: true,
      //     closeButton: true,
      //     timeOut: 3000
      //   });
      // }
        this.spinnerService.show();
        // add auth header with jwt if user is logged in and request is to the api url
        const currentUser = this.authenticationService.currentUserValue;
        const isLoggedIn = currentUser && currentUser.data.secure;
        const isApiUrl = request.url.startsWith(environment.baseUrl);
         if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    token: `${currentUser.data.secure}`
                }
            });
        }
        return next
            .handle(request)
            .pipe(
                tap((event: HttpEvent<any>) => {
                    if (event instanceof HttpResponse) {
                        this.spinnerService.hide();
                    }
                }, (error) => {
                    this.spinnerService.hide();
                })
            );

        // return next.handle(request);
    }
}
