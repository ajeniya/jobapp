import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { NgxCoolDialogsModule } from 'ngx-cool-dialogs';
import { HeaderComponent } from '../ui/header/header.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { AutoLogoutComponent } from '../auto-logout/auto-logout.component';

@NgModule({
  declarations: [
    HeaderComponent,
    AutoLogoutComponent
  ],
  exports: [
    HeaderComponent,
    AutoLogoutComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    NgSelectModule,
    FormsModule,
    NgxSmartModalModule,
    RichTextEditorAllModule,
    ScrollToModule.forRoot(),
    NgxCoolDialogsModule.forRoot(),
    PaginationModule.forRoot()
  ]
})
export class SharedModule { }
