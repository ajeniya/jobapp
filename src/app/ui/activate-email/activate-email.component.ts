import { Component, OnInit } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-activate-email',
  templateUrl: './activate-email.component.html',
  styleUrls: ['./activate-email.component.scss']
})
export class ActivateEmailComponent implements OnInit {
  navBar: void;
  isLoadingResults: boolean;
  activated: boolean;

  constructor(public eventServices: EventServicesService,
    public authServices: AuthenticationService,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);

    this.route.queryParams
      .subscribe(params => {
        this.isLoadingResults = true;
        const userActivateEmail = params.usid;
        this.authServices.activateEmail(userActivateEmail).subscribe((data) => {
          this.activated = true;
        }, (error) => {
          this.activated = false;
        })
      })
  }

}
