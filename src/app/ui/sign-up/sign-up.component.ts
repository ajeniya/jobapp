import { Component, OnInit, EventEmitter, OnDestroy } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { slideInLeft } from 'ng-animate';
import { CountryService } from './../../services/country.service';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Observable, Subscription } from 'rxjs';
import { DataService } from 'src/app/services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MainSettingService, SettingData } from 'src/app/admin/setting/main-setting/main-setting.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
  animations: [
    trigger('slideInLeft', [transition('* => *', useAnimation(slideInLeft, {
      // Set the duration to 5seconds and delay to 2seconds
      params: { timing: 0.5, delay: 1 }
    }))])
  ],
})

export class SignUpComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  signUpSubscription: Subscription;
  slideInLeft: any;
  step: number;
  selectedCountries: any;
  shouldShow = true;
  hide = true;
  onselectUser = true;
  onselectRecuiter = false;
  navBar: any;
  submitted = false;
  countries: Subscription;
  userForm: FormGroup;
  loading = false;
  isPasswordVisible: boolean;
  error: any;
  returnUrl: any;
  getSettingSubscription: Subscription;
  settingData: SettingData;

  constructor( public countryServices: CountryService,
               public eventServices: EventServicesService,
               private router: Router, public setting: MainSettingService,
               private route: ActivatedRoute,
               private toastr: ToastrService,
               private authenticationService: AuthenticationService,
               private formBuilder: FormBuilder,
               public dataServices: DataService) {}
  userModel: any = {};
  companyModel: any = {};

  ngOnInit() {
    this.step = 1;
    this.setForm();
    this.getCountry();
    this.navBar = this.eventServices.navchange.emit(false);
    this.userForm.controls['accountType'].setValue(3);
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    this.getAdminSetting();
  }

  setForm() {
    this.userForm = this.formBuilder.group({
      fulllName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      accountType: ['', Validators.required]
    });
  }


  // convenience getter for easy access to form fields
  get u() { return this.userForm.controls; }

  getCountry() {
    this.countries = this.dataServices.getCountries().subscribe((data) => {
      this.selectedCountries = Object.assign([], data[0]);
    });
    this.subscriptions.push(this.countries);
  }

  getAdminSetting() {
    this.getSettingSubscription = this.setting.getSetting().subscribe((data: SettingData) => {
      this.settingData = data;
    });

    this.subscriptions.push(this.getSettingSubscription);
  }

  onSelectUser() {
    this.onselectUser = true;
    this.onselectRecuiter = false;
    this.userForm.controls['accountType'].setValue(3);
  }

  onSelectRecruiter() {
    this.onselectRecuiter = true;
    this.onselectUser = false;
    this.userForm.controls['accountType'].setValue(2);
  }

  onSubmit() {
    this.submitted = true;
        // stop here if form is invalid
        if (this.userForm.invalid) {
            return;
        }
        this.loading = true;
        this.submitted = true;
        this.signUpSubscription = this.authenticationService.signUp(
          this.userForm.value.email,
          this.userForm.value.password,
          this.userForm.value.fulllName,
          this.userForm.value.accountType
          ).subscribe(
            data => {
              this.authenticationService.signIn(this.userForm.value.email, this.userForm.value.password)
                .subscribe((data) => {
                  this.router.navigate([this.returnUrl]);
                });
            }, error => {
              if(error == 'email_exist') {
                this.error = error;
                this.toastr.error('There is an account associated with this email!', 'Error!', {
                  enableHtml: true,
                  closeButton: true,
                  timeOut: 3000
                });
                this.loading = false;
                return;
              }
                this.error = error;
                this.toastr.error('Incorrect email or password!', 'Error!', {
                  enableHtml: true,
                  closeButton: true,
                  timeOut: 3000
                });
                this.loading = false;
        });

        this.subscriptions.push(this.signUpSubscription);
  }


  onSubmitCompany() {
    this.submitted = true;
        // stop here if form is invalid
        if (this.userForm.invalid) {
            return;
        }
        this.loading = true;
        this.submitted = true;
        this.signUpSubscription = this.authenticationService.onCompanySignUp(
          this.userForm.value.email,
          this.userForm.value.password,
          this.userForm.value.fulllName,
          this.userForm.value.accountType
          ).subscribe(
            data => {
              this.authenticationService.signIn(this.userForm.value.email, this.userForm.value.password)
                .subscribe((data) => {
                  this.router.navigate(['/company/create']);
                });
            }, error => {
              if(error == 'email_exist') {
                this.error = error;
                this.toastr.error('There is an account associated with this email!', 'Error!', {
                  enableHtml: true,
                  closeButton: true,
                  timeOut: 3000
                });
                this.loading = false;
                return;
              }
                this.error = error;
                this.toastr.error('Incorrect email or password!', 'Error!', {
                  enableHtml: true,
                  closeButton: true,
                  timeOut: 3000
                });
                this.loading = false;
        });

        this.subscriptions.push(this.signUpSubscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
