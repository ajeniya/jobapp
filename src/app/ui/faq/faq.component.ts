import { Component, OnInit } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { FaqService } from './faq.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  navBar: void;
  faqList: any;

  constructor(public eventServices: EventServicesService,
    public data: FaqService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getFaq();
  }

  getFaq() {
    this.data.getFaqs().subscribe((data) => {
      this.faqList = data.result;
    })
  }

}
