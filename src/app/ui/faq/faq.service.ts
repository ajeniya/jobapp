import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(private http: HttpClient) { }

  getFaqs() {
    return this.http.get<any>
    (`${environment.baseUrl}/Faq/getfaqlist`, {})
    .pipe(map(result => {
      return result;
    }));
  }
}
