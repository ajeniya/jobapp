import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MainSettingService, SettingData } from 'src/app/admin/setting/main-setting/main-setting.service';
import { SocialService } from 'src/app/admin/setting/social/social.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  getSettingSubscription: Subscription;
  settingData: any;
  socialData: any;
  year: any;

  constructor(private http: HttpClient,
    public social: SocialService,
    public setting: MainSettingService) { }

  ngOnInit() {
    this.getAdminSetting();
    this.getSocialLinks();
  }

  getAdminSetting() {
    this.getSettingSubscription = this.setting.getSetting().subscribe((data: SettingData) => {
      this.settingData = data;
      this.year = new Date().getFullYear();
    })
  }

  getSocialLinks() {
    this.getSettingSubscription = this.social.getSetting().subscribe((data: any) => {
      this.socialData = data;
    })
  }

}
