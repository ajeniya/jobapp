import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MainSettingService, SettingData } from 'src/app/admin/setting/main-setting/main-setting.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  getSettingSubscription: Subscription;
  model: any = {};
  shouldShow = true;
  onselectUser = true;
  onselectRecuiter = false;
  isLoadingResults: boolean;
  reset: boolean;
  param: string;
  isPasswordVisible: boolean;
  loading: boolean;
  settingData: any;

  constructor(public eventServices: EventServicesService,
    public authServices: AuthenticationService, public setting: MainSettingService,
    private toastr: ToastrService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        this.isLoadingResults = true;
        this.param = params.usid;
      });

    this.getAdminSetting();
  }

  onSubmit() {
    this.loading = true;
    this.authServices.resetPassword(this.model, this.param).subscribe((data) => {
      this.reset = true;
      this.loading = false;
      this.router.navigate(['/sign-in']);
      this.toastr.success('Password reset !', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    }, (error) => {
      this.reset = false;
      this.loading = false;
      this.toastr.error('Unable to reset password Try Again!', 'Failed!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    })
  }

  getAdminSetting() {
    this.getSettingSubscription = this.setting.getSetting().subscribe((data: SettingData) => {
      this.settingData = data;
    });

    this.subscriptions.push(this.getSettingSubscription);
  }

  onSelectUser() {
    this.onselectUser = true;
    this.onselectRecuiter = false;
  }

  onSelectRecruiter() {
    this.onselectRecuiter = true;
    this.onselectUser = false;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
