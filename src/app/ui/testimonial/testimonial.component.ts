import { HttpClient } from '@angular/common/http';
import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { MockDataService } from '../home/mock-data.service';

@Component({
  selector: 'app-testimonial',
  templateUrl: './testimonial.component.html',
  styleUrls: ['./testimonial.component.scss']
})
export class TestimonialComponent implements OnInit {
  testimonylist: void;
  testimonylistOne: any;
  testimonylistTwo: any;
  testimonylistThree: any;
  testimonylistFour: any;
  testimonylistFive: any;

  constructor(@Inject(DOCUMENT) private document: Document,
  public data: MockDataService,
  private http: HttpClient) { }

  ngOnInit(): void {

    this.getTestimony();
    this.loadTestimonial();
  }

  loadTestimonial() {
    var	testim = this.document.getElementById("testim"),
		testimDots = Array.prototype.slice.call(this.document.getElementById("testim-dots").children),
    testimContent = Array.prototype.slice.call(this.document.getElementById("testim-content").children),
    testimLeftArrow = this.document.getElementById("left-arrow"),
    testimRightArrow = this.document.getElementById("right-arrow"),
    testimSpeed = 4500,
    currentSlide = 0,
    currentActive = 0,
    testimTimer,
		touchStartPos,
		touchEndPos,
		touchPosDiff,
		ignoreTouch = 30;

    window.onload = function() {

        // Testim Script
        function playSlide(slide) {
            for (var k = 0; k < testimDots.length; k++) {
                testimContent[k].classList.remove("active");
                testimContent[k].classList.remove("inactive");
                testimDots[k].classList.remove("active");
            }

            if (slide < 0) {
                slide = currentSlide = testimContent.length-1;
            }

            if (slide > testimContent.length - 1) {
                slide = currentSlide = 0;
            }

            if (currentActive != currentSlide) {
                testimContent[currentActive].classList.add("inactive");
            }
            testimContent[slide].classList.add("active");
            testimDots[slide].classList.add("active");

            currentActive = currentSlide;

            clearTimeout(testimTimer);
            testimTimer = setTimeout(function() {
                playSlide(currentSlide += 1);
            }, testimSpeed)
        }

        testimLeftArrow.addEventListener("click", function() {
            playSlide(currentSlide -= 1);
        })

        testimRightArrow.addEventListener("click", function() {
            playSlide(currentSlide += 1);
        })

        for (var l = 0; l < testimDots.length; l++) {
            testimDots[l].addEventListener("click", function() {
                playSlide(currentSlide = testimDots.indexOf(this));
            })
        }

        playSlide(currentSlide);

        // keyboard shortcuts
        this.document.addEventListener("keyup", function(e) {
            switch (e.keyCode) {
                case 37:
                    testimLeftArrow.click();
                    break;

                case 39:
                    testimRightArrow.click();
                    break;

                case 39:
                    testimRightArrow.click();
                    break;

                default:
                    break;
            }
        })

        testim.addEventListener("touchstart", function(e) {
            touchStartPos = e.changedTouches[0].clientX;
        })

        testim.addEventListener("touchend", function(e) {
            touchEndPos = e.changedTouches[0].clientX;

            touchPosDiff = touchStartPos - touchEndPos;

            if (touchPosDiff > 0 + ignoreTouch) {
                testimLeftArrow.click();
            } else if (touchPosDiff < 0 - ignoreTouch) {
                testimRightArrow.click();
            } else {
              return;
            }

        })
    }
  }

  getTestimony() {
    this.data.getTestimony().subscribe((data) => {
      this.testimonylist = data;
      this.testimonylistOne = this.testimonylist[0];
      this.testimonylistTwo = this.testimonylist[1];
      this.testimonylistThree = this.testimonylist[2];
      this.testimonylistFour = this.testimonylist[3];
      this.testimonylistFive = this.testimonylist[4];
    })
  }

}
