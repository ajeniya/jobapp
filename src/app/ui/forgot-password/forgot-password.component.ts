import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { MainSettingService, SettingData } from 'src/app/admin/setting/main-setting/main-setting.service';
import { Subscription } from 'rxjs/internal/Subscription';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  getSettingSubscription: Subscription;
  model: any = {};
  shouldShow = true;
  onselectUser = true;
  onselectRecuiter = false;
  reset: boolean;
  loading: any;
  settingData: SettingData;

  constructor(public eventServices: EventServicesService,
    public authServices: AuthenticationService, public setting: MainSettingService,
    private toastr: ToastrService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.getAdminSetting();
  }

  onSubmit() {
    this.loading = true;
    this.authServices.forgotPassword(this.model).subscribe((data) => {
      this.loading = false;
      this.reset = true;
      this.toastr.success('Password reset mail as been send to your email!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    }, (error) => {
      this.reset = false;
      this.loading = false;
      this.toastr.error('Unable to send password reset mail Try Again!', 'Failed!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    })
  }

  getAdminSetting() {
    this.getSettingSubscription = this.setting.getSetting().subscribe((data: SettingData) => {
      this.settingData = data;
    });

    this.subscriptions.push(this.getSettingSubscription);
  }

  onSelectUser() {
    this.onselectUser = true;
    this.onselectRecuiter = false;
  }

  onSelectRecruiter() {
    this.onselectRecuiter = true;
    this.onselectUser = false;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
