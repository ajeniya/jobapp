import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(private toastr: ToastrService) {}

  ngOnInit() {
    this.toastr.success('Hello world!', 'Toastr fun!');
  }

}
