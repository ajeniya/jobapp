import { Subscription } from 'rxjs';
import { Component, OnInit, HostListener, ElementRef, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { MainSettingService, SettingData } from 'src/app/admin/setting/main-setting/main-setting.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  navbarStatus: boolean;
  getCompanyDetailSubscription: Subscription;
  navbarOpen: any;
  accountType: number;
  fullname: string;
  currentUser: any;
  userLoggedIn = false;
  isSocialLogin: boolean;
  companyId: string;
  getSettingSubscription: any;
  settingData: any;


  constructor(private eventServices: EventServicesService,
              private authService: SocialAuthService,
              private router: Router, public setting: MainSettingService,
              public authenticationService: AuthenticationService,
              private eRef: ElementRef) { }


  ngOnInit() {
    this.companyId = localStorage.getItem('companyId');
    this.eventServices.navchange.subscribe((data: any) => {
      this.navbarStatus = data;
    });

    this.authenticationService.currentUser.subscribe((currentUserObject: any) => {
      if( currentUserObject ) {
        this.userLoggedIn = true;
        const userObject = currentUserObject.data;
        this.accountType = userObject.account_type;
        this.fullname = userObject.full_name;
      }
    });


    this.authService.authState.subscribe((user) => {
      if(user) {
        this.isSocialLogin = true;
        this.userLoggedIn = true;
        this.accountType = 3;
        this.fullname = user.name;
      }
    });
    this.getAdminSetting();
  }

  getAdminSetting() {
    this.getSettingSubscription = this.setting.getSetting().subscribe((data: SettingData) => {
      this.settingData = data;
    })
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  mouseLeave() {
    this.navbarOpen = false;
  }

  @HostListener('document:click', ['$event'])
  handleOutsideClick(event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.navbarOpen = false;
    }
  }

  closeMenuOnclick() {
    this.navbarOpen = false;
  }

  mouseEnter() {
    this.navbarOpen = true;
  }

  logout() {
    this.authenticationService.logout();

    if(this.isSocialLogin) {
      this.authService.signOut();
      this.accountType = 4;
      this.userLoggedIn = false;
      this.fullname = '';
      this.router.navigate(['/sign-in']);
    }

    this.accountType = 4;
    this.userLoggedIn = false;
    this.fullname = '';
    this.router.navigate(['/sign-in']);
  }

}
