import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { first } from 'rxjs/internal/operators/first';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs/internal/Subscription';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { MainSettingService, SettingData } from 'src/app/admin/setting/main-setting/main-setting.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  signInSubscription: Subscription;
  model: any = {};
  navBar: any;
  loading = false;
  submitted: boolean;
  error: any;
  returnUrl: any;
  currentUser: any;
  user: SocialUser;
  signInWithGoogleSubscription: Subscription;
  getSettingSubscription: any;
  settingData: any;

  constructor(private eventServices: EventServicesService,
        private route: ActivatedRoute, public setting: MainSettingService,
        private router: Router, private toastr: ToastrService,
        private authService: SocialAuthService,
        private authenticationService: AuthenticationService) {
           // redirect to home if already logged in
          if (this.authenticationService.currentUserValue) {
            this.router.navigate(['/']);
          }
        }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(false);
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.getAdminSetting();
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    this.signInSubscription = this.authenticationService.signIn(this.model.email, this.model.password)
    .pipe(first()).subscribe(
        data => {
            if(data.data.account_type == 2) {
              if(data.data.verified == 1) {
                this.router.navigate([this.returnUrl]);
              } else {
                this.router.navigate(['/company/dashboard']);
              }
            }

            if(data.data.account_type == 3) {
              if(data.data.verified == 1) {
                this.router.navigate([this.returnUrl]);
              } else {
                this.router.navigate(['/user/setting']);
              }
            }

            if(data.data.account_type == 1) {
              this.router.navigate(['/admin']);
            }

        }, error => {
            this.error = error;
            this.toastr.error('Incorrect email or password!', 'Error!', {
              enableHtml: true,
              closeButton: true,
              timeOut: 3000
          });
            this.loading = false;
    });
    this.subscriptions.push(this.signInSubscription);
  }

  signInWithGoogle(): void {
    this.authenticationService.signWithGoogle();
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    this.authService.authState.subscribe((user) => {
      this.router.navigate([this.returnUrl]);
    });
  }

  getAdminSetting() {
    this.getSettingSubscription = this.setting.getSetting().subscribe((data: SettingData) => {
      this.settingData = data;
    });

    this.subscriptions.push(this.getSettingSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
