import { Component, OnInit } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss']
})
export class ConfirmEmailComponent implements OnInit {
  navBar: void;

  constructor(private eventServices: EventServicesService, public translate: TranslateService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
  }

}
