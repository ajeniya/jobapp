import { Component, OnInit } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';

import { TranslateService } from '@ngx-translate/core';
import { MockDataService } from './mock-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  navBar: any;
  public toggle: string = 'job';
  joblists: any;
  companyList: any;
  postArticle: any;
  model: any = {};
  featuredjobsList: any;
  featuredcompaniesList: any;

  constructor(public eventServices: EventServicesService,
    private route: ActivatedRoute, public router: Router,
    public data: MockDataService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.featuredcompanies();
    this.featuredjobs();
  }


  featuredjobs() {
    this.data.featuredjobs().subscribe((data) => {
      this.featuredjobsList = data;
    })
  }

  featuredcompanies() {
    this.data.featuredcompanies().subscribe((data) => {
      this.featuredcompaniesList = data;
    })
  }



  onSubmit() {
    this.router.navigate(['/job/list/'], { queryParams: { jobname: this.model.jobname, location: this.model.location} });
  }


}
