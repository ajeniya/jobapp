import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class MockDataService {

  constructor(private http: HttpClient) { }

  featuredjobs() {
    return this.http.get<any>
    (`${environment.baseUrl}/Job/featuredjobs`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  featuredcompanies() {
    return this.http.get<any>
    (`${environment.baseUrl}/Company/featuredcompanies`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getTestimony() {
    return this.http.get<any>
    (`${environment.baseUrl}/Testimonial/getTestimonialList`, {})
    .pipe(map(result => {
      return result;
    }));
  }
}
