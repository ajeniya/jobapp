import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import * as jwt_decode from "jwt-decode";
import { environment } from 'src/environments/environment';
import { SocialAuthService, GoogleLoginProvider, SocialUser } from 'angularx-social-login';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    public subscriptions: Subscription[] = [];
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(private http: HttpClient,  private authService: SocialAuthService,) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }

    signIn(email: string, password: string) {
        return this.http.post<any>(`${environment.baseUrl}/auth/doLogin`, { email: email, password: password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                localStorage.setItem('companyId', user.data.company_id);
                this.currentUserSubject.next(user);
                console.log(user.data.company_id);

                return user;
            }));
    }


    signWithGoogle() {
        this.authService.authState.subscribe((user) => {
          if(user) {
            const signInObject = {
              status: true,
              data: {
                full_name: user.name,
                account_type: 3,
                logged: true,
                secure: user.authToken,
                isSocialSignIn: true
              }
            }
            localStorage.setItem('currentUser', JSON.stringify(signInObject));
            this.currentUserSubject.next(signInObject);
          }
        });
    }


    getCompanyDetailFromQuery(userId){
      return this.http.get<any>(`${environment.baseUrl}/Company/getsingle?user_id=${userId}`, {})
      .pipe(map(result => {
              return result;
        }));
    }


    signUp(email: string, password: string, full_name: string, account_type: number ) {
      return this.http.post<any>(`${environment.baseUrl}/auth/actionCreate`,
           { email: email, password: password, full_name: full_name, account_type: account_type  })
            .pipe(map(signup => {
                return signup;
            }));
    }

    onCompanySignUp(email: string, password: string, full_name: string, account_type: number) {
      return this.http.post<any>(`${environment.baseUrl}/auth/actionCreateCompany`,
           { email: email, password: password, full_name: full_name, account_type: account_type  })
            .pipe(map(signup => {
                return signup;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        localStorage.removeItem('companyId');
        this.currentUserSubject.next(null);
    }


    getDecodedAccessToken(token: string): any {
      try {
          return jwt_decode(token);
      }
      catch(Error){
          return null;
      }
    }

    decodeToken() {
      const getLoginDetails = JSON.parse(localStorage.getItem('currentUser'));
      if(getLoginDetails) {
       return this.getDecodedAccessToken(getLoginDetails.data.secure);
      }
    }

    activateEmail(usid) {
      return this.http.get<any>(`${environment.baseUrl}/auth/activateEmail?usid=${usid}`, {})
      .pipe(map(result => {
              return result;
        }));
    }

    getUserdetails(email) {
      return this.http.get<any>(`${environment.baseUrl}/auth/getuserdetails?email=${email}`, {})
      .pipe(map(result => {
              return result;
        }));
    }

    resetPassword(form, param) {
      return this.http.post<any>(`${environment.baseUrl}/auth/actionChangePwd`,
           { password: form.password, user_id: param })
            .pipe(map(signup => {
                return signup;
            }));
    }

    forgotPassword(form) {
      return this.http.post<any>(`${environment.baseUrl}/auth/actionForgotPassword`,
           { email: form.email })
            .pipe(map(signup => {
                return signup;
            }));
    }

    ngOnDestroy() {
      this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    }
}
