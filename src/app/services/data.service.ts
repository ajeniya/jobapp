import { HttpClient , HttpEvent, HttpHeaders, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable, Input, Output, EventEmitter, Directive, Inject } from '@angular/core';
import { throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { AppError } from '../common/app-error';
import { NotFoundError } from '../common/not-found-error';
import { BadInput } from '../common/bad-input';
import { map } from 'rxjs/operators';

@Injectable()
export class DataService {
  @Output() getLoggedInStatus: EventEmitter<any> = new EventEmitter();
  public endpoint = 'http://localhost:8888/Jobserver/index.php/';
  getAuthToken = localStorage.getItem('x-auth-token');

  constructor(public http: HttpClient) {}

  isLoggedIn() {
    const helper = new JwtHelperService();
    const RawToken = this.getAuthToken;
    if (!RawToken) {
      return false;
      // After the user has logged in, emit the behavior subject changes.
    }
    const expirationDate = helper.getTokenExpirationDate(RawToken);
    const isExpired = helper.isTokenExpired(RawToken);
    const decodeToken = helper.decodeToken(RawToken);
    return true;
  }

  getCurrentUser() {
    const Authtoken = this.getAuthToken;
    if (!Authtoken) {
      return null;
    }
    const jwtHelper = new JwtHelperService();
    return jwtHelper.decodeToken(Authtoken);
  }

  logout() {
      localStorage.removeItem('x-signup-token');
      localStorage.removeItem('x-auth-token');
      localStorage.clear();
  }


  userRecord() {
    return this.http.get(`${this.endpoint}user/me`)
    .pipe(map((response: any) => response.json().catch(this.handleError)));
  }

  getCountries() {
    return this.http.get(`${this.endpoint}Auth/countries`)
    .pipe(map((response: any) => response));
  }

  getState(resource) {
    return this.http.get(`${this.endpoint}Auth/states`, resource)
    .pipe(map((response: any) => response));
  }


  activateEmail(resource) {
    return this.http.post(`${this.endpoint}user/activate`, resource)
     .pipe(map((response: any) => response.json().catch(this.handleError)));
  }

  emailPrivacy(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}user/emailprivacy`, resource)
     .pipe(map((response: any) => response.json().catch(this.handleError)));
  }

  messagePrivacy(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}user/privacymessage`, resource)
     .pipe(map((response: any) => response.json().catch(this.handleError)));
  }

  location(resource) {
    console.log(resource);
    return this.http.post(`${this.endpoint}user/location`, resource)
     .pipe(map((response: any) => response.json().catch(this.handleError)));
  }



  deleteAccount() {
    return this.http.post(`${this.endpoint}user/delete`, '')
     .pipe(map((response: any) => response.json().catch(this.handleError)));
  }


  avater(resource) {
    return this.http.post(`${this.endpoint}profile/avater`, resource)
     .pipe(map((response: any) => response.json().catch(this.handleError)));
  }


  public handleError(error: Response) {
    if (error.status === 400) {
      return throwError(new BadInput(error));
    }

    if (error.status === 404) {
      return throwError(new NotFoundError(error));
    }

    return throwError(new AppError(error));
  }
}
