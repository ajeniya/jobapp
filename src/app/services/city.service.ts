import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class CityService extends DataService {

  constructor(http: HttpClient) {
    super('http://localhost:3000/api/city', http);
  }

  getCity(resource) {
    const body: any = { state_id: resource };
    return this.http.post('http://localhost:3000/api/city/cities', body)
    .pipe(map((response: any) => response.json().catch(this.handleError)));

  }
}
