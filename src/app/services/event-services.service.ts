import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EventServicesService {
  navchange: EventEmitter<boolean> = new EventEmitter();
  isSetting: EventEmitter<boolean> = new EventEmitter();
  isAdmin: EventEmitter<boolean> = new EventEmitter();

  constructor() { }
}
