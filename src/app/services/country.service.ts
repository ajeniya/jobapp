import { HttpClient , HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable, Directive } from '@angular/core';
import { DataService } from '../services/data.service';
import { map } from 'rxjs/operators';


@Directive()
@Injectable({
  providedIn: 'root'
})
export class CountryService extends DataService  {

  constructor(http: HttpClient) {
    super(http);
  }

  getCountries() {
    return this.http.get(`${this.endpoint}auth/countries`)
    .pipe(map((response: any) => {
      console.log(response);
    }));

  }
}
