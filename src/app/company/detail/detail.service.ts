import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { CompanyJob, CompanyDetail } from './company-detail.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

jobs: CompanyJob[] = [
  {
  'job_id': '1',
  'job_title': 'Regional Sales Manager',
  'location': 'Chicago, Michigan',
  'job_salary': '$22200-$221000',
  'created_at': '1 Week Ago'
 },
 {
  'job_id': '2',
  'job_title': 'Devops',
  'location': 'Abuja',
  'job_salary': '$99200-$20300',
  'created_at': '1 Week Ago'
 },
 {
  'job_id': '3',
  'job_title': 'Developer Backend',
  'location': 'Lagos',
  'job_salary': '$9200-$22000',
  'created_at': '1 Week Ago'
 },
 {
  'job_id': '4',
  'job_title': 'Branch Manager',
  'location': 'Oyo',
  'job_salary': '$12300-$99000',
  'created_at': '1 Week Ago'
 }
]

constructor(private http: HttpClient) { }

  getCompanyJob() {
    return of(this.jobs);
  }

  getCompanyDetail(){
    return this.http.get<any>(`${environment.baseUrl}/Company/getcompany`, {})
    .pipe(map(result => {
            return result;
      }));
  }

  getCompanyDetailFromQuery(companyId){
    return this.http.get<any>(`${environment.baseUrl}/Company/getcompany?company_id=${companyId}`, {})
    .pipe(map(result => {
            return result;
      }));
  }
}
