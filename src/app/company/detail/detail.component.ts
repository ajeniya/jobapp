import { Component, OnInit, HostListener, Inject, OnDestroy } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import { EventServicesService } from 'src/app/services/event-services.service';
import { toolbarUpdated } from '@syncfusion/ej2-richtexteditor';
import { Subscription } from 'rxjs';
import { DetailService } from './detail.service';
import { CompanyDetail, CompanyJob } from './company-detail.model';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  navBar: any;
  element: any;
  addActiveClass: any;
  private selectedResumeName: string;
  subscriptions: Subscription[] = [];
  getCompanyDetailSubscription: Subscription;
  getcompanyJobSubscription: Subscription;
  singleCompany: any;
  companyJobList: any;
  logo: any;
  profile_image: any;
  companyLogo: any;
  about: any;
  company_name: any;
  location: any;
  type: any;
  category: any;


  constructor(@Inject(DOCUMENT) document,
  private eventServices: EventServicesService,
  public authServices: AuthenticationService,
  private route: ActivatedRoute,
  public data: DetailService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.element = document.getElementById('profile-nav');
    this.element.classList.add('fixed-bottom');
    this.addActiveClass = 'about';
    this.getDetailFromLink();
    this.getCompanyJob();
  }

  getDetailFromLink() {
    this.route.queryParams
      .subscribe(params => {
        if(params.link) {
          this.getCompanyDetailSubscription = this.data.getCompanyDetailFromQuery(params.link).subscribe((data: any) => {
            this.singleCompany = data;
            this.companyLogo = data.logo;
            this.profile_image = data.profile_image;
            this.about = data.about;
            this.company_name = data.company_name;
            this.location = data.location;
            this.type = data.type;
            this.category = data.category;
          })
        }
        this.subscriptions.push(this.getCompanyDetailSubscription);
      });
  }


  @HostListener('window:scroll', ['$event'])
    onWindowScroll(e) {
       if (window.pageYOffset > 400) {
         this.element.classList.add('nav-onschroll');
         this.element.classList.remove('fixed-bottom');
       }else {
         this.element.classList.remove('nav-onschroll');
         this.element.classList.add('fixed-bottom');
       }
    }

    getScrollNav(e, link) {
      this.addActiveClass = link;
    }


    getCompanyJob() {
      this.getcompanyJobSubscription = this.data.getCompanyJob().subscribe((data: any) => {
        this.companyJobList = data;
      })
      this.subscriptions.push(this.getcompanyJobSubscription);
    }

    // ngOnDestroy() {
    //   this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    // }

}
