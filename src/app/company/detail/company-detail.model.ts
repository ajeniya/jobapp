export class CompanyJob {
  job_id: string;
  job_title: string;
  location: string;
  job_salary: string;
  created_at: string;
}

export class CompanyDetail {
  company_id: string;
  company_name: string;
  company_logo: string;
  company_description: string;
  company_location: string;
  company_keyword: string[];
}
