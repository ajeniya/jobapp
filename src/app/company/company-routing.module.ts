import { AdminGuard } from './../_helpers/admin.guard';
import { CreateCompanyComponent } from './create-company/create-company.component';
import { EditJobComponent } from './edit-job/edit-job.component';
import { AddJobComponent } from './add-job/add-job.component';
import { BannedJobComponent } from './banned-job/banned-job.component';
import { ExpiredJobComponent } from './expired-job/expired-job.component';
import { PendingJobComponent } from './pending-job/pending-job.component';
import { JobListComponent } from './job-list/job-list.component';
import { MembershipComponent } from './membership/membership.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from '../company/detail/detail.component';
import { ListComponent } from '../company/list/list.component';
import { MessageComponent } from './message/message.component';
import { ManageJobComponent } from './manage-job/manage-job.component';
import { SettingComponent } from './setting/setting.component';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TransactionComponent } from './transaction/transaction.component';
import { AppliedUserComponent } from './applied-user/applied-user.component';
import { CompanyGuard } from '../_helpers/company.guard';

const routes: Routes = [
  { path: 'dashboard',
    canActivate: [CompanyGuard],
    component: DashboardComponent
  },
  { path: 'detail',
    component: DetailComponent
  },
  { path: 'list',
    component: ListComponent
  },
  {
    path: 'add-job',
    canActivate: [CompanyGuard],
    component: AddJobComponent
  },
  {
    path: 'manage-job',
    canActivate: [CompanyGuard],
    component: ManageJobComponent
  },
  {
    path: 'message',
    canActivate: [CompanyGuard],
    component: MessageComponent
  },
  {
    path: 'setting',
    canActivate: [CompanyGuard],
    component: SettingComponent
  },
  {
    path: 'update-profile',
    canActivate: [CompanyGuard],
    component: UpdateProfileComponent
  },
  {
    path: 'membership',
    canActivate: [CompanyGuard],
    component: MembershipComponent
  },
  {
    path: 'job-lists',
    canActivate: [CompanyGuard],
    component: JobListComponent
  },
  {
    path: 'pending-jobs',
    canActivate: [CompanyGuard],
    component: PendingJobComponent
  },
  {
    path: 'expired-jobs',
    canActivate: [CompanyGuard],
    component: ExpiredJobComponent
  },
  {
    path: 'banned-jobs',
    canActivate: [CompanyGuard],
    component: BannedJobComponent
  },
  {
    path: 'transaction',
    canActivate: [CompanyGuard],
    component: TransactionComponent
  },
  {
    path: 'applied-users',
    canActivate: [CompanyGuard],
    component: AppliedUserComponent
  },
  {
    path: 'edit-job',
    canActivate: [CompanyGuard],
    component: EditJobComponent
  },
  {
    path: 'create',
    canActivate: [CompanyGuard],
    component: CreateCompanyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompanyRoutingModule { }
