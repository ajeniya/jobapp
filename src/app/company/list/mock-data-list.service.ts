import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { CompanyLists } from './companies-list-model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';
import { delay } from 'rxjs/internal/operators/delay';

@Injectable({
  providedIn: 'root'
})
export class MockDataListService {

  constructor(private http: HttpClient) { }

  getcompanylist(pageNo?, pageSize?) {
    return this.http.get<any>(`${environment.baseUrl}/Company/getcompanylist?pageNo=${pageNo}&pageSize=${pageSize}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getSearch(pageNo?, pageSize?, name?, location?, category?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Company/searchcompanies?pageNo=${pageNo}&pageSize=${pageSize}&companyName=${name}&companyLocation=${location}&category=${category}`, {})
    .pipe(map(result => {
        return result;
    }));
  }
}
