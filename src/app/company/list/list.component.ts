import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { CompanyLists } from './companies-list-model';
import { EventServicesService } from 'src/app/services/event-services.service';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent, MatDialog } from '@angular/material';
import { MockDataListService } from './mock-data-list.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  resultsLength: number;
  subscriptions: Subscription[] = [];
  getCompanyListSubscription: Subscription;
  getCompantListSubmitSubscription: Subscription;
  getCompantListSubscription: Subscription;
  recentCompany: any[];
  searchForm: FormGroup;
  navBar: void;
  isLoadingResults: boolean;
  searchFound: boolean;
  setPageIndex: number;
  pageSize = 10;
  loading = false;
  noRecord = false;
  isSearch = false;

  constructor(public data: MockDataListService,
    private toastr: ToastrService, private formBuilder: FormBuilder,
    public eventServices: EventServicesService) { }

    ngOnInit() {
      this.navBar = this.eventServices.navchange.emit(true);
      this.getCompanylists();
      this.loadForm();
    }


  loadForm() {
    this.searchForm = this.formBuilder.group({
      companyName: [''],
      companyLocation: [''],
      companyCategory: ['']
    });
  }


  onSubmit( ) {
    this.setPageIndex = 1;
    const search = this.searchForm.value;
    this.loading = true;
    this.isSearch = true;
    this.getCompantListSubmitSubscription =
    this.data.getSearch(this.setPageIndex, this.pageSize, search.companyName, search.companyLocation, search.companyCategory)
    .subscribe((data: any) => {
     this.loading = false;
     this.recentCompany = data.result;
     this.resultsLength = data.paginationObject.totalRecord;
     this.searchFound = true;
     this.isLoadingResults = false;
   }, (error) => {
     this.isLoadingResults = false;
     this.loading = false;
     this.noRecord = false;
     this.toastr.error('No record found!', 'No Record!', {
       enableHtml: true,
       closeButton: true,
       timeOut: 3000
     });
   });

   this.subscriptions.push(this.getCompantListSubmitSubscription);
  }

  clearSearch() {
    this.isSearch = false;
    this.getCompanylists();
  }

  getCompanylists() {
    this.isLoadingResults = true;
    this.searchFound = false;

    if(!this.setPageIndex) {
      this.setPageIndex = 1
    }

    this.getCompantListSubscription = this.data.getcompanylist(this.setPageIndex, this.pageSize)
     .subscribe((data: any) => {
      this.recentCompany = data.result;
      this.resultsLength = data.paginationObject.totalRecord;
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noRecord = true;
    }, (error) => {
      this.isLoadingResults = false;
      this.resultsLength = 0;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getCompantListSubscription);
  }


  pageChanged(event) {
    this.setPageIndex = event.page;
    this.pageSize = event.itemsPerPage;
    this.getCompanylists()
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
