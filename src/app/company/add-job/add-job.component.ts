import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { EventServicesService } from 'src/app/services/event-services.service';
import { CurrencyPipe } from '@angular/common';
import { AddJobService } from './add-job.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-job',
  templateUrl: './add-job.component.html',
  styleUrls: ['./add-job.component.scss']
})
export class AddJobComponent implements OnInit {
  navBar: void;
  addJobForm: FormGroup;
  submitted = false;
  formattedAmount: string;
  amount: any;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  tags: any = [
  ];

  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]

  };

  fruits: any[] = [
    {name: 'Lemon'},
    {name: 'Lime'},
    {name: 'Apple'},
  ];
  currencySign: string;
  formattedAmountMax: any;

  SubscriptionPlan: any[] = [
    { name: 'Free Plan', value: 'free'},
    { name: 'Use my subscription plan', value: 'payed'}
  ];
  allowCVupload: any;

  constructor(private eventServices: EventServicesService,
    private data: AddJobService, private toastr: ToastrService,
    private router: Router,
    private currencyPipe : CurrencyPipe,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);

    this.addJobForm  =  this.formBuilder.group({
      category: [''],
      title: ['', Validators.required],
      description: ['', Validators.required],
      jobType: [''],
      salaryMin: [''],
      salaryMax: [''],
      negotiable: [''],
      city: ['', Validators.required],
      address: [''],
      submit_cv: [''],
      jobUrl: [''],
      tags: ['']
    });
  }

  get formControls() { return this.addJobForm.controls; }

  addJob() {
    this.submitted = true;
    const jobUrl = this.addJobForm.controls['jobUrl'].value.replace(/(^\w+:|^)\/\//, '');
    this.addJobForm.controls['tags'].setValue(this.tags);
    this.addJobForm.controls['jobUrl'].setValue(jobUrl);
    const value = this.addJobForm.value;
    const companyId = localStorage.getItem('companyId');

    this.data.addJob(value, companyId).subscribe((data) => {
      this.router.navigateByUrl('/company/job-lists');
      this.toastr.success('Job add success!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    }, (error) => {
      if(error == 'planError') {
        this.toastr.error('Subscription expired!', 'Failure!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      }

      if(error == 'failedtopost') {
        this.toastr.error('Unable to add job try again!', 'Failure!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      }
    })
  }

  submitCv(event) {
    this.allowCVupload = event.checked;
    this.addJobForm.controls['jobUrl'].setValue('');
  }

  transformAmountMin(element){
    this.currencySign = '$';
    const inputedValue = this.formControls.salaryMin.value;
    const removeText = inputedValue.replace(/[^0-9\.]+/g, "");
    this.formattedAmount = this.formControls.salaryMin.value;
    this.formattedAmount = this.currencyPipe.transform(removeText, this.currencySign);

    this.addJobForm.controls['salaryMin'].setValue(this.formattedAmount);
  }

  transformAmountMax(element){
    this.currencySign = '$';
    const inputedValue = this.formControls.salaryMax.value;
    const removeText = inputedValue.replace(/[^0-9\.]+/g, "");
    this.formattedAmountMax = this.formControls.salaryMax.value;
    this.formattedAmountMax = this.currencyPipe.transform(removeText, this.currencySign);
    this.addJobForm.controls['salaryMax'].setValue(this.formattedAmountMax);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push({name: value.trim()});
      this.addJobForm.controls['tags'].setValue(this.tags);
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(tag: any): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
      this.addJobForm.controls['tags'].setValue(this.tags);
    }
  }

}
