import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AddJobService {

  constructor(private http: HttpClient) { }

  addJob(formData, companyId){
    return this.http.post<any>(`${environment.baseUrl}/Job/addjob`,
    {
      'company_id': companyId,
      'occupation': formData.category,
      'location': formData.city,
      'description': formData.description,
      'type': formData.jobType,
      'job_url': formData.jobUrl,
      'negotiable': formData.negotiable,
      'min_salary': formData.salaryMin,
      'max_salary': formData.salaryMax,
      'tags': formData.tags,
      'title': formData.title,
      'submit_cv': formData.submit_cv,
    }).pipe(map(result => {
            return result;
      }));
  }
}
