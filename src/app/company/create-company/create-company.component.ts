import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import * as $ from 'jquery';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-create-company',
  templateUrl: './create-company.component.html',
  styleUrls: ['./create-company.component.scss']
})
export class CreateCompanyComponent implements OnInit {
  navBar: void;
  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]

  };

  @ViewChild('chooseProfileImg') chooseProfileImg: ElementRef;
  settingForm: FormGroup;
  createCompanyForm: FormGroup;
  selectedLogoFile: File;
  selectedProfileImgFile: File;
  submitted = false;
  loading = false;
  isProfileImageValid = false;
  companyLogo: any;
  companyProfileImg: any;
  showAccountNotVerified = false;

  constructor(private eventServices: EventServicesService,
    public authServices: AuthenticationService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);

    $('#chooseFile').bind('change', function () {
      var filename = $("#chooseFile").val();
      if (/^\s*$/.test(filename)) {
        $(".file-upload").removeClass('active');
        $("#noFile").text("No file chosen...");
      }
      else {
        $(".file-upload").addClass('active');
        $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
      }
    });

    $('#chooseFilelogo').bind('change', function () {
      var filenamelogo = $("#chooseFilelogo").val();
      if (/^\s*$/.test(filenamelogo)) {
        $(".file-upload-logo").removeClass('active');
        $("#noLogoFile").text("No file chosen...");
      }
      else {
        $(".file-upload-logo").addClass('active');
        $("#noLogoFile").text(filenamelogo.replace("C:\\fakepath\\", ""));
      }
    });

    this.loadForm();
    this.showNofication();
    this.prefillCompanyDetails();
  }

  showNofication() {
    this.authServices.currentUser.subscribe((currentUserObject: any) => {
      if( currentUserObject.data.verified == 1 ) {
          this.showAccountNotVerified = false;
      } else {
        this.showAccountNotVerified = true;
      }
    });
  }

  prefillCompanyDetails() {
    const decoded = this.authServices.decodeToken();
    this.createCompanyForm.patchValue({
      companyName: decoded.full_name,
      companyEmail: decoded.email
    })
  }

  get f() { return this.createCompanyForm.controls; }

  loadForm() {
    this.createCompanyForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      companyLocation: ['', Validators.required],
      companySize: ['', Validators.required],
      companyCategory: ['', Validators.required],
      companyEmail: ['',[Validators.required, Validators.email]],
      companyEmailJob: ['',[Validators.required, Validators.email]],
      website: [''],
      youtube: [''],
      facebook: [''],
      linkdin: [''],
      twitter: [''],
      aboutCompany: ['', Validators.required]
    });
  }

  onFileSelectedLogo(event) {
    this.selectedLogoFile = event.target.files[0];
  }

  onFileSelectedProfileImg(event) {
    this.selectedProfileImgFile = event.target.files[0];
    const Img = new Image();
    Img.src = URL.createObjectURL(this.selectedProfileImgFile);
    Img.onload = (e: any) => {
      const width = e.path[0].width;
      const height = e.path[0].height;

      if(width < 1490 || height < 749) {
        return this.toastr.error('Your uploaded image size is wrong!', 'Wrong Size!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      } else {
        this.isProfileImageValid = true;
      }
    }
  }

  onSubmit() {
    if(!this.selectedProfileImgFile) {
      return this.toastr.error('Upload your profile image!', 'Profile Image error!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    }

    if(!this.selectedLogoFile) {
      return this.toastr.error('Upload your logo!', 'Logo error!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    }

    this.submitted = true;
    this.loading = true;
    const formData = new FormData();
    const getToken = this.authServices.decodeToken();
    if(this.selectedLogoFile) {
      formData.append('logo', this.selectedLogoFile, this.selectedLogoFile.name);
    }

    if(this.selectedProfileImgFile && this.isProfileImageValid) {
      formData.append('profileImage', this.selectedProfileImgFile, this.selectedProfileImgFile.name);
    }
    formData.append('userId', getToken.id);
    formData.append('companyName', this.createCompanyForm.get('companyName').value);
    formData.append('companyLocation', this.createCompanyForm.get('companyLocation').value);
    formData.append('companySize', this.createCompanyForm.get('companySize').value);
    formData.append('category', this.createCompanyForm.get('companyCategory').value);
    formData.append('companyEmail', this.createCompanyForm.get('companyEmail').value);
    formData.append('companyEmailJob', this.createCompanyForm.get('companyEmailJob').value);
    formData.append('website', this.createCompanyForm.get('website').value);
    formData.append('youtube', this.createCompanyForm.get('youtube').value);
    formData.append('facebook', this.createCompanyForm.get('facebook').value);
    formData.append('linkdin', this.createCompanyForm.get('linkdin').value);
    formData.append('twitter', this.createCompanyForm.get('twitter').value);
    formData.append('aboutCompany', this.createCompanyForm.get('aboutCompany').value);

    this.http.post(`${environment.baseUrl}/Company/addcompany`, formData ).subscribe((data: any) => {
      this.loading = false;
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
    });
  }

}
