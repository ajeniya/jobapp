import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { Job } from 'src/app/job/list/list.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class BannedService {
  constructor(private http: HttpClient) { }

  getCompanyDetailFromQuery(userId){
   return this.http.get<any>(`${environment.baseUrl}/Company/getsingle?user_id=${userId}`, {})
   .pipe(map(result => {
           return result;
     }));
 }

 deleteJob(jodId) {
  return this.http.post<any>(`${environment.baseUrl}/Job/deletejob`, {
    'checked': jodId
  })
  .pipe(map(result => {
          return result;
    }));
}

  getBannedJoblist(pageNo?, pageSize?) {
   return this.http.get<any>
   (`${environment.baseUrl}/Job/getjoblist?pageNo=${pageNo}&pageSize=${pageSize}&status=4&companyId=${localStorage.getItem('companyId')}`, {})
   .pipe(map(result => {
       return result;
   }));
 }
}
