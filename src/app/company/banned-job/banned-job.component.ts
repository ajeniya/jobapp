import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { Job } from 'src/app/job/list/list.model';
import { BannedService } from './banned.service';
import { EventServicesService } from 'src/app/services/event-services.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ToastrService } from 'ngx-toastr';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-banned-job',
  templateUrl: './banned-job.component.html',
  styleUrls: ['./banned-job.component.scss']
})
export class BannedJobComponent implements OnInit, OnDestroy {
  navBar: void;
  subscriptions: Subscription[] = [];
  getJobSubscription: Subscription;
  getCompanyDetailSubscription: Subscription;
  deleteJobSubscription: Subscription;
  companyId: any;
  isLoadingResults: boolean;
  searchFound: boolean;
  setPageIndex: number;
  recentJob: any[];
  pageSize = 10;
  loading = false;
  noRecord = false;
  isSearch = false;
  resultsLength: any;
  jobLists: any;

  constructor(private eventServices: EventServicesService,
    private toastr: ToastrService,  public dialog: MatDialog,
    public authenticationService: AuthenticationService,
    public data: BannedService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getPendingJoblists();
  }

  getCompanyDetails() {
    const token = this.authenticationService.decodeToken();
    this.getCompanyDetailSubscription = this.data.getCompanyDetailFromQuery(token.id).subscribe((data: any) => {
      this.companyId = data;
      localStorage.setItem('companyId', data.company_id);
    })
  }

  deleteJob(jobId) {
    this.deleteJobSubscription = this.data.deleteJob(jobId)
     .subscribe((data: any) => {
      this.getPendingJoblists();
      this.toastr.error('Deleted successful!', 'Deleted!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.deleteJobSubscription);
  }

  onConfirmDelete(jobId) {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Remove Job',
        message: 'Are you sure, you want to delete job '
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.deleteJob(jobId);
        }
      });
  }

  getPendingJoblists() {
    this.isLoadingResults = true;
    this.searchFound = false;

    if(!this.setPageIndex) {
      this.setPageIndex = 1
    }

    this.getJobSubscription = this.data.getBannedJoblist(this.setPageIndex, this.pageSize)
     .subscribe((data: any) => {
      this.jobLists = data.result;
      this.resultsLength = data.paginationObject.totalRecord;
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noRecord = true;
    }, (error) => {
      this.isLoadingResults = false;
      this.resultsLength = 0;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getJobSubscription);
  }

  pageChanged(event) {
    this.setPageIndex = event.page;
    this.pageSize = event.itemsPerPage;
    this.getPendingJoblists();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
