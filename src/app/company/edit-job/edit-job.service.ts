import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { EditJob } from './edit-job.mock';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class EditJobService {
  constructor(private http: HttpClient) { }

  getEditJob(param) {
    return this.http.get<any>(`${environment.baseUrl}/Job/getsinglejob?job_id=${param}`, {})
    .pipe(map(result => { return result }));
  }

   editJob(payload, job_id) {
    return this.http.post<any>(`${environment.baseUrl}/Job/editJob`, {
      'job_id': job_id,
      'title': payload.title,
      'occupation': payload.category,
      'description': payload.description,
      'jobType': payload.jobType,
      'salaryMin': payload.salaryMin,
      'salaryMax': payload.salaryMax,
      'location': payload.city,
      'job_url': payload.jobUrl,
      'tags': payload.tags,
      'negotiable': payload.negotiable,
      'submit_cv': payload.submit_cv,
    })
    .pipe(map(result => {
        return result;
    }));
  }
}
