import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { EventServicesService } from 'src/app/services/event-services.service';
import { CurrencyPipe } from '@angular/common';
import { EditJobService } from './edit-job.service';
import { EditJob } from './edit-job.mock';
import { ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-job',
  templateUrl: './edit-job.component.html',
  styleUrls: ['./edit-job.component.scss']
})
export class EditJobComponent implements OnInit, OnDestroy {
  navBar: void;
  subscriptions: Subscription[] = [];
  updateJobForm: FormGroup;
  submitted = false;
  formattedAmount: string;
  amount: any;
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  tags: any = [
  ];

  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]

  };
  currencySign: string;
  formattedAmountMax: any;

  SubscriptionPlan: any[] = [
    { name: 'Free Plan', value: 'free'},
    { name: 'Use my subscription plan', value: 'payed'}
  ];
  getJobSubscription: Subscription;
  jobToEdit: EditJob;
  setTags: object[];
  allowCVupload: any;
  jobEdit: any;
  jobId: any;
  loading: boolean;

  constructor(private eventServices: EventServicesService,
    public data: EditJobService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private currencyPipe : CurrencyPipe,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);

    this.route.queryParams
      .subscribe(params => {
        this.jobId = params.a;
      })

    this.updateJobForm  =  this.formBuilder.group({
      category: [''],
      title: ['', Validators.required],
      description: ['', Validators.required],
      jobType: [''],
      salaryMin: [''],
      salaryMax: [''],
      negotiable: [''],
      city: ['', Validators.required],
      jobUrl: [''],
      submit_cv: [''],
      tags: [''],
      subscribeType: [''],
    });
    this.getEditJobs();
  }

  get formControls() { return this.updateJobForm.controls; }

  getEditJobs() {
    this.getJobSubscription = this.data.getEditJob(this.jobId).subscribe((data) => {
      this.jobEdit = data;
      this.updateJobForm.controls['category'].setValue(data.occupation);
      this.updateJobForm.controls['title'].setValue(data.title);
      this.updateJobForm.controls['description'].setValue(data.description);
      this.updateJobForm.controls['jobType'].setValue(data.type);
      this.updateJobForm.controls['salaryMin'].setValue(data.min_salary);
      this.updateJobForm.controls['salaryMax'].setValue(data.max_salary);
      this.updateJobForm.controls['negotiable'].setValue(data.negotiable);
      this.updateJobForm.controls['city'].setValue(data.location);
      this.updateJobForm.controls['jobUrl'].setValue(data.job_url);
      if(data.submit_cv == 1) {
        this.allowCVupload = true;
        this.updateJobForm.controls['submit_cv'].setValue(true);
      } else {
        this.allowCVupload = false;
        this.updateJobForm.controls['submit_cv'].setValue(false);
      }

      let encodedTag = JSON.parse(data.tags);
      encodedTag.forEach(element => {
        this.tags.push(element)
      });

      this.updateJobForm.controls['tags'].setValue(this.tags);

    });
    this.subscriptions.push(this.getJobSubscription);
  }


  updateJob() {
    // Remove https://, http://, www from Url links
    this.loading = true;
    const formatedLink = this.formControls.jobUrl.value.replace(/^https?:\/\//,'');
    this.updateJobForm.controls['jobUrl'].setValue(formatedLink);
    const value = this.updateJobForm.value;
    this.data.editJob(value, this.jobId).subscribe((data) => {
      this.tags = [];
      this.getEditJobs();
      this.loading = false;
      this.toastr.success('Update Successful!', 'Successful!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    })
  }

  submitCv(event) {
    this.allowCVupload = event.checked;
    this.updateJobForm.controls['jobUrl'].setValue('');
  }

  transformAmountMin(element){
    this.currencySign = '$';
    const inputedValue = this.formControls.salaryMin.value;
    const removeText = inputedValue.replace(/[^0-9\.]+/g, "");
    this.formattedAmount = this.formControls.salaryMin.value;
    this.formattedAmount = this.currencyPipe.transform(removeText, this.currencySign);

    this.updateJobForm.controls['salaryMin'].setValue(this.formattedAmount);
  }

  transformAmountMax(element){
    this.currencySign = '$';
    const inputedValue = this.formControls.salaryMax.value;
    const removeText = inputedValue.replace(/[^0-9\.]+/g, "");
    this.formattedAmountMax = this.formControls.salaryMax.value;
    this.formattedAmountMax = this.currencyPipe.transform(removeText, this.currencySign);
    this.updateJobForm.controls['salaryMax'].setValue(this.formattedAmountMax);
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    if ((value || '').trim()) {
      this.tags.push({name: value.trim()});
      this.updateJobForm.controls['tags'].setValue(this.tags);
    }

    if (input) {
      input.value = '';
    }
  }

  remove(tag: any): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
      this.updateJobForm.controls['tags'].setValue(this.tags);
    }
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
