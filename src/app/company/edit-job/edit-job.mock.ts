export class EditJob {
  job_id: string;
  category: string;
  title: string;
  description: string;
  job_type: string;
  min_salary?: string;
  max_salary?: string;
  negotiate?: boolean;
  city: string;
  address?: string;
  application_url?: string;
  tags?: object[];
  free_plan?: string;
  payed_plan?: string;

}
