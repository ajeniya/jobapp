export class Applicants {
  applicant_id: string;
  company_id: string;
  user_id: string;
  job_id: string;
  fullname: string;
  location: string;
  email: string;
  phone: string;
  aboutApplicant: string;
  uplodedcv: string;
  title: string;
  date_applied: string;
}
