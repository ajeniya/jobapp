import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Applicants } from './applied-user.model';
import { EventServicesService } from 'src/app/services/event-services.service';
import { AppliedUserService } from './applied-user.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';

@Component({
  selector: 'app-applied-user',
  templateUrl: './applied-user.component.html',
  styleUrls: ['./applied-user.component.scss']
})
export class AppliedUserComponent implements OnInit, OnDestroy {
  navBar: void;
  subscriptions: Subscription[] = [];
  getUsersSubscription: Subscription;
  getJobApplicantsSubscription: Subscription;
  deleteApplicantSubscription: Subscription;
  userLists: Applicants[];
  cvLink: any;
  userCVName: string;
  setPageIndex: number;
  pageSize = 5;
  searchFound: boolean;
  isLoadingResults: boolean;
  resultsLength: any;
  noRecord: boolean;
  applicants: any;

  constructor(private eventServices: EventServicesService,
    private toastr: ToastrService, private route: ActivatedRoute,
    public ngxSmartModalService: NgxSmartModalService, public dialog: MatDialog,
    public data: AppliedUserService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getAppliedUsers();
  }

  getAppliedUsers() {
    this.route.queryParams
    .subscribe(params => {
      const jobId = params.a;
      this.isLoadingResults = true;
      this.searchFound = false;

      if(!this.setPageIndex) {
        this.setPageIndex = 1
      }

      this.getJobApplicantsSubscription = this.data.getApplicants(this.setPageIndex, this.pageSize, jobId)
       .subscribe((data: any) => {
        this.applicants = data.result;
        this.resultsLength = data.paginationObject.totalRecord;
        this.searchFound = true;
        this.isLoadingResults = false;
        this.noRecord = true;
      }, (error) => {
        this.isLoadingResults = false;
        this.resultsLength = 0;
        this.toastr.error('No applicant found!', 'No Record!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      });
    });
    this.subscriptions.push(this.getJobApplicantsSubscription);
  }

  deleteApplicant(jobId) {
    this.deleteApplicantSubscription = this.data.deleteApplicant(jobId)
     .subscribe((data: any) => {
      this.getAppliedUsers();
      this.toastr.error('Deleted successful!', 'Deleted!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.deleteApplicantSubscription);
  }

  onConfirmDelete(jobId) {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Remove Applicant Details',
        message: 'Are you sure, you want to delete applicant details'
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.deleteApplicant(jobId);
        }
      });
  }

  openCV(applicant: Applicants){
    this.userCVName = applicant.fullname;
    this.cvLink = applicant.uplodedcv;
    this.ngxSmartModalService.getModal('myModal').open();
  }

  downloadCVFile(applicant){
    const createLink = document.createElement('a');
    createLink.setAttribute('href', applicant.uplodedcv);
    document.body.appendChild(createLink);
    createLink.click();
    createLink.remove();
  }

  pageChanged(event) {
    this.setPageIndex = event.page;
    this.pageSize = event.itemsPerPage;
    this.getAppliedUsers();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
