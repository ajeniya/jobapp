import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AppliedUserService {
  constructor(private http: HttpClient) { }

  getApplicants(pageNo?, pageSize?, jobId?) {
    return this.http.get<any>(`${environment.baseUrl}/Job/getJobsApplicant?pageNo=${pageNo}&pageSize=${pageSize}&job_id=${jobId}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  deleteApplicant(applicantId) {
    return this.http.post<any>(`${environment.baseUrl}/Job/removeapplicant`, {
      'applicantId': applicantId
    })
    .pipe(map(result => {
            return result;
      }));
  }
}
