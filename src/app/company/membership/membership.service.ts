import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MembershipService {

  constructor(private http: HttpClient) { }

  getplanlist() {
    return this.http.get<any>(`${environment.baseUrl}/Plan/getallplan`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  cancelplan(planId) {
    return this.http.post<any>(`${environment.baseUrl}/Job/cancelplan`, {
      'planId': planId
    })
    .pipe(map(result => {
        return result;
    }));
  }

  getplandetails(companyId) {
    return this.http.get<any>(`${environment.baseUrl}/Job/jobplandetails?company_id=${companyId}`, {})
    .pipe(map(result => {
        return result;
    }));
  }
}
