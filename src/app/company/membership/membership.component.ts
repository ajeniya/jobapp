import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MembershipService } from './membership.service';
import { EventServicesService } from 'src/app/services/event-services.service';
import { MatDialog } from '@angular/material';
import { PaymentDialogComponent } from 'src/app/_components/payment-dialog/payment-dialog.component';
import { ToastrService } from 'ngx-toastr';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.scss']
})
export class MembershipComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  getPlanSubscription: Subscription;
  getPlanDetailsSubscription: Subscription;
  list: any;
  navBar: void;

  showSuccess: boolean;
  showCancel: boolean;
  companyId: any;
  planDetails: any;

  constructor(public data: MembershipService, public dialog: MatDialog,
    public eventServices: EventServicesService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getPlans();
    this.getCompanyPlanDetails();
  }

  getPlans() {
    this.getPlanSubscription = this.data.getplanlist().subscribe((data) => {
      this.list = data;
    });

    this.subscriptions.push(this.getPlanSubscription);
  }

  getCompanyPlanDetails() {
    const companyId = localStorage.getItem('companyId');
    this.getPlanDetailsSubscription = this.data.getplandetails(companyId).subscribe((data) => {
      this.planDetails = data;
    });

    this.subscriptions.push(this.getPlanDetailsSubscription);
  }

  cancelSubscription(planId) {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Delete Plan',
        message: 'Are you sure, you want to delete Plan '
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.data.cancelplan(planId).subscribe((data) => {
            this.getCompanyPlanDetails();
          });

          this.toastr.error('Plan as been deleted!', 'Plan Deleted!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });
        }
      });
  }

  openDialogPlanActine(item): void {
    if(this.planDetails.isPlanActive == 'true') {
      this.toastr.error('You are on a Plan!', 'Plan Exist!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    } else {
      const dialogRef = this.dialog.open(PaymentDialogComponent, {
        width: '400px',
        data: item
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getCompanyPlanDetails();
      });
    }
  }

  openDialog(item): void {
      const dialogRef = this.dialog.open(PaymentDialogComponent, {
        width: '400px',
        data: item
      });

      dialogRef.afterClosed().subscribe(result => {
        this.getCompanyPlanDetails();
      });
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
