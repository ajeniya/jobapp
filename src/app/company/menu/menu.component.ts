import { Subscription } from 'rxjs';
import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Input()
  companyId: string;
  userDetails: any;
  subscriptions: Subscription[] = [];
  getCompanyDetailSubscription: Subscription;

  constructor(public authServices: AuthenticationService,
    private ref: ChangeDetectorRef,
    public authenticationService: AuthenticationService,) { }

  ngOnInit(): void {
    this.userDetails = this.authServices.decodeToken();
    this.companyId = localStorage.getItem('companyId');

    this.ref.markForCheck();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
