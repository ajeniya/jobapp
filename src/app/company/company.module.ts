import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { CompanyRoutingModule } from './company-routing.module';
import { DetailComponent } from './detail/detail.component';
import { ListComponent } from './list/list.component';
import { NgxPayPalModule } from 'ngx-paypal';

import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatTableModule } from '@angular/material/table'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { BreadcrumbModule } from 'angular-crumbs';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { ManageJobComponent } from './manage-job/manage-job.component';
import { MessageComponent } from './message/message.component';
import { SettingComponent } from './setting/setting.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MembershipComponent } from './membership/membership.component';
import { JobListComponent } from './job-list/job-list.component';
import { PendingJobComponent } from './pending-job/pending-job.component';
import { ExpiredJobComponent } from './expired-job/expired-job.component';
import { BannedJobComponent } from './banned-job/banned-job.component';
import { TransactionComponent } from './transaction/transaction.component';
import { MenuComponent } from './menu/menu.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import {NgxPaginationModule} from 'ngx-pagination';
import { MatSortModule } from '@angular/material/sort';
import { AddJobComponent } from './add-job/add-job.component';
import { MatMenuModule} from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { ChartsModule } from 'ng2-charts';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule, MatBadgeModule, MatInputModule, MatRippleModule, MatRadioModule, MatDialogModule } from '@angular/material';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { AppliedUserComponent } from './applied-user/applied-user.component';
import { EditJobComponent } from './edit-job/edit-job.component';
import { CreateCompanyComponent } from './create-company/create-company.component';


@NgModule({
  declarations: [DetailComponent, ListComponent, UpdateProfileComponent, ManageJobComponent, MessageComponent,
    SettingComponent, DashboardComponent, MembershipComponent, JobListComponent,
    PendingJobComponent, ExpiredJobComponent, BannedJobComponent, TransactionComponent,
    MenuComponent, AddJobComponent, AppliedUserComponent, EditJobComponent, CreateCompanyComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    PaginationModule,
    NgxPaginationModule,
    CompanyRoutingModule,
    NgxSmartModalModule,
    ScrollToModule.forRoot(),
    SharedModule,
    MatTableModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MatChipsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatRadioModule,
    MatBadgeModule,
    ChartsModule,
    NgxDocViewerModule,
    MatDialogModule,
    RichTextEditorAllModule,
    BreadcrumbModule,
    NgxPayPalModule
  ],
  providers: [CurrencyPipe]
})
export class CompanyModule { }
