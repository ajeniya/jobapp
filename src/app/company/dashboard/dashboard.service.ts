import { Injectable } from '@angular/core';
import { Job } from 'src/app/job/list/list.model';
import { of } from 'rxjs/internal/observable/of';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient) { }

  getCompanyDashboardInfo(companyId) {
    console.log(companyId);
    return this.http.get<any>
    (`${environment.baseUrl}/Company/getcompanydashboardinfo?companyId=${companyId}`, {})
    .pipe(map(result => {
        return result;
    }));
  }
}
