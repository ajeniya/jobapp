import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs';
import { Job } from 'src/app/job/list/list.model';
import { DashboardService } from './dashboard.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { MatDialog } from '@angular/material';
import { ToastrService } from 'ngx-toastr';
import { JobListService } from '../job-list/job-list.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  navBar: void;
  subscriptions: Subscription[] = [];
  getJobSubscription: Subscription;
  searchJobSubscription: Subscription;
  getCompanyDetailSubscription: Subscription;
  deleteJobSubscription: Subscription;
  getCompanyDashboardInfos: Subscription;
  jobLists: Job[];
  isLoadingResults: boolean;
  searchFound: boolean;
  setPageIndex: number;
  recentJob: any[];
  pageSize = 10;
  loading = false;
  noRecord = false;
  isSearch = false;
  resultsLength: any;
  searchValue: any;
  clearValue = '';
  clear: boolean;
  companyId: any;

  showAccountNotVerified = false;
  dashboardInfo: any;


  constructor(private eventServices: EventServicesService,
    private toastr: ToastrService, public dialog: MatDialog,
    public authenticationService: AuthenticationService,
    public dashboard: DashboardService,
    public data: JobListService) {
    }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getJoblists();
    this.getCompanyDashboardInfo();
  }

  getCompanyDashboardInfo() {
    this.getCompanyDashboardInfos = this.dashboard.getCompanyDashboardInfo(this.companyId)
     .subscribe((data: any) => {
      this.dashboardInfo = data;
    });

    this.subscriptions.push(this.getCompanyDashboardInfos);
  }


  searchJob() {
    this.isLoadingResults = true;
    this.searchFound = false;

    if(!this.setPageIndex) {
      this.setPageIndex = 1
    }

    this.searchJobSubscription = this.data.searchjoblist(this.setPageIndex, this.pageSize, this.searchValue)
     .subscribe((data: any) => {
      this.jobLists = data.result;
      this.resultsLength = data.paginationObject.totalRecord;
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noRecord = true;
    }, (error) => {
      this.isLoadingResults = false;
      this.resultsLength = 0;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.searchJobSubscription);
  }

  onKey(event) {
    const inputValue = event.target.value;
    this.searchValue = inputValue;

    if(inputValue.length >= 3) {
      this.clear = true;
      this.searchJob();
    }
  }

  clearSearch() {
    this.searchValue = '';
    this.clearValue = '';
    this.clear = false;
    this.searchJob();
  }


  getJoblists() {
    this.isLoadingResults = true;
    this.searchFound = false;

    if(!this.setPageIndex) {
      this.setPageIndex = 1
    }

    this.getJobSubscription = this.data.getjoblist(this.setPageIndex, this.pageSize)
     .subscribe((data: any) => {
      this.jobLists = data.result;
      this.resultsLength = data.paginationObject.totalRecord;
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noRecord = true;
    }, (error) => {
      this.isLoadingResults = false;
      this.resultsLength = 0;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getJobSubscription);
  }

  deleteJob(jobId) {
    this.deleteJobSubscription = this.data.deleteJob(jobId)
     .subscribe((data: any) => {
      this.getJoblists();
      this.getCompanyDashboardInfo();
      this.toastr.error('Deleted successful!', 'Deleted!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.deleteJobSubscription);
  }

  onConfirmDelete(jobId) {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Remove Job',
        message: 'Are you sure, you want to delete job '
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.deleteJob(jobId);
        }
      });
  }

  pageChanged(event) {
    this.setPageIndex = event.page;
    this.pageSize = event.itemsPerPage;
    this.getJoblists();
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
