import { Injectable } from '@angular/core';
import { Job } from 'src/app/job/list/list.model';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class JobListService {

   constructor(private http: HttpClient) { }

   getjoblist(pageNo?, pageSize?) {
    return this.http.get<any>(`${environment.baseUrl}/Job/getjoblist?pageNo=${pageNo}&pageSize=${pageSize}&companyId=${localStorage.getItem('companyId')}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  deleteJob(jodId) {
    return this.http.post<any>(`${environment.baseUrl}/Job/deletejob`, {
      'checked': jodId
    })
    .pipe(map(result => {
            return result;
      }));
  }

  searchjoblist(pageNo?, pageSize?, search?) {
    return this.http.post<any>(`${environment.baseUrl}/Job/searchjobs`, {
      'pageNo': pageNo,
      'pageSize': pageSize,
      'search': search
    })
    .pipe(map(result => {
        return result;
    }));
  }
}
