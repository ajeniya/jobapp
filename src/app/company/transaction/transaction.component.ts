import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { EventServicesService } from 'src/app/services/event-services.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.scss']
})
export class TransactionComponent implements OnInit {

  displayedColumns: string[] = ['title', 'amount', 'plan', 'payment', 'date', 'status'];
  dataSource = new MatTableDataSource<SubscriptionPlan>(ELEMENT_DATA);

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  navBar: void;

  constructor(private eventServices: EventServicesService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}


export interface SubscriptionPlan {
  title: string;
  amount: number;
  plan: string;
  payment: string;
  date: string;
  status: string;
}

const ELEMENT_DATA: SubscriptionPlan[] = [
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Web developer', amount: 20, plan: 'Payed', payment: '', date: '12 Sep 2020', status: 'success'},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},
  { title: 'Hydrogen', amount: 1.0079, plan: 'H', payment: '', date: '', status: ''},

];
