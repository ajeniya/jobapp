import { Component, OnInit } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';

@Component({
  selector: 'app-manage-job',
  templateUrl: './manage-job.component.html',
  styleUrls: ['./manage-job.component.scss']
})
export class ManageJobComponent implements OnInit {
  navBar: void;

  constructor(private eventServices: EventServicesService,) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
  }

}
