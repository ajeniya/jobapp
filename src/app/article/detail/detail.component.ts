import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { EventServicesService } from 'src/app/services/event-services.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  navBar: void;

  constructor(public ngxSmartModalService: NgxSmartModalService,
    private eventServices: EventServicesService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
  }

}
