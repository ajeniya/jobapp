import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { EventServicesService } from 'src/app/services/event-services.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  navBar: void;

  constructor(public ngxSmartModalService: NgxSmartModalService,
    private eventServices: EventServicesService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
  }

}
