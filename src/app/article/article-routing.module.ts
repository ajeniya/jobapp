import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from '../article/detail/detail.component';
import { HomeComponent } from '../article/home/home.component';
import { ListComponent } from '../article/list/list.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'article', component: HomeComponent },
  { path: 'detail', component: DetailComponent },
  { path: 'list', component: ListComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArticleRoutingModule { }
