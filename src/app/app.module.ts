import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AgmCoreModule } from '@agm/core';
import { MatToolbarModule, MatIconModule, MatSidenavModule, MatListModule, MatButtonModule, MatProgressSpinnerModule, MatInputModule, MatFormFieldModule, MatDialogModule } from  '@angular/material';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { PaginationModule } from 'ngx-bootstrap/pagination';

import { AppRoutingModule } from './app-routing.module';
import { ToastrModule } from 'ngx-toastr';
import { AppComponent } from './app.component';
import { TestComponent } from './ui/test/test.component';
import { PagenotfoundComponent } from './ui/pagenotfound/pagenotfound.component';
import { SignUpComponent } from './ui/sign-up/sign-up.component';
import { SignInComponent } from './ui/sign-in/sign-in.component';
import { ProfileComponent } from './ui/profile/profile.component';
import { HomeComponent } from './ui/home/home.component';
import { HeaderComponent } from './ui/header/header.component';
import { FooterComponent } from './ui/footer/footer.component';
import { ActivateEmailComponent } from './ui/activate-email/activate-email.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ResetPasswordComponent } from './ui/reset-password/reset-password.component';
import { ForgotPasswordComponent } from './ui/forgot-password/forgot-password.component';

import { NgxPayPalModule } from 'ngx-paypal';
import { CompanyModule } from './company/company.module';
import { JobModule } from './job/job.module';
import { SharedModule } from './shared/shared.module';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DataService } from './services/data.service';
import { ConfirmEmailComponent } from './ui/confirm-email/confirm-email.component';
import { TestimonialComponent } from './ui/testimonial/testimonial.component';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { SocialLoginModule, SocialAuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';
import { DateAgoPipe } from './_helpers/pipes/date-ago.pipe';
import { PaymentDialogComponent } from './_components/payment-dialog/payment-dialog.component';
import { StripeModule } from "stripe-angular"

export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    PagenotfoundComponent,
    SignUpComponent,
    SignInComponent,
    ProfileComponent,
    HomeComponent,
    FooterComponent,
    ActivateEmailComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    ConfirmEmailComponent,
    TestimonialComponent,
    DateAgoPipe,
    PaymentDialogComponent

  ],
  imports: [
    SharedModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    CompanyModule,
    JobModule,
    BrowserModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    PaginationModule.forRoot(),
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    NgxSmartModalModule.forRoot(),
    ScrollToModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDpjvFNeX3IeOubKcAPIxLbt-0QjmGEL6c'
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    }),
    SocialLoginModule,
    NgxPayPalModule,
    StripeModule.forRoot("sk_test_51H88NwFgBIl4SeHI1OgbE9vC3uQSeUAl2YhsChNF2DJPXB07SDNI3thE9CeMHuRRG7zOfVrDh4eYPT7mPLnfjlxd00W2n0Hsof")
  ],
  providers: [
    DataService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    {provide: String, useValue: "test"},
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '624796833023-clhjgupm0pu6vgga7k5i5bsfp6qp6egh.apps.googleusercontent.com'
            ),
          }
        ],
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
