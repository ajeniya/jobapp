import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { error } from 'console';
import { NgxSmartModalService } from 'ngx-smart-modal';

@Component({
  selector: 'app-account-setting',
  templateUrl: './account-setting.component.html',
  styleUrls: ['./account-setting.component.scss']
})
export class AccountSettingComponent implements OnInit, OnDestroy {
  navBar: void;
  showAccountNotVerified = false;
  updateSettingForm: FormGroup;
  selectedCVFile: any;
  selectedProfileImgFile: any;
  isProfileImageValid: boolean;
  companyProfileImg: any;
  profileImage: any;
  user_id: any;
  submitted: boolean;
  loading: boolean;
  loadingDeactivate: boolean;
  profileCV: any;
  username: any;

  constructor(private eventServices: EventServicesService,
    public authenticationService: AuthenticationService,
    public translate: TranslateService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    public ngxSmartModalService: NgxSmartModalService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.showNofication();
    this.loadForm();
    this.getProfile();
  }

  get f() { return this.updateSettingForm.controls; }

  loadForm() {
    this.updateSettingForm = this.formBuilder.group({
      fullName: [''],
      email: [''],
      jobTitle: ['', Validators.required],
      summary: ['', Validators.required],
    });
  }

  getProfile() {
    const getToken = this.authenticationService.decodeToken();
    this.username = getToken.full_name;
    this.http.get(`${environment.baseUrl}/Profile/getprofile?user_id=${getToken.id}`, {}).subscribe((data: any) => {
      this.profileCV = data.cv_link;
      this.companyProfileImg = data.profile_image;
        this.updateSettingForm.patchValue({
          fullName: getToken.full_name,
          email: getToken.email,
          jobTitle: data.job_title,
          summary: data.summary,
        })
    }, (error) => {
        this.updateSettingForm.patchValue({
          fullName: getToken.full_name,
          email: getToken.email,
          jobTitle: '',
          summary: '',
        })
    });
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    const formData = new FormData();
    const getToken = this.authenticationService.decodeToken();
    if(this.selectedCVFile) {
      formData.append('cv', this.selectedCVFile, this.selectedCVFile.name);
    }

    if(this.selectedProfileImgFile && this.isProfileImageValid) {
      formData.append('profileImage', this.selectedProfileImgFile, this.selectedProfileImgFile.name);
    }
    formData.append('user_id', getToken.id);
    formData.append('fullName', this.updateSettingForm.get('fullName').value);
    formData.append('email', this.updateSettingForm.get('email').value);
    formData.append('jobTitle', this.updateSettingForm.get('jobTitle').value);
    formData.append('summary', this.updateSettingForm.get('summary').value);

    this.http.post(`${environment.baseUrl}/Profile/updateprofile`, formData ).subscribe((data: any) => {
      this.profileCV = data.cv_link;
      this.companyProfileImg = data.profile_image;

        this.updateSettingForm.patchValue({
          fullName: data.full_name,
          email: data.email,
          jobTitle: data.job_title,
          summary: data.summary
        })
      this.loading = false;
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
    });
  }

  openCV(){
    this.ngxSmartModalService.getModal('myModal').open();
  }

  onFileSelectedCv(event) {
    this.selectedCVFile = event.target.files[0];
  }

  onFileSelectedProfileImg(event) {
    this.selectedProfileImgFile = event.target.files[0];
    const Img = new Image();
    Img.src = URL.createObjectURL(this.selectedProfileImgFile);
    Img.onload = (e: any) => {
      const width = e.path[0].width;
      const height = e.path[0].height;

      if(width < 400 || height < 400) {
        return this.toastr.error('Your uploaded image size is wrong!', 'Wrong Size!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      } else {
        this.isProfileImageValid = true;
      }
    }
  }

  showNofication() {
    const decodedToken = this.authenticationService.decodeToken();
    this.authenticationService.getUserdetails(decodedToken.email).subscribe((data) => {
      if(data.verification_code == 1) {
        this.showAccountNotVerified = false;
      } else {
        this.showAccountNotVerified = true;
      }
    })
  }

  ngOnDestroy() {

  }

}
