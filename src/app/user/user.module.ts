import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { AppliedJobComponent } from './applied-job/applied-job.component';
import { AccountSettingComponent } from './account-setting/account-setting.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { MatIconModule, MatMenuModule, MatProgressSpinnerModule } from '@angular/material';
import { NgxDocViewerModule } from 'ngx-doc-viewer';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  declarations: [
    AppliedJobComponent,
    AccountSettingComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    PaginationModule,
    TranslateModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    FormsModule,
    MatMenuModule,
    MatIconModule,
    NgxDocViewerModule,
    NgxSmartModalModule,
    TabsModule.forRoot()
  ]
})
export class UserModule { }
