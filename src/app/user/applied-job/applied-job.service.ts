import { Injectable } from '@angular/core';
import { Subscription, of } from 'rxjs';
import { Job } from 'src/app/job/list/list.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AppliedJobService {

  jobs: Job[] = [
    {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi', 'Abuja'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago'
   }
  ];

  constructor(private http: HttpClient) { }

  getJob() {
    return of(this.jobs);
  }

  getAppliedJobs(user_id) {
    return this.http.get<any>
    (`${environment.baseUrl}/Job/myappliedjobs?user_id=${user_id}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  deleteAppliedJob(applicant_id) {
    return this.http.post<any>(`${environment.baseUrl}/Job/deleteappliedjobs`, {
      'checked': applicant_id
    })
    .pipe(map(result => {
            return result;
      }));
  }
}
