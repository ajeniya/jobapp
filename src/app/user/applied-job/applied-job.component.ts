import { Component, OnInit, ViewChild, EventEmitter, Output, Input, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Job } from 'src/app/job/list/list.model';
import { AppliedJobService } from './applied-job.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-applied-job',
  templateUrl: './applied-job.component.html',
  styleUrls: ['./applied-job.component.scss']
})
export class AppliedJobComponent implements OnInit,  OnDestroy {
  navBar: void;
  isJob: boolean;
  isCompany: boolean;
  disableJobPointer: boolean;
  checkTabsType: string;
  totalItems: number;
  subscriptions: Subscription[] = [];
  getJobSubscription: Subscription;
  getAppliedJobsSubscription: Subscription;
  deleteAppliedJobSubscription: Subscription;
  jobLists: Job[];
  appliedList: any;
  isLoadingResults: boolean;
  noAppliedList: boolean;
  userId: any;


  constructor(private eventServices: EventServicesService,
    private toastr: ToastrService, public dialog: MatDialog,
    public translate: TranslateService, public authServices: AuthenticationService,
    public data: AppliedJobService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.isJob = true;
    this.checkTabsType = 'isJob';
    this.totalItems = 40;
    this.getUserId();
  }

  getUserId() {
    const docodedToken = this.authServices.decodeToken();
    this.userId = docodedToken.id;
    this.getAppliedJobs();
  }

  getAppliedJobs() {
    this.isLoadingResults = true;
    this.getAppliedJobsSubscription = this.data.getAppliedJobs(this.userId).subscribe((data) => {
      this.appliedList = data;
      this.isLoadingResults = false;
    }, (error) => {
      this.noAppliedList = true;
      this.isLoadingResults = false;
    });
    this.subscriptions.push(this.getAppliedJobsSubscription);
  }

  deleteJob(jobId) {
    this.deleteAppliedJobSubscription = this.data.deleteAppliedJob(jobId)
     .subscribe((data: any) => {
      this.getAppliedJobs();
      this.toastr.error('Deleted successful!', 'Deleted!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.deleteAppliedJobSubscription);
  }

  onConfirmDelete(appliedId) {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Remove Applied Job',
        message: 'Are you sure, you want to delete applied job '
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.deleteJob(appliedId);
        }
      });
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
