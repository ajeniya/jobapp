import { ProfileComponent } from './../ui/profile/profile.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountSettingComponent } from './account-setting/account-setting.component';
import { AppliedJobComponent } from './applied-job/applied-job.component';
import { AuthGuard } from '../_helpers/auth.guard';

const routes: Routes = [
  { path: 'applied', component: AppliedJobComponent, canActivate: [AuthGuard]  },
  { path: 'setting', component: AccountSettingComponent, canActivate: [AuthGuard]  },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
