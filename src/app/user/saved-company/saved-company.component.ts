import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SavedCompanyService } from './saved-company.service';
import { CompanyLists } from 'src/app/company/list/companies-list-model';
import { EventServicesService } from 'src/app/services/event-services.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-saved-company',
  templateUrl: './saved-company.component.html',
  styleUrls: ['./saved-company.component.scss']
})
export class SavedCompanyComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  getCompanyListSubscription: Subscription;
  savedCompany: CompanyLists[];
  navBar: void;
  totalItems: number;

  constructor( private eventServices: EventServicesService,
    public translate: TranslateService,
    public mockdata: SavedCompanyService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.totalItems = 40;
    this.getCompanies();
  }

  getCompanies() {
    this.getCompanyListSubscription = this.mockdata.getCompanyList().subscribe((data: CompanyLists[]) => {
      this.savedCompany = data;
    })
    this.subscriptions.push(this.getCompanyListSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
