import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedCompanyComponent } from './saved-company.component';

describe('SavedCompanyComponent', () => {
  let component: SavedCompanyComponent;
  let fixture: ComponentFixture<SavedCompanyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedCompanyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedCompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
