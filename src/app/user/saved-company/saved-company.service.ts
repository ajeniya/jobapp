import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { CompanyLists } from 'src/app/company/list/companies-list-model';

@Injectable({
  providedIn: 'root'
})
export class SavedCompanyService {

  companyLists: CompanyLists[] = [
    {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Invitea',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Tech','Health Care']
   },
   {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Testfit',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Architecture, Tech']
   },
   {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Itemize',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': ['Architecture, Tech']
   },
   {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Teampay',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Architecture, Tech']
   },
   {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'PepsiCo',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Architecture, Tech']
   },
   {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Speedway',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Architecture, Tech']
   },    {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Lyft',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Architecture, Tech']
   },
   {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Coney Island prep',
    'company_location': ' Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Architecture, Tech']
   },
   {
    'company_id': '1',
    'company_proile_image': 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F1063%2Fabout_modules%2F85651%2Fb1e43b0e-c766-4017-b20e-5f532b0caf08.jpg%3Fv%3D84cfaef1c10e994b4a24dd829c84eb3b12cf1bbbfcd9045414a7f2680ce14a08&h=295&prog=1&w=400',
    'company_name': 'Aramark',
    'company_location': 'Chicago, Michigan',
    'company_keyword': 'Small Size',
    'company_category': [' Architecture, Tech']
   }
  ]
  navchange: any;

  constructor() { }

  getCompanyList() {
    return of(this.companyLists);
  }
}
