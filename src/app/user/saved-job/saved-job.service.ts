import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Job, Company } from 'src/app/job/list/list.model';

@Injectable({
  providedIn: 'root'
})
export class SavedJobService {

  jobs: Job[] = [
    {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi', 'Abuja'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago'
   }
  ];

  constructor() { }

  getJob() {
    return of(this.jobs);
  }

}
