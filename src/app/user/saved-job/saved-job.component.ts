import { Component, OnInit, ViewChild, EventEmitter, Output, Input, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { Job } from 'src/app/job/list/list.model';
import { SavedJobService } from './saved-job.service';

@Component({
  selector: 'app-saved-job',
  templateUrl: './saved-job.component.html',
  styleUrls: ['./saved-job.component.scss']
})
export class SavedJobComponent implements OnInit, OnDestroy {
  navBar: void;
  isJob: boolean;
  isCompany: boolean;
  disableJobPointer: boolean;
  checkTabsType: string;
  totalItems: number;
  subscriptions: Subscription[] = [];
  getJobSubscription: Subscription;
  jobLists: Job[];

  constructor(private eventServices: EventServicesService,
    public translate: TranslateService,
    public mockdata: SavedJobService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.isJob = true;
    this.checkTabsType = 'isJob';
    this.totalItems = 40;
    this.getJobs();
  }

  getJobs() {
    this.getJobSubscription = this.mockdata.getJob().subscribe((data) => {
      this.jobLists = data;
    });
    this.subscriptions.push(this.getJobSubscription);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
