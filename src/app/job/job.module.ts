import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AgmCoreModule } from '@agm/core';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';


import { JobRoutingModule } from './job-routing.module';
import { HomeComponent } from './home/home.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { TranslateModule } from '@ngx-translate/core';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { SearchComponent } from '../_components/search/search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [HomeComponent, ListComponent, DetailComponent, SearchComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TranslateModule,
    JobRoutingModule,
    FormsModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    ScrollToModule.forRoot(),
    TabsModule.forRoot()
  ]
})
export class JobModule { }
