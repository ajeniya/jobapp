import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from '../job/detail/detail.component';
import { HomeComponent } from '../job/home/home.component';
import { ListComponent } from '../job/list/list.component';
import { DetailComponent as CompanyDetails } from '../company/detail/detail.component';


const routes: Routes = [
  { path: 'job', component: HomeComponent },
  { path: 'detail', component: DetailComponent },
  { path: 'list', component: ListComponent },
  { path: 'company', component: CompanyDetails },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JobRoutingModule { }
