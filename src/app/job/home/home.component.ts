import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { TranslateService } from '@ngx-translate/core';
import { MockService } from './mock.service';
import { Subscription } from 'rxjs';
import { JobLists } from './job-list.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  navbarOpen = false;
  hideHeader = true;
  navBar: void;
  latitude = -28.68352;
  longitude = -147.20785;
  mapType = 'satellite';
  joblist: any;
  jobListSubscription: Subscription;
  subscriptions: Subscription[] = []

  constructor(private eventServices: EventServicesService, public translate: TranslateService, public mock: MockService) { }

  ngOnInit() {
    this.getJob();
    this.navBar = this.eventServices.navchange.emit(true);
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

  getJob() {
    this.jobListSubscription = this.mock.getJoblist().subscribe((data: JobLists[]) => {
      this.joblist = data;
      data.forEach(data => {
        const jobTitle = data.job_title.length;
        data['title_length'] = jobTitle;
      });
    });

    this.subscriptions.push(this.jobListSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
