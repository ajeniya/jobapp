import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { JobLists } from './job-list.model';

@Injectable({
  providedIn: 'root'
})
export class MockService {
  jobs: JobLists[] = [
    {
      job_id: '993843434',
      company_name: 'Andela',
      job_title: 'Software Engineer - Front End',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '99384343874',
      company_name: 'NTA',
      job_title: 'Devops',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '993434',
      company_name: 'Microsoft',
      job_title: 'Full Stack',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '993843434',
      company_name: 'Andela',
      job_title: 'Software Engineer - Front End',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '99384343874',
      company_name: 'NTA',
      job_title: 'Devops',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '993434',
      company_name: 'Microsoft',
      job_title: 'Full Stack',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '993843434',
      company_name: 'Andela',
      job_title: 'Software Engineer - Front End',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '99384343874',
      company_name: 'NTA',
      job_title: 'Principal Software Engineer - Front End',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    },
    {
      job_id: '993434',
      company_name: 'Microsoft',
      job_title: 'Full Stack',
      image: 'https://pilbox.themuse.com/image.jpg?url=https%3A%2F%2Fassets.themuse.com%2Fuploaded%2Fcompanies%2F744%2Fmodule_offices%2F12169%2Fad76eef1-f4b8-4596-a097-12d7ee4c67cd.jpg%3Fv%3D14f0504fc0d41c82c72c91d5d0d112aca95d359dc8c43ed48b1dbdc4bf4c9fcf&amp;h=295&amp;prog=1&amp;w=400"',
      city: [ 'Ghana', 'Cairo', 'Egypt', 'Kampala', 'Uganda', 'Kigali', 'Rwanda', 'Lagos', 'Nigeria', 'Nairobi', 'Kenya']
    }
  ];

  constructor() { }

  getJoblist() {
    return of(this.jobs);
  }
}
