export class JobLists {
  job_id: string;
  company_name: string;
  job_title: string;
  image: string;
  city: string[];
}
