import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { RecommendedJob } from './recommended.model';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MockJobDetailService {


  constructor(private http: HttpClient) { }

  getDetail(param) {
    return this.http.get<any>(`${environment.baseUrl}/Job/getbyid?job_id=${param}`, {})
    .pipe(map(result => {
            return result;
      }));
  }

  getSuggestion(param) {
    return this.http.get<any>(`${environment.baseUrl}/Job/getjoboccupation?occupation=${param}`, {})
    .pipe(map(result => {
            return result;
      }));
  }

  getUserProfile(user_id) {
    return this.http.get<any>(`${environment.baseUrl}/Profile/getprofile?user_id=${user_id}`, {})
    .pipe(map(result => {
            return result;
      }));
  }

}
