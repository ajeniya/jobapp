export class RecommendedJob {
  job_id: string;
  company_name: string;
  company_id: string;
  title: string;
  location: string;
  date_created: string;
}

export class JobDetail {
  job_id: string;
  company_id: string;
  title: string;
  description: string;
  type: string;
  status: string;
  plan_id: string;
  job_url: string;
  tags: string;
  job_email: string;
  submit_cv: string;
  negotiable: string;
  occupation: string;
  experience: string;
  required: string;
  location: string;
  min_salary: string;
  max_salary: string;
  company_name: string;
  logo: string;
  profile_image: string;
  about: string;
  date_created: Date;
}
