import { Component, OnInit, HostListener, Inject, OnDestroy } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations';
import { DOCUMENT } from '@angular/common';
import { EventServicesService } from 'src/app/services/event-services.service';
import { MockJobDetailService } from './mock-job-detail.service';
import { RecommendedJob, JobDetail } from './recommended.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  animations:[
    trigger('fade',
    [
      state('void', style({ opacity : 0})),
      transition(':enter',[ animate(300)]),
      transition(':leave',[ animate(500)]),
    ]
)]
})
export class DetailComponent implements OnInit, OnDestroy {
  public navBar: void;
  public selectedResumeName: string;
  public singleJob: JobDetail;
  subscriptions: Subscription[] = [];
  public recommendedList: RecommendedJob[];
  getRecommendedJobSubscription: Subscription;
  getJobDetailSubscription: Subscription;
  public getUserProfileSubscription: Subscription;
  applyForm: FormGroup;
  submitted = false;
  selectedCV: any;
  loading = false;
  category: string;
  isLoadingResults = true;
  pageNotFound = false;
  appliedWithProfileCV: any;
  resumeExist: boolean;

  constructor( @Inject(DOCUMENT) document,
    private eventServices: EventServicesService,
    public formBuilder: FormBuilder,
    private http: HttpClient,
    public authServices: AuthenticationService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    public data: MockJobDetailService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    const element = document.getElementById('hideHeader');
    element.classList.add('hide-header');
    this.getJob();
    this.loadApplyForm();
    this.getUserprofile();
  }

  @HostListener('window:scroll', ['$event'])
    onWindowScroll(e) {
       if (window.pageYOffset > 300) {
         const element = document.getElementById('hideHeader');
         element.classList.remove('hide-header');
       } else {
        const element = document.getElementById('hideHeader');
        element.classList.add('hide-header');
       }
    }

  onChange(targetValue){
    this.selectedResumeName = targetValue.target.files[0].name;
  }

  getUserprofile() {
    const decodedToken = this.authServices.decodeToken();
    if(decodedToken) {
      this.getUserProfileSubscription = this.data.getUserProfile(decodedToken.id).subscribe((data) => {
        if(data.cv_link) {
          this.resumeExist = true;
        } else {
          this.resumeExist = false;
        }
      });
    }
    this.subscriptions.push(this.getUserProfileSubscription);
  }

  loadApplyForm() {
    this.applyForm = this.formBuilder.group({
      fullname: ['', Validators.required],
      location: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      profilecv: [''],
      aboutApplicant: ['']
    });
  }

  get f() { return this.applyForm.controls; }

  onUploadCV(event) {
    this.selectedCV = event.target.files[0];
  }

  profilecv(event) {
    this.appliedWithProfileCV = event.checked;
  }

  onSubmit() {
    const decoded = this.authServices.decodeToken();
    if(this.appliedWithProfileCV || this.selectedCV ) {
      this.submitted = true;
      this.loading = true;
      const formData = new FormData();

      if(this.selectedCV) {
        formData.append('uplodedcv', this.selectedCV, this.selectedCV.name);
      }

      formData.append('company_id', this.singleJob.company_id);
      formData.append('job_id', this.singleJob.job_id);
      formData.append('user_id', decoded.id);
      formData.append('title', this.singleJob.title);
      formData.append('fullname', this.applyForm.value.fullname);
      formData.append('location', this.applyForm.value.location);
      formData.append('email', this.applyForm.value.email);
      formData.append('phone', this.applyForm.value.phone);
      formData.append('profilecv', this.applyForm.value.profilecv);
      formData.append('aboutApplicant', this.applyForm.value.aboutApplicant);

       this.http.post(`${environment.baseUrl}/Job/apply`, formData ).subscribe((data: any) => {
        Object.keys(this.applyForm.controls).forEach(controlName => {
          this.applyForm.controls[controlName].reset();
          this.applyForm.controls[controlName].setErrors(null);
        });
        this.loading = false;
          this.toastr.success('Your Application as been submitted!', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });
      }, (error) => {
        this.loading = false;
        this.toastr.error('You have previously applied to this job!', 'Error!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        })
      });
    } else {
      return this.toastr.error('Upload or select your resume!', 'Error!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    }

}

  getJob(){
    this.route.queryParams
      .subscribe(params => {
        this.isLoadingResults = true;
        const companyId = params.a;
        this.getJobDetailSubscription = this.data.getDetail(companyId).subscribe((data: JobDetail) => {
          this.singleJob = data;
          this.category = data.occupation;
          this.isLoadingResults = false;
          this.getRecommendedJobSubscription = this.data.getSuggestion(this.category).subscribe((result: any) => {
            this.recommendedList = result.data;
          })
        }, (error) => {
            this.pageNotFound = true;
            this.isLoadingResults = false;
        })
        this.subscriptions.push(this.getRecommendedJobSubscription);
        this.subscriptions.push(this.getJobDetailSubscription);
      })
  }

  onWork(event) {
    console.log(event);
  }


  goToLink(url: string){
    window.open(url, "_blank");
}

  ngOnDestroy() {

  }

}
