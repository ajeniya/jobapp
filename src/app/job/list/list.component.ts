import { Component, OnInit, ViewChild, EventEmitter, Output, Input, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { MockListService } from './mock-list.service';
import { Subscription } from 'rxjs';
import { Job, Company } from './list.model';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  navBar: void;
  isJob: boolean;
  isCompany: boolean;
  disableJobPointer: boolean;
  disableCompanyPointer: boolean;
  checkTabsType: string;
  viewCompany: boolean;
  totalItems: number;
  subscriptions: Subscription[] = [];
  getJobSubscription: Subscription;
  getCompanySubscription: Subscription;
  getAllJobSubscription: Subscription;
  getCompanySearchSubscription: Subscription;
  jobLists: Job[];
  getCompanies: any;
  isLoadingResults: boolean;
  searchFound: boolean;
  setPageIndex: any;
  resultsLength: any;
  noRecord: boolean;
  pageSize = 10;
  companyResultsLength: any;
  companySetPageIndex: any;
  companyPageSize = 10;
  isLoadingCompanyResults: boolean;
  clearSearch: boolean;
  public searchFieldJobType = {
    jobTitle: '',
    location: '',
    category: '',
    jobType: '',
    companysize: ''
  }

  public searchCompany = {
    companyName: '',
    companyLocation: ''
  }
  clearCompanySearch: boolean;


  constructor(private eventServices: EventServicesService,
    private toastr: ToastrService, private route: ActivatedRoute,
    public data: MockListService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.isJob = true;
    this.checkTabsType = 'isJob';
    this.getCompany();
    this.getAllApprovedListJobs();
    this.searchFromHomepage();
  }

  searchFromHomepage() {
    this.route.queryParams
    .subscribe(params => {
      if(params.jobname || params.location) {
        this.searchFieldJobType = {
          jobTitle: params.jobname != undefined ? params.jobname : '',
          location: params.location != undefined ? params.location : '',
          category: '',
          jobType: '',
          companysize: ''
        }
        this.searchJobWithField();
      }
    });
  }

  searchJobWithField() {
    this.isLoadingResults = true;
    this.searchFound = false;
    this.clearSearch = true;

    if(!this.setPageIndex) {
      this.setPageIndex = 1
    }

    this.getCompanySearchSubscription = this.data.searchJob(this.setPageIndex, this.pageSize, this.searchFieldJobType)
      .subscribe((data: any) => {
      this.jobLists = data.result;
      this.resultsLength = data.paginationObject.totalRecord;
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noRecord = true;
    }, (error) => {
      this.isLoadingResults = false;
      this.resultsLength = 0;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getCompanySearchSubscription);
  }


  searchCompanyField() {
    this.isLoadingResults = true;
    this.searchFound = false;
    this.clearCompanySearch = true;

    if(!this.setPageIndex) {
      this.setPageIndex = 1
    }

    this.getCompanySubscription = this.data.searchCompanies(this.setPageIndex, this.pageSize, this.searchCompany)
     .subscribe((data: any) => {
      this.getCompanies = data.result;
      this.companyResultsLength = data.paginationObject.totalRecord;
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noRecord = true;
       }, (error) => {
        this.isLoadingResults = false;
        this.companyResultsLength = 0;
        this.toastr.error('No record found!', 'No Record!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      });

    this.subscriptions.push(this.getCompanySubscription);
}


clearSearchJob() {
  this.searchFieldJobType = {
    jobTitle: '',
    location: '',
    category: '',
    jobType: '',
    companysize: ''
  }
  this.searchJobWithField();
  this.clearSearch = false;
}

clearSearchCompanies() {
  this.searchCompany = {
    companyName: '',
    companyLocation: ''
  }
  this.searchCompanyField();
  this.clearCompanySearch = false;
}

  jobsTab() {
    this.isJob = true;
    this.isCompany = false;
    this.disableJobPointer = false;
    this.checkTabsType = 'isJob';
    this.getAllApprovedListJobs();
  }

  getCompany() {
    this.isLoadingCompanyResults = true;
    this.searchFound = false;

    if(!this.companySetPageIndex) {
      this.companySetPageIndex = 1
    }

    this.getCompanySubscription = this.data.getCompanies(this.companySetPageIndex, this.companyPageSize)
    .subscribe((data: any) => {
     this.getCompanies = data.result;
     this.companyResultsLength = data.paginationObject.totalRecord;
     this.searchFound = true;
     this.isLoadingCompanyResults = false;
     this.noRecord = true;
     }, (error) => {
      this.isLoadingCompanyResults = false;
      this.companyResultsLength = 0;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
     });

    this.subscriptions.push(this.getCompanySubscription);
  }

  getAllApprovedListJobs() {
    this.isLoadingResults = true;
    this.searchFound = false;

    if(!this.setPageIndex) {
      this.setPageIndex = 1
    }

    this.getAllJobSubscription = this.data.getAllApproveJobList(this.setPageIndex, this.pageSize)
     .subscribe((data: any) => {
      this.jobLists = data.result;
      this.resultsLength = data.paginationObject.totalRecord;
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noRecord = true;
    }, (error) => {
      this.isLoadingResults = false;
      this.resultsLength = 0;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getAllJobSubscription);
  }

  pageChanged(event) {
    this.setPageIndex = event.page;
    this.pageSize = event.itemsPerPage;
    this.getAllApprovedListJobs();
  }

  companyPageChanged(event) {
    this.companySetPageIndex = event.page;
    this.companyPageSize = event.itemsPerPage;
    this.getCompany();
  }


  companiesTab() {
    this.isJob = false;
    this.isCompany = true;
    this.disableCompanyPointer = false;
    this.checkTabsType = 'isCompany';
    this.getCompany();
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }


}
