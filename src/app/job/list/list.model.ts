export class Job {
  job_id: string;
  company_name: string;
  job_title: string;
  applied_user?: number;
  location: string[];
  formated_location?: string[];
  created_at: string;
}


export class Company {
  company_id: string;
  company_name: string;
  company_image: string;
  company_location: string[];
  company_sector?: string[];
  company_type: string;
}
