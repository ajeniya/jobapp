import { Injectable } from '@angular/core';
import { Job, Company } from './list.model';
import { of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class MockListService {
  constructor(private http: HttpClient) { }

  getAllApproveJobList(pageNo, pageSize) {
    return this.http.get<any>
    (`${environment.baseUrl}/Job/getalljoblist?pageNo=${pageNo}&pageSize=${pageSize}&status=1`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getCompanies(pageNo, pageSize) {
    return this.http.get<any>
    (`${environment.baseUrl}/Company/getallcompnylist?pageNo=${pageNo}&pageSize=${pageSize}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  searchJob(pageNo?, pageSize?, search?){
    return this.http
    .get<any>(`${environment.baseUrl}/Job/searchjobsList?pageNo=${pageNo}&pageSize=${pageSize}&location=${search.location}&occupation=${search.category}&type=${search.jobType}&companyType=${search.companysize}&title=${search.jobTitle}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  searchCompanies(pageNo?, pageSize?, search?){
    return this.http
    .get<any>(`${environment.baseUrl}/Company/searchcompanies?pageNo=${pageNo}&pageSize=${pageSize}&companyName=${search.companyName}&companyLocation=${search.companyLocation}`, {})
    .pipe(map(result => {
        return result;
    }));
  }
}
