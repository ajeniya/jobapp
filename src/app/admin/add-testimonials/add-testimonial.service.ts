import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AddTestimonialService {

  constructor(private http: HttpClient) { }

  addTestimonial(formData){
    return this.http.post<any>(`${environment.baseUrl}/Testimonial/addtestimonial`,
    {
      'testimonialBy': formData.testimonialBy,
      'testimonial': formData.testimonial,
      'companyDesignation': formData.companyDesignation,
      'isDefault': formData.default,
      'isActive': formData.active,
    }).pipe(map(result => {
            return result;
      }));
  }
}
