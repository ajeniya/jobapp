import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AddTestimonialService } from './add-testimonial.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-testimonials',
  templateUrl: './add-testimonials.component.html',
  styleUrls: ['./add-testimonials.component.scss']
})
export class AddTestimonialsComponent implements OnInit, OnDestroy {
  navBar: void;
  addTestimonialsForm: FormGroup;
  addTestimonialSubscription: Subscription;
  subscriptions: Subscription[] = [];
  submitted = false;
  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]
  };

  loading = false;


  constructor(private eventServices: EventServicesService,
    public data: AddTestimonialService, private router: Router,
    private toastr: ToastrService, private route: ActivatedRoute,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.loadForm();
  }

  loadForm() {
    this.addTestimonialsForm  =  this.formBuilder.group({
      testimonialBy: ['', Validators.required],
      testimonial: ['', Validators.required],
      companyDesignation: ['', Validators.required],
      default: [''],
      active: ['']
    });
  }

  get f() { return this.addTestimonialsForm.controls; }

  addTestimonial() {
    this.submitted = true;
    const value = this.addTestimonialsForm.value;
    this.submitted = true;
    this.loading = true;
    this.addTestimonialSubscription = this.data.addTestimonial(value).subscribe((data: any) => {
      this.router.navigate(['/admin//list-testimonies/']);
      this.toastr.success('Added successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
      Object.keys(this.addTestimonialsForm.controls).forEach(controlName => {
        this.addTestimonialsForm.controls[controlName].reset();
        this.addTestimonialsForm.controls[controlName].setErrors(null);
      });
    });

    this.subscriptions.push(this.addTestimonialSubscription);
  }

  reset() {
    this.loadForm();
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
