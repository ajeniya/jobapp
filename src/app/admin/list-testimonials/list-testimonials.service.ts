import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListTestimonialsService {

  constructor(private http: HttpClient) { }

  getTestimonials(pageNo?, pageSize?) {
    return this.http.get<any>(`${environment.baseUrl}/Testimonial/gettestimonial?pageNo=${pageNo}&pageSize=${pageSize}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  deleteCheckedTestimony(checked) {
    return this.http.post<any>(`${environment.baseUrl}/Testimonial/deletetestimonial`, {
      'checked': checked
    })
    .pipe(map(result => {
        return result;
    }));
  }

  updateTestimony(payload, testimonial_id) {
    return this.http.post<any>(`${environment.baseUrl}/Testimonial/UpdateTestimonial`, {
      'testimonial_id': testimonial_id,
      'testimonialBy': payload.testimonialBy,
      'testimonial': payload.testimonial,
      'companyDesignation': payload.companyDesignation,
      'isDefault': payload.default,
      'isActive': payload.active
    })
    .pipe(map(result => {
        return result;
    }));
  }

  getSearch(pageNo?, pageSize?, testimonialBy?, isActive?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Testimonial/searchtestimonial?pageNo=${pageNo}&pageSize=${pageSize}&testimonialBy=${testimonialBy}&isActive=${isActive}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

}

export class Testimonial {
  testimonial_id?: string;
  testimonialBy?: string;
  testimonial?: string;
  companyDesignation?: string;
  isDefault?: string;
  isActive?: string;
}
