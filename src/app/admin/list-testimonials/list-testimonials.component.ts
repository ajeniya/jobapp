import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent, MatDialog } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Testimonial, ListTestimonialsService } from './list-testimonials.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-list-testimonials',
  templateUrl: './list-testimonials.component.html',
  styleUrls: ['./list-testimonials.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(90%)'}),
        animate('400ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ListTestimonialsComponent implements OnInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  testimonial: Testimonial[];
  getTestimonialSubscription: Subscription;
  deleteCheckedSubscription: Subscription;
  updateSubscription: Subscription;
  getSearchSubscription: Subscription;
  searchForm: FormGroup;
  editTestimonialsForm: FormGroup;

  displayedColumns: string[] = ['select','testimonialBy', 'testimonial', 'companyDesignation', 'isDefault','isActive','action'];
  selection = new SelectionModel<Testimonial>(true, []);

  public dataSource = new MatTableDataSource<Testimonial>();
  resultsLength: number;
  paginationMeta: any;
  submitted: boolean;
  dateValidationError: string;
  checkedBtn: boolean;

  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]
  };

  isLoadingResults = false;
  searchFound: boolean;
  setPageIndex: any;
  checkedDeleteLoader = false;
  checkedId = [];
  selectedEmoji: string;
  isSearch = false;
  updateLoading: boolean;
  showEditTestimonyPanel: boolean;
  testimonialModel: any = {};
  testimonial_id: any;
  noTestimonyRecord: boolean;
  searchLoading: boolean;


  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router, public dialog: MatDialog,
    private cdref: ChangeDetectorRef, public data: ListTestimonialsService) {

    }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.createForm();
    this.loadEditForm();
  }

  ngAfterViewInit() {
    this.getTestimonial();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  // convenience getter for easy access to form fields
  get f() { return this.searchForm.controls; }

  getTestimonial() {
    this.isLoadingResults = true;
    this.searchFound = false;
    if(this.paginator.pageIndex) {
      this.setPageIndex = this.paginator.pageIndex + 1;
    } else {
      this.setPageIndex = 1;
    }
    this.getTestimonialSubscription = this.data.getTestimonials(this.setPageIndex, this.paginator.pageSize)
     .subscribe((data: any) => {
      // this.countries = data;
      this.resultsLength = data.paginationObject.totalRecord;
      this.dataSource = new MatTableDataSource<Testimonial>(data.result);
      this.dataSource.sort = this.sort;
      this.selection.clear();
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noTestimonyRecord = false;
      this.selection.selected.length = 0;
    }, (error) => {
      this.noTestimonyRecord = true;
      this.isLoadingResults = false;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getTestimonialSubscription);
  }

  onConfirmDelete() {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Remove Testimoney',
        message: 'Are you sure, you want to delete testimony '
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.onDeleteChecked();
        }
      });
  }


  onDeleteChecked() {
    this.checkedDeleteLoader = true;
    this.isSearch = false;
    const getChecked = this.selection.selected;

    getChecked.map((data) => {
      this.checkedId.push(data.testimonial_id);
    });

    this.deleteCheckedSubscription = this.data.deleteCheckedTestimony(this.checkedId).subscribe((data) => {
      this.getTestimonialSubscription = this.data.getTestimonials(
        this.setPageIndex, this.paginator.pageSize)
        .subscribe((data: any) => {
          // this.countries = data;
          this.resultsLength = data.paginationObject.totalRecord;
          this.dataSource = new MatTableDataSource<Testimonial>(data.result);
          this.dataSource.sort = this.sort;
          this.selection.clear();
          this.searchForm.reset();
          this.noTestimonyRecord = false;

          this.toastr.success('Deleted successful!', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });

          this.checkedDeleteLoader = false;
          this.checkedId.length = 0;
    });
    })

    this.subscriptions.push(this.getTestimonialSubscription);
    this.subscriptions.push(this.deleteCheckedSubscription);
  }

  editTestimony(element) {
    this.showEditTestimonyPanel = true;
    this.testimonial_id = element.testimonial_id;
    this.editTestimonialsForm.patchValue({
      testimonialBy: element.testimonialBy,
      testimonial: element.testimonial,
      companyDesignation: element.companyDesignation,
      default: element.isDefault,
      active: element.isActive,
    })

  }

  createForm() {
    this.searchForm = this.formBuilder.group({
      testimonial: [''],
      isDefault: ['']
    });
  }

  clearSearch() {
    this.isSearch = false;
    this.setPageIndex = 1;
    this.getTestimonial();
    this.searchForm.reset();
  }

  onSearch() {
    this.submitted = true;
    this.searchLoading = true;
    this.setPageIndex = 1;
    this.selection.selected.length = 0;
    this.getSearchSubscription =
    this.data.getSearch(this.setPageIndex, this.paginator.pageSize,
      this.searchForm.value.testimonial, this.searchForm.value.isDefault ).subscribe((data: any) => {
      this.resultsLength = data.paginationObject.totalRecord;
      this.dataSource = new MatTableDataSource<Testimonial>(data.result);
      this.dataSource.sort = this.sort;
      this.selection.clear();
      this.searchLoading = false;
      this.isSearch = true;
      this.searchFound = true;
    }, (error) => {
      this.searchFound = false;
      this.searchLoading = false;
      this.isSearch = true;
      this.resultsLength = 0;
    });

    this.subscriptions.push(this.getSearchSubscription);

  }

  updateTestimony() {
    this.submitted = true;
      this.updateLoading = true;
      this.updateSubscription = this.data.updateTestimony(this.editTestimonialsForm.value, this.testimonial_id).subscribe((data: any) => {
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
        this.updateLoading = false;
        this.showEditTestimonyPanel = false;
        this.getTestimonial();
      });

      this.subscriptions.push(this.updateSubscription);

  }

  deleteSingleTestimony(element) {
    this.checkedId.push(element.testimonial_id);
    this.onConfirmDelete();
  }


  loadEditForm() {
    this.editTestimonialsForm  =  this.formBuilder.group({
      testimonialBy: ['', Validators.required],
      testimonial: ['', Validators.required],
      companyDesignation: ['', Validators.required],
      default: [''],
      active: ['']
    });
  }

  get editForm() { return this.editTestimonialsForm.controls; }


  getNext(event: PageEvent) {
    this.getTestimonial();
  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    if (numSelected > 0) {
      this.checkedBtn = true;
    } else {
      this.checkedBtn = false;
    }
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Testimonial): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.testimonial_id + 1}`;
  }

  navigateTo(row: any) {
    this.router.navigate(['/admin/'+row.id]);
  }

  onClickedOutside(e: Event) {
    if(this.showEditTestimonyPanel == true) {
      this.showEditTestimonyPanel = false;
    } else {
      this.showEditTestimonyPanel = true;
    }

  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
