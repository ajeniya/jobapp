import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy, HostListener, ElementRef } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Country, ManageCountryService } from './manage-country.service';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { trigger, transition, animate, style } from '@angular/animations'
@Component({
  selector: 'app-manage-country',
  templateUrl: './manage-country.component.html',
  styleUrls: ['./manage-country.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(90%)'}),
        animate('400ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ManageCountryComponent implements OnInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  getCountriesSubscription: Subscription;
  deleteCheckedSubscription: Subscription;
  updateSubscription: Subscription;
  getSearchSubscription: Subscription;
  countries: any;
  searchForm: FormGroup;
  pageNo: any;

  countryModel: any = {};
  updateLoading = false;
  searchLoading = false;
  showEditCountryPanel = false;

  displayedColumns: string[] = ['select','sortname', 'name', 'phoneCode','action'];
  selection = new SelectionModel<Country>(true, []);

  public dataSource = new MatTableDataSource<Country>();
  resultsLength: number;
  paginationMeta: any;
  submitted: boolean;
  dateValidationError: string;
  checkedBtn: boolean;
  setPageIndex: any;
  checkedDeleteLoader = false;
  checkedId = [];
  selectedEmoji: string;
  isSearch = false;
  searchFound: boolean;
  isLoadingResults = true;


  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router,
    public dialog: MatDialog, public data: ManageCountryService) {

    }

    ngOnInit(): void {
      this.navBar = this.eventServices.navchange.emit(true);
      this.createForm();
    }


    ngAfterViewInit() {
      this.getCountries();
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }

    // convenience getter for easy access to form fields
    get f() { return this.searchForm.controls; }

    getCountries() {
      this.isLoadingResults = true;
      this.searchFound = false;
      if(this.paginator.pageIndex) {
        this.setPageIndex = this.paginator.pageIndex + 1;
      } else {
        this.setPageIndex = 1;
      }
      this.getCountriesSubscription = this.data.getCountries(this.setPageIndex, this.paginator.pageSize)
       .subscribe((data: any) => {
        this.countries = data;
        this.resultsLength = data.paginationObject.totalRecord;
        this.dataSource = new MatTableDataSource<Country>(data.result);
        this.dataSource.sort = this.sort;
        this.selection.clear();
        this.searchFound = true;
        this.isLoadingResults = false;
      });

      this.subscriptions.push(this.getCountriesSubscription);
    }

    clearSearch() {
      this.isSearch = false;
      this.setPageIndex = 1;
      this.getCountries();
      this.searchForm.reset();
    }


    createForm() {
      this.searchForm = this.formBuilder.group({
        name: [''],
        countryCode: ['']
      });
    }

    getNext(event: PageEvent) {
      this.getCountries();
    }

    onSearch() {
      this.submitted = true;
      this.searchLoading = true;
      this.setPageIndex = 1;
      this.selection.selected.length = 0;
      this.getSearchSubscription =
      this.data.getSearch(this.setPageIndex, this.paginator.pageSize,
        this.searchForm.value.name, this.searchForm.value.countryCode ).subscribe((data: any) => {
        this.countries = data;
        this.resultsLength = data.paginationObject.totalRecord;
        this.dataSource = new MatTableDataSource<Country>(data.result);
        this.dataSource.sort = this.sort;
        this.selection.clear();
        this.searchLoading = false;
        this.isSearch = true;
        this.searchFound = true;
      }, (error) => {
        this.toastr.error('No record found!', 'No Record!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        })
        this.searchFound = false;
        this.searchLoading = false;
        this.resultsLength = 0;
        this.isSearch = true;
      });

      this.subscriptions.push(this.getSearchSubscription);

    }

    onConfirmDeleteCountry() {
      let dialog = this.dialog.open(DialogBodyComponent, {
        data: {
          title: 'Confirm Remove Country',
          message: 'Are you sure, you want to delete country '
        }
      });
      dialog.afterClosed()
        .subscribe(selection => {
          if (selection) {
            this.onDeleteChecked();
          }
        });
    }


    editCountry(element) {
      this.showEditCountryPanel = true;
      this.countryModel.sortnamecode = element.sortname;
      this.countryModel.country = element.name;
      this.countryModel.phoneCode = element.phonecode;
      this.countryModel.id = element.id;
    }

    onUpdateCountry() {
      this.submitted = true;
        this.updateLoading = true;
        this.updateSubscription = this.data.updateCountry(this.countryModel).subscribe((data: any) => {
          this.toastr.success('Updated successful!', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });
          this.updateLoading = false;
          this.showEditCountryPanel = false;
          this.getCountries();
        });

        this.subscriptions.push(this.updateSubscription);

    }

    deleteSingleCountry(data) {
      this.checkedId.push(data.id);
      this.onConfirmDeleteCountry();
    }

    onDeleteChecked() {
      this.checkedDeleteLoader = true;
      const getChecked = this.selection.selected;

      getChecked.map((data) => {
        this.checkedId.push(data.id);
      });

      this.deleteCheckedSubscription = this.data.deleteCheckedCountries(this.checkedId).subscribe((data) => {
        this.getCountriesSubscription = this.data.getCountries(
          this.setPageIndex, this.paginator.pageSize)
          .subscribe((data: any) => {
            this.countries = data;
            this.resultsLength = data.paginationObject.totalRecord;
            this.dataSource = new MatTableDataSource<Country>(data.result);
            this.dataSource.sort = this.sort;
            this.selection.clear();
            this.searchForm.reset();

            this.toastr.success('Deleted successful!', 'Success!', {
              enableHtml: true,
              closeButton: true,
              timeOut: 3000
            });

            this.checkedDeleteLoader = false;
            this.checkedId.length = 0;
      });
      })

      this.subscriptions.push(this.getCountriesSubscription);
      this.subscriptions.push(this.deleteCheckedSubscription);
    }

    /** Whether the number of selected elements matches the total number of rows. */
    isAllSelected() {
      const numSelected = this.selection.selected.length;
      if (numSelected > 0) {
        this.checkedBtn = true;
      } else {
        this.checkedBtn = false;
      }
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.data.forEach(row => this.selection.select(row));
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: Country): string {
      if (!row) {
        return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.name + 1}`;
    }


    ngOnDestroy() {
      this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    }


}
