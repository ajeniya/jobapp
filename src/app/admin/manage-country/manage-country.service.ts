import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class ManageCountryService {

  constructor(private http: HttpClient) { }

  getCountries(pageNo?, pageSize?) {
    return this.http.get<any>(`${environment.baseUrl}/Location/countries?pageNo=${pageNo}&pageSize=${pageSize}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getCountriesList() {
    return this.http.get<any>(`${environment.baseUrl}/Location/getcountrylist`, {})
    .pipe(map(result => {
        return result;
    }));
  }


  getSearch(pageNo?, pageSize?, name?, countryCode?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Location/searchcountry?pageNo=${pageNo}&pageSize=${pageSize}&name=${name}&countryCode=${countryCode}`, {})
    .pipe(map(result => {
        return result;
    }));
  }


  deleteCheckedCountries(checked) {
    return this.http.post<any>(`${environment.baseUrl}/Location/deleteChecked`, {
      'checked': checked
    })
    .pipe(map(result => {
        return result;
    }));
  }

  updateCountry(payload) {
    return this.http.post<any>(`${environment.baseUrl}/Location/updatecountry`, {
      'country_id': payload.id,
      'sortname': payload.sortnamecode,
      'name': payload.country,
      'phonecode': payload.phoneCode,
    })
    .pipe(map(result => {
        return result;
    }));
  }
}

export class Country {
  id: number;
  sortname: string;
  name: string;
  phoneCode?: number;
}
