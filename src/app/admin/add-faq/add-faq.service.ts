import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AddFaqService {

  constructor(private http: HttpClient) { }

  addFaq(formData){
    return this.http.post<any>(`${environment.baseUrl}/Faq/addfaq`,
    {
      'question': formData.question,
      'answer': formData.answer,
      'status': formData.status
    }).pipe(map(result => {
            return result;
      }));
  }
}
