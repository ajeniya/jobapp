import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AddFaqService } from './add-faq.service';

@Component({
  selector: 'app-add-faq',
  templateUrl: './add-faq.component.html',
  styleUrls: ['./add-faq.component.scss']
})
export class AddFaqComponent implements OnInit {
  subscriptions: Subscription[] = [];
  addFaqSubscription: Subscription;
  navBar: void;
  addFaqsForm: FormGroup;
  submitted = false;
  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]
  };
  loading: boolean;

  constructor(private eventServices: EventServicesService, public data: AddFaqService,
    private toastr: ToastrService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.loadForm();
  }

  loadForm() {
    this.addFaqsForm  =  this.formBuilder.group({
      question: ['', Validators.required],
      answer: ['', Validators.required],
      status: ['', Validators.required],
    });
  }

  get f() { return this.addFaqsForm.controls; }

  addFaqs() {
    this.submitted = true;
    const value = this.addFaqsForm.value;
    this.submitted = true;
    this.loading = true;
    this.addFaqSubscription = this.data.addFaq(this.addFaqsForm.value).subscribe((data: any) => {
      this.toastr.success('Added successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.addFaqSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
