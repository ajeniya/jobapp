import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { SharedModule } from '../shared/shared.module';
import { ListAdminUserComponent } from './list-admin-user/list-admin-user.component';
import { EditAdminUserComponent } from './edit-admin-user/edit-admin-user.component';
import { ListJobsComponent } from './list-jobs/list-jobs.component';
import { ListCompanyComponent } from './list-company/list-company.component';
import { FaqListsComponent } from './faq-lists/faq-lists.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { AddTestimonialsComponent } from './add-testimonials/add-testimonials.component';
import { ListTestimonialsComponent } from './list-testimonials/list-testimonials.component';
import { AddCountriesComponent } from './add-countries/add-countries.component';
import { AddStatesComponent } from './add-states/add-states.component';
import { ListStatesComponent } from './list-states/list-states.component';
import { ListCitiesComponent } from './list-cities/list-cities.component';
import { AddCitiesComponent } from './add-cities/add-cities.component';
import { AddPackagesComponent } from './add-packages/add-packages.component';
import { ListSubscriptionComponent } from './list-subscription/list-subscription.component';
import { UnapprovedJobsComponent } from './unapproved-jobs/unapproved-jobs.component';
import { BannedJobComponent } from './banned-job/banned-job.component';
import { ExpiredJobsComponent } from './expired-jobs/expired-jobs.component';
import { FlagedJobsComponent } from './flaged-jobs/flaged-jobs.component';
import { SettingComponent } from './setting/setting.component';
import { AddAdminUserComponent } from './add-admin-user/add-admin-user.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { TranslateModule } from '@ngx-translate/core';
import { MatTableModule, MatCheckboxModule, MatProgressSpinnerModule,
         MatPaginatorModule, MatSortModule, MatMenuModule, MatIconModule,
         MatChipsModule, MatButtonModule, MatFormFieldModule, MatInputModule,
         MatRippleModule, MatRadioModule, MatBadgeModule, MatDatepickerModule,
         MatNativeDateModule, MatTabsModule, MatTooltipModule, MatExpansionModule,
         MatDialogModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { RichTextEditorAllModule } from '@syncfusion/ej2-angular-richtexteditor';
import { ManageCountryComponent } from './manage-country/manage-country.component';
import { ManageAdsComponent } from './setting/manage-ads/manage-ads.component';
import { PaymentGatewayComponent } from './setting/payment-gateway/payment-gateway.component';
import { SocialComponent } from './setting/social/social.component';
import { SocialLoginComponent } from './setting/social-login/social-login.component';
import { MainSettingComponent } from './setting/main-setting/main-setting.component';
import { EmailSettingComponent } from './setting/email-setting/email-setting.component';
import { EmailTemplateComponent } from './setting/email-template/email-template.component';
import { AddPlanComponent } from './add-plan/add-plan.component';
import { ListPlanComponent } from './list-plan/list-plan.component';
import { DialogBodyComponent } from '../_components/search/dialog-body/dialog-body.component';
@NgModule({
  declarations: [AdminHomeComponent, AdminMenuComponent, ListAdminUserComponent,
     EditAdminUserComponent, ListJobsComponent, ListCompanyComponent, FaqListsComponent,
     AddFaqComponent, AddTestimonialsComponent, ListTestimonialsComponent,
     AddCountriesComponent, AddStatesComponent, ListStatesComponent,
     ListCitiesComponent, AddCitiesComponent, AddPackagesComponent,
     ListSubscriptionComponent, UnapprovedJobsComponent,
     BannedJobComponent, ExpiredJobsComponent, FlagedJobsComponent, SettingComponent,
     AddAdminUserComponent, ManageCountryComponent,
     ManageAdsComponent, PaymentGatewayComponent, SocialComponent,
     SocialLoginComponent, MainSettingComponent, EmailSettingComponent, EmailTemplateComponent,
     AddPlanComponent, ListPlanComponent, DialogBodyComponent],
  imports: [
    SharedModule,
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    PaginationModule,
    TranslateModule,
    MatTableModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MatChipsModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatRadioModule,
    MatBadgeModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTabsModule,
    MatDialogModule,
    MatTooltipModule,
    MatExpansionModule,
    RichTextEditorAllModule,
   ToastrModule.forRoot()
  ],
  providers: [
    MatDatepickerModule,
    CurrencyPipe
  ],
  entryComponents: [DialogBodyComponent]
})
export class AdminModule { }

