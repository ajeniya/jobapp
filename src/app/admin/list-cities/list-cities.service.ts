import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ListCitiesService {
  constructor(private http: HttpClient) { }

  getCities(pageNo?, pageSize?, state_id?) {
    return this.http.get<any>(`${environment.baseUrl}/Location/cities?pageNo=${pageNo}&pageSize=${pageSize}&state_id=${state_id}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getCountries() {
    return this.http.get<any>(`${environment.baseUrl}/Location/getcountrylist`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getStateList(countryID?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Location/getstatelist?countryID=${countryID}`, {})
    .pipe( map(result => {
        return result;
    }));
  }

  deleteCheckedCity(checked) {
    return this.http.post<any>(`${environment.baseUrl}/Location/deleteCheckedCity`, {
      'checked': checked
    })
    .pipe(map(result => {
        return result;
    }));
  }

  updateCity(payload, cityId) {
    return this.http.post<any>(`${environment.baseUrl}/Location/updatecity`, {
      'name': payload,
      'city_id': cityId
    })
    .pipe(map(result => {
        return result;
    }));
  }

}

export class City {
  cityId: number;
  cityName: string;
  countryId: number;
  countryName: string;
  stateId: number;
  stateName: string;
}
