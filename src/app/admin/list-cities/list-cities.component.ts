import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent, MatDialog } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { ListStateService } from '../list-states/list-state.service';
import { City, ListCitiesService } from './list-cities.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { trigger, transition, animate, style } from '@angular/animations'
import { MainSettingService } from '../setting/main-setting/main-setting.service';

@Component({
  selector: 'app-list-cities',
  templateUrl: './list-cities.component.html',
  styleUrls: ['./list-cities.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(90%)'}),
        animate('400ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ListCitiesComponent implements OnInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  getStatesSubscription: Subscription;
  getCountriesSubscription: Subscription;
  getCitiesSubscription: Subscription;
  deleteCheckedSubscription: Subscription;
  updateSubscription: Subscription;
  SettingSubscription: Subscription;
  states: any;
  countriesList: any;
  cities: City[];
  searchForm: FormGroup;
  showEditCityPanel = false;
  cityModel: any = {};

  displayedColumns: string[] = ['select','country', 'city', 'state','action'];
  selection = new SelectionModel<City>(true, []);

  public dataSource = new MatTableDataSource<any>();
  resultsLength: number;
  setPageIndex: any;
  searchFound: boolean;
  isLoadingResults = true;
  paginationMeta: any;
  submitted: boolean;
  dateValidationError: string;
  checkedBtn: boolean;
  state_id: number;
  selectedCountryId: any;
  pageSize: any;
  noCityRecord = false;
  checkedDeleteLoader = false;
  isSearch = false;
  checkedId = [];
  updateLoading = false;
  cityId: any;

  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router, public cityData: ListCitiesService,
    public dialog: MatDialog, public siteSetting: MainSettingService,
    public mockdata: ListStateService) {

    }

    ngOnInit(): void {
      this.navBar = this.eventServices.navchange.emit(true);
      this.createForm();
      this.getCountries();

    }

    ngAfterViewInit() {
      this.SettingSubscription = this.siteSetting.getSetting().subscribe((data) => {
        this.state_id = data.defaultState;
        this.getCities();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });

      this.subscriptions.push(this.SettingSubscription);
    }

    // convenience getter for easy access to form fields
    get f() { return this.searchForm.controls; }

    getCountries() {
      this.getCountriesSubscription = this.cityData.getCountries().subscribe((data: any) => {
          this.countriesList = data;
      });

      this.subscriptions.push(this.getCountriesSubscription);
    }

    getStates(countryId) {
      this.getStatesSubscription = this.cityData.getStateList(countryId).subscribe((data: any) => {
        this.states = data;
      });

      this.subscriptions.push(this.getStatesSubscription);
    }

    getSelectedCountryId(event) {
      this.selectedCountryId = event.target.value;
      this.getStates(this.selectedCountryId);
    }

    getSelectedstateId(event) {
      this.selectedCountryId = event.target.value;
      this.state_id = this.selectedCountryId;
      this.getCities();
    }

    getCities() {
      this.isLoadingResults = true;
      this.searchFound = false;

      this.isLoadingResults = true;
      if(this.paginator.pageIndex) {
        this.setPageIndex = this.paginator.pageIndex + 1;
      } else {
        this.setPageIndex = 1;
      }
      this.getCitiesSubscription = this.cityData.getCities(this.setPageIndex, this.paginator.pageSize, this.state_id)
       .subscribe((data: any) => {
        this.resultsLength = data.paginationObject.totalRecord;
        this.pageSize = data.totalPage;
        this.dataSource = new MatTableDataSource<any>(data.result);
        this.dataSource.sort = this.sort;
        this.selection.clear();
        this.searchFound = true;
        this.isLoadingResults = false;
        this.noCityRecord = false;
      }, (error) => {
        this.noCityRecord = true;
        this.isLoadingResults = false;
        this.toastr.error('No record found!', 'No Record!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      });

      this.subscriptions.push(this.getCitiesSubscription);
    }

    createForm() {
      this.searchForm = this.formBuilder.group({
        country: [''],
        state: [''],
        city: ['']
      });
    }

    onConfirmDeleteCity() {
      let dialog = this.dialog.open(DialogBodyComponent, {
        data: {
          title: 'Confirm Remove City',
          message: 'Are you sure, you want to delete city '
        }
      });
      dialog.afterClosed()
        .subscribe(selection => {
          if (selection) {
            this.onDeleteChecked();
          }
        });
    }

    deleteSingleCity(data) {
      this.checkedId.push(data.cityId);
      this.onConfirmDeleteCity();
    }

    onUpdateCity() {
      this.submitted = true;
        this.updateLoading = true;
        this.updateSubscription = this.cityData.updateCity(this.cityModel.city, this.cityId).subscribe((data: any) => {
          this.toastr.success('Updated successful!', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });
          this.updateLoading = false;
          this.showEditCityPanel = false;
          this.getCities();
        });

        this.subscriptions.push(this.updateSubscription);

    }

    editCity(element) {
      console.log(element);
      this.showEditCityPanel = true;
      this.cityModel.city = element.cityName;
      this.cityId = element.cityId;
    }


    onDeleteChecked() {
      this.checkedDeleteLoader = true;
      this.isSearch = false;
      const getChecked = this.selection.selected;

      getChecked.map((data: City) => {
        this.checkedId.push(data.cityId);
      });

      this.deleteCheckedSubscription = this.cityData.deleteCheckedCity(this.checkedId).subscribe((data) => {
        this.getCitiesSubscription = this.cityData.getCities(this.setPageIndex, this.paginator.pageSize, this.state_id)
        .subscribe((data: any) => {
          this.resultsLength = data.paginationObject.totalRecord;
          this.pageSize = data.totalPage;
          this.dataSource = new MatTableDataSource<any>(data.result);
          this.dataSource.sort = this.sort;
          this.selection.clear();
          this.searchFound = true;
          this.isLoadingResults = false;
          this.noCityRecord = false;

          this.toastr.success('Deleted successful!', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });

          this.checkedDeleteLoader = false;
          this.checkedId.length = 0;
        });
      })

      this.subscriptions.push(this.getCountriesSubscription);
      this.subscriptions.push(this.deleteCheckedSubscription);
    }

    getNext(event: PageEvent) {
      this.getCities();
    }

    onSearch() {
      this.submitted = true;
      // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.searchForm.value, null, 4));
    }

    /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    if (numSelected > 0) {
      this.checkedBtn = true;
    } else {
      this.checkedBtn = false;
    }
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: City): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.cityId  + 1}`;
  }

  navigateTo(row: any) {
    this.router.navigate(['/admin/'+row.id]);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }


}
