import { TestBed } from '@angular/core/testing';

import { ExpiredJobService } from './expired-job.service';

describe('ExpiredJobService', () => {
  let service: ExpiredJobService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExpiredJobService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
