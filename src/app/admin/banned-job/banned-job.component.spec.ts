import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannedJobComponent } from './banned-job.component';

describe('BannedJobComponent', () => {
  let component: BannedJobComponent;
  let fixture: ComponentFixture<BannedJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannedJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannedJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
