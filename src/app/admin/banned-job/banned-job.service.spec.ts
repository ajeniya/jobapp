import { TestBed } from '@angular/core/testing';

import { BannedJobService } from './banned-job.service';

describe('BannedJobService', () => {
  let service: BannedJobService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BannedJobService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
