import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Country, ManageCountryService } from '../manage-country/manage-country.service';
import { AddStateService } from './add-state.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-states',
  templateUrl: './add-states.component.html',
  styleUrls: ['./add-states.component.scss']
})
export class AddStatesComponent implements OnInit, OnDestroy {
  navBar: void;
  stateModel: any = {};
  subscriptions: Subscription[] = [];
  countries: Country[];
  getCountriesSubscription: Subscription;
  addStateSubscription: Subscription;
  loading = false;
  submitted: boolean;

  constructor(private eventServices: EventServicesService,
    public data: AddStateService, private toastr: ToastrService,
    public country: ManageCountryService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getCountries();
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    this.addStateSubscription = this.data.addState(this.stateModel).subscribe((data: any) => {
      this.toastr.success('Added successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.addStateSubscription);
  }


  getCountries() {
    this.getCountriesSubscription = this.country.getCountriesList().subscribe((data: Country[]) => {
       this.countries = data;
    });
    this.subscriptions.push(this.getCountriesSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
