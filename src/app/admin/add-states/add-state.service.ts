import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AddStateService {

  constructor(private http: HttpClient) { }

  addState(formData){
    return this.http.post<any>(`${environment.baseUrl}/Location/addstate`,
    {
      'name': formData.state,
      'countryID': formData.country
    }).pipe(map(result => {
            return result;
      }));
  }

}

