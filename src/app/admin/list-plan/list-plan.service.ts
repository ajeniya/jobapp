import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class ListPlanService {

  constructor(private http: HttpClient) { }

  getPlans(pageNo?, pageSize?) {
    return this.http.get<any>(`${environment.baseUrl}/Plan/getplan?pageNo=${pageNo}&pageSize=${pageSize}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  deleteCheckedPlans(checked) {
    return this.http.post<any>(`${environment.baseUrl}/Plan/deleteplan`, {
      'checked': checked
    })
    .pipe(map(result => {
        return result;
    }));
  }

  getSearch(pageNo?, pageSize?, name?, activate?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Plan/searchplan?pageNo=${pageNo}&pageSize=${pageSize}&name=${name}&activate=${activate}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  activatePlan(planId) {
    return this.http.post<any>(`${environment.baseUrl}/Plan/activateplan`, {
      'plan_id': planId,
      'activate': 'true',
    })
    .pipe(map(result => {
        return result;
    }));
  }

  deactivePlan(planId) {
    return this.http.post<any>(`${environment.baseUrl}/Plan/activateplan`, {
      'plan_id': planId,
      'activate': 'false',
    })
    .pipe(map(result => {
        return result;
    }));
  }

  updatePlan(payload, plan_id) {
    return this.http.post<any>(`${environment.baseUrl}/Plan/updatePlan`, {
      'plan_id': plan_id,
      'plan_name': payload.plan_name,
      'activate': payload.activate,
      'amount': payload.amount,
      'amount_number': payload.amount_number,
      'featured': payload.featured,
      'highlight': payload.highlight,
      'job_expired': payload.job_expired,
      'on_home_page': payload.on_home_page,
      'on_top_of_search': payload.on_top_of_search,
      'plan_expired': payload.plan_expired,
      'plan_limit': payload.plan_limit,
      'recommended': payload.recommended,
      'isfree': payload.isfree,
      'urgent': payload.urgent,
    })
    .pipe(map(result => {
        return result;
    }));
  }
}

export class Plan {
  plan_id?: string;
  activate?: string;
  amount?: string;
  amount_number?: string;
  highlight?: boolean;
  job_expired?: string;
  on_home_page?: boolean;
  on_top_of_search?: string;
  plan_expired?: boolean;
  plan_limit?: string;
  plan_name?: string;
  recommended?: boolean;
  urgent?: string;
}

