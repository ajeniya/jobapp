import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent, MatDialog } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { Plan, ListPlanService } from './list-plan.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { trigger, transition, style, animate } from '@angular/animations';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-list-plan',
  templateUrl: './list-plan.component.html',
  styleUrls: ['./list-plan.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(90%)'}),
        animate('400ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ListPlanComponent implements OnInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  getPlanSubscription: Subscription;
  deletePlanSubscription: Subscription;
  getSearchSubscription: Subscription;
  activatePlanSubscription: Subscription;
  deactivatePlanSubscription: Subscription;
  plan: Plan[];
  searchForm: FormGroup;
  public dataSource = new MatTableDataSource<Plan>();

  displayedColumns: string[] = ['select','plan_name', 'plan_limit', 'amount', 'recommended','activated','action'];
  selection = new SelectionModel<Plan>(true, []);

  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]
  };

  currencySign: string;
  formattedAmount: any;
  addPlanForm: FormGroup;
  editPlanForm: FormGroup;

  resultsLength: number;
  paginationMeta: any;
  submitted: boolean;
  dateValidationError: string;
  checkedBtn: boolean;
  isLoadingResults: boolean;
  searchFound: boolean;
  setPageIndex: any;
  noPlanRecord: boolean;
  checkedDeleteLoader: boolean;
  isSearch: boolean;
  checkedId = [];
  noFaqRecord: boolean;
  searchLoading: boolean;
  showEditPlanPanel: boolean;
  plan_id: any;
  updateLoading: boolean;
  updateSubscription: any;

  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router, public dialog: MatDialog,
    private currencyPipe : CurrencyPipe,
    private cdref: ChangeDetectorRef, public data: ListPlanService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.createForm();
    this.loadEditForm();
  }

  ngAfterViewInit() {
    this.getPlans();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  // convenience getter for easy access to form fields
  get a() { return this.searchForm.controls; }

  getNext(event: PageEvent) {
    this.getPlans();
  }

  loadEditForm() {
    this.editPlanForm  =  this.formBuilder.group({
      plan_name: ['', Validators.required],
      plan_expired: ['', Validators.required],
      plan_limit: ['', Validators.required],
      job_expired: ['', Validators.required],
      featured: ['', Validators.required],
      urgent: ['', Validators.required],
      highlight: ['', Validators.required],
      on_top_of_search: ['', Validators.required],
      on_home_page: ['', Validators.required],
      amount: ['', Validators.required],
      amount_number: ['', Validators.required],
      activate: ['', Validators.required],
      isfree: ['', Validators.required],
      recommended: [''],
    });
  }

  get f() { return this.editPlanForm.controls; }


  transformAmountMin(){
    const inputedValue = this.f.amount.value;
    const removeText = inputedValue.replace(/[^0-9\.]+/g, "");
    if( removeText <= 0 ) {
      this.toastr.error('Value cannot be less than 1', 'Error!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });

      this.editPlanForm.controls['amount'].setErrors(null);
      this.editPlanForm.controls['amount_number'].setErrors(null);
      return;
    }
    this.formattedAmount = this.f.amount.value;
    this.formattedAmount = this.currencyPipe.transform(removeText, this.currencySign);
    this.editPlanForm.controls['amount'].setValue(this.formattedAmount);
    this.editPlanForm.controls['amount_number'].setValue(removeText);
  }


  editPlan(element) {
    console.log(element);
    this.showEditPlanPanel = true;
    this.plan_id = element.plan_id;
    this.editPlanForm.patchValue({
      plan_name: element.plan_name,
      plan_expired: element.plan_expired,
      plan_limit: element.plan_limit,
      job_expired: element.job_expired,
      featured: element.featured,
      urgent: element.urgent,
      highlight: element.highlight,
      on_top_of_search: element.on_top_of_search,
      on_home_page: element.on_home_page,
      amount: element.amount,
      amount_number: element.amount_number,
      activate: element.activate,
      isfree: element.isfree,
      recommended: element.recommended
    })

  }

  updatePlan() {
    this.submitted = true;
      this.updateLoading = true;
      this.updateSubscription = this.data.updatePlan(this.editPlanForm.value, this.plan_id).subscribe((data: any) => {
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
        this.updateLoading = false;
        this.showEditPlanPanel = false;
        this.getPlans();
      });

      this.subscriptions.push(this.updateSubscription);

  }

  getPlans() {
    this.isLoadingResults = true;
    this.searchFound = false;
    if(this.paginator.pageIndex) {
      this.setPageIndex = this.paginator.pageIndex + 1;
    } else {
      this.setPageIndex = 1;
    }
    this.getPlanSubscription = this.data.getPlans(this.setPageIndex, this.paginator.pageSize)
     .subscribe((data: any) => {
      this.resultsLength = data.paginationObject.totalRecord;
      this.dataSource = new MatTableDataSource<Plan>(data.result);
      this.dataSource.sort = this.sort;
      this.selection.clear();
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noPlanRecord = false;
      this.selection.selected.length = 0;
    }, (error) => {
      this.noPlanRecord = true;
      this.isLoadingResults = false;
      this.searchLoading = false;
      this.resultsLength = 0;
      this.checkedDeleteLoader = false;

      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getPlanSubscription);
  }

  createForm() {
    this.searchForm = this.formBuilder.group({
      name: [''],
      activate: ['']
    });
  }

  clearSearch() {
    this.isSearch = false;
    this.setPageIndex = 1;
    this.getPlans();
    this.searchForm.reset();
  }

  onSearch() {
    this.submitted = true;
    this.searchLoading = true;
    this.setPageIndex = 1;
    this.selection.selected.length = 0;
    this.getSearchSubscription =
    this.data.getSearch(this.setPageIndex, this.paginator.pageSize,
      this.searchForm.value.name, this.searchForm.value.activate ).subscribe((data: any) => {
      this.resultsLength = data.paginationObject.totalRecord;
      this.dataSource = new MatTableDataSource<Plan>(data.result);
      this.dataSource.sort = this.sort;
      this.selection.clear();
      this.searchLoading = false;
      this.isSearch = true;
      this.searchFound = true;
    }, (error) => {
      this.searchFound = false;
      this.searchLoading = false;
      this.isSearch = true;
      this.resultsLength = 0;
    });

    this.subscriptions.push(this.getSearchSubscription);

  }


  deleteSinglePlan(element) {
    this.checkedId.push(element.plan_id);
    this.onConfirmDelete();
  }

  onConfirmDelete() {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Remove Plan',
        message: 'Are you sure, you want to delete plan '
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.onDeleteChecked();
        }
      });
  }

  activatePlan(planId) {
    this.activatePlanSubscription = this.data.activatePlan(planId.plan_id).subscribe((data) => {
      this.getPlanSubscription = this.data.getPlans(
        this.setPageIndex, this.paginator.pageSize)
        .subscribe((data: any) => {
          this.resultsLength = data.paginationObject.totalRecord;
          this.dataSource = new MatTableDataSource<Plan>(data.result);
          this.dataSource.sort = this.sort;
          this.noFaqRecord = false;

          this.toastr.success('Plan activated', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });

          this.checkedDeleteLoader = false;
          this.checkedId.length = 0;
      });
    })

    this.subscriptions.push(this.activatePlanSubscription);
  }

  deactivatePlan(planId) {
    this.deactivatePlanSubscription = this.data.deactivePlan(planId.plan_id).subscribe((data) => {
      this.getPlanSubscription = this.data.getPlans(
        this.setPageIndex, this.paginator.pageSize)
        .subscribe((data: any) => {
          this.resultsLength = data.paginationObject.totalRecord;
          this.dataSource = new MatTableDataSource<Plan>(data.result);
          this.dataSource.sort = this.sort;
          this.noFaqRecord = false;

          this.toastr.success('Plan deactivated', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });

          this.checkedDeleteLoader = false;
          this.checkedId.length = 0;
      });
    })

    this.subscriptions.push(this.deactivatePlanSubscription);
  }




  onDeleteChecked() {
    this.checkedDeleteLoader = true;
    this.isSearch = false;
    const getChecked = this.selection.selected;

    getChecked.map((data) => {
      this.checkedId.push(data.plan_id);
    });

    this.deletePlanSubscription = this.data.deleteCheckedPlans(this.checkedId).subscribe((data) => {
      this.selection.clear();
      this.searchForm.reset();
      this.checkedDeleteLoader = false;
      this.checkedId.length = 0;
      this.getPlans();
    })

    this.subscriptions.push(this.getPlanSubscription);
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    if (numSelected > 0) {
      this.checkedBtn = true;
    } else {
      this.checkedBtn = false;
    }
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Plan): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.plan_name + 1}`;
  }

  navigateTo(row: any) {
    this.router.navigate(['/admin/'+row.id]);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }


}
