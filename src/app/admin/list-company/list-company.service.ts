import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { CompanyLists } from './company-list.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class ListCompanyService {

  companyLists: CompanyLists[] = [
   {
    'company_id': '1',
    'company_name': 'Invitea',
    'company_location': 'Abuja',
    'company_status': 'approved',
    'company_category': 'IT'
   },
   {
    'company_id': '1',
    'company_name': 'Testfit',
    'company_location': 'Abuja',
    'company_status': 'approved',
    'company_category': 'Engineering'
   },
  ];

  constructor(private http: HttpClient) { }

  getCompanies(pageNo?, pageSize?) {
    return this.http.get<any>(`${environment.baseUrl}/Company/searchcompaniesadmin?pageNo=${pageNo}&pageSize=${pageSize}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getSearch(pageNo?, pageSize?, name?, location?, category?, status?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Company/searchcompaniesadmin?pageNo=${pageNo}&pageSize=${pageSize}&companyName=${name}&companyLocation=${location}&category=${category}&status=${status}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  deleteCheckedCompany(checked) {
    return this.http.post<any>(`${environment.baseUrl}/Company/deletecompany`, {
      'checked': checked
    })
    .pipe(map(result => {
        return result;
    }));
  }
}
