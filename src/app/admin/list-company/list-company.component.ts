import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent, MatDialog } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { CompanyLists } from './company-list.model';
import { ListCompanyService } from './list-company.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';

@Component({
  selector: 'app-list-company',
  templateUrl: './list-company.component.html',
  styleUrls: ['./list-company.component.scss']
})
export class ListCompanyComponent implements OnInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  getCompaniesSubscription: Subscription;
  deleteCompanySubscription: Subscription;
  getSearchSubscription: Subscription;
  companyList: CompanyLists[];
  searchForm: FormGroup;

  displayedColumns: string[] = ['company_name','location', 'category', 'status','action'];
  selection = new SelectionModel<CompanyLists>(true, []);

  public dataSource = new MatTableDataSource<CompanyLists>();
  resultsLength: number;
  paginationMeta: any;
  submitted: boolean;
  checkedBtn: boolean;
  setPageIndex: any;
  isLoadingResults: boolean;
  searchFound: boolean;
  noCompanyRecord: any;
  checkedId = [];
  checkedDeleteLoader: boolean;
  isSearch: boolean;
  searchLoading: boolean;

  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router, public dialog: MatDialog,
    private cdref: ChangeDetectorRef, private data: ListCompanyService) { }

    ngOnInit() {
      this.navBar = this.eventServices.navchange.emit(true);
      this.createForm();
    }

    ngAfterViewInit() {
      this.getCompanies();
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    }

    // convenience getter for easy access to form fields
    get f() { return this.searchForm.controls; }

    deleteSingleCompany(element) {
      this.checkedId.push(element.company_id);
      this.onConfirmDelete(element);
    }

    onConfirmDelete(element) {
      let dialog = this.dialog.open(DialogBodyComponent, {
        data: {
          title: `Confirm remove ${element.company_name}`,
          message: `Are you sure, you want to delete ${element.company_name}`
        }
      });
      dialog.afterClosed()
        .subscribe(selection => {
          if (selection) {
            this.onDeleteChecked();
          }
        });
    }

    onDeleteChecked() {
      this.checkedDeleteLoader = true;
      this.isSearch = false;
      const getChecked = this.selection.selected;

      getChecked.map((data) => {
        this.checkedId.push(data.company_id);
      });

      this.deleteCompanySubscription = this.data.deleteCheckedCompany(this.checkedId).subscribe((data) => {
        this.getCompaniesSubscription = this.data.getCompanies(this.setPageIndex, this.paginator.pageSize)
          .subscribe((data: any) => {
            this.resultsLength = data.paginationObject.totalRecord;
            this.dataSource = new MatTableDataSource<CompanyLists>(data.result);
            this.dataSource.sort = this.sort;
            this.selection.clear();
            this.searchFound = true;
            this.isLoadingResults = false;
            this.noCompanyRecord = false;
            this.selection.selected.length = 0;
          }, (error) => {
            this.noCompanyRecord = true;
            this.isLoadingResults = false;
            this.toastr.error('No record found!', 'No Record!', {
              enableHtml: true,
              closeButton: true,
              timeOut: 3000
            });
          });
      })

      this.subscriptions.push(this.getCompaniesSubscription);
      this.subscriptions.push(this.deleteCompanySubscription);
    }

    onSearch() {
      this.submitted = true;
      this.searchLoading = true;
      this.setPageIndex = 1;
      this.selection.selected.length = 0;
      this.getSearchSubscription =
      this.data.getSearch(this.setPageIndex, this.paginator.pageSize,
        this.searchForm.value.name, this.searchForm.value.location, this.searchForm.value.category, this.searchForm.value.status ).subscribe((data: any) => {
        this.resultsLength = data.paginationObject.totalRecord;
        this.dataSource = new MatTableDataSource<CompanyLists>(data.result);
        this.dataSource.sort = this.sort;
        this.selection.clear();
        this.searchLoading = false;
        this.isSearch = true;
        this.searchFound = true;
      }, (error) => {
        this.searchFound = false;
        this.searchLoading = false;
        this.isSearch = true;
        this.resultsLength = 0;
      });

      this.subscriptions.push(this.getSearchSubscription);

    }

    getCompanies() {
      this.isLoadingResults = true;
      this.searchFound = false;
      if(this.paginator.pageIndex) {
        this.setPageIndex = this.paginator.pageIndex + 1;
      } else {
        this.setPageIndex = 1;
      }
      this.getCompaniesSubscription = this.data.getCompanies(this.setPageIndex, this.paginator.pageSize)
       .subscribe((data: any) => {
        this.resultsLength = data.paginationObject.totalRecord;
        this.dataSource = new MatTableDataSource<CompanyLists>(data.result);
        this.dataSource.sort = this.sort;
        this.selection.clear();
        this.searchFound = true;
        this.isLoadingResults = false;
        this.noCompanyRecord = false;
        this.selection.selected.length = 0;
      }, (error) => {
        this.noCompanyRecord = true;
        this.isLoadingResults = false;
        this.toastr.error('No record found!', 'No Record!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
      });

      this.subscriptions.push(this.getCompaniesSubscription);
    }

    getNext(event: PageEvent) {
      this.getCompanies();
    }

    clearSearch() {
      this.isSearch = false;
      this.setPageIndex = 1;
      this.getCompanies();
      this.searchForm.reset();
    }

    createForm() {
      this.searchForm = this.formBuilder.group({
        name: [''],
        location: [''],
        category: [''],
        status: ['']
      });
    }


    navigateToCompany(data) {
      this.router.navigate(['/company/detail'], { queryParams: { link: data.company_id } });
    }

    isAllSelected() {
      const numSelected = this.selection.selected.length;
      if (numSelected > 0) {
        this.checkedBtn = true;
      } else {
        this.checkedBtn = false;
      }
      const numRows = this.dataSource.data.length;
      return numSelected === numRows;
    }

    /** Selects all rows if they are not all selected; otherwise clear selection. */
    masterToggle() {
      this.isAllSelected() ?
          this.selection.clear() :
          this.dataSource.data.forEach(row => this.selection.select(row));
    }

    /** The label for the checkbox on the passed row */
    checkboxLabel(row?: CompanyLists): string {
      if (!row) {
        return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
      }
      return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.company_name + 1}`;
    }

    navigateTo(row: any) {
      this.router.navigate(['/admin/'+row.id]);
    }


    ngOnDestroy() {
      this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    }


}
