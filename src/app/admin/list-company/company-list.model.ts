export class CompanyLists {
  company_id: string;
  company_name: string;
  company_location: string;
  company_keyword?: string;
  company_status: string;
  company_category?: string;
}
