import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy, HostListener, ElementRef } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent, MatDialog } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { ListStateService, State } from './list-state.service';
import { ManageCountryService } from '../manage-country/manage-country.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { trigger, transition, animate, style } from '@angular/animations'
import { MainSettingService } from '../setting/main-setting/main-setting.service';

@Component({
  selector: 'app-list-states',
  templateUrl: './list-states.component.html',
  styleUrls: ['./list-states.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(90%)'}),
        animate('400ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class ListStatesComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  getStatesSubscription: Subscription;
  getCountriesSubscription: Subscription;
  deleteCheckedSubscription: Subscription;
  updateSubscription: Subscription;
  SettingSubscription: Subscription;
  states: any;
  countries: any;
  searchForm: FormGroup;
  countryList = [];
  stateModel: any = {};
  searchLoading = false;
  showEditCountryPanel = false;
  updateLoading = false;

  displayedColumns: string[] = ['select','counterCodeName', 'country', 'state','action'];
  selection = new SelectionModel<State>(true, []);

  public dataSource = new MatTableDataSource<State>();
  resultsLength: number;
  paginationMeta: any;
  submitted: boolean;
  dateValidationError: string;
  checkedBtn: boolean;
  setPageIndex: any;
  searchFound: boolean;
  isLoadingResults = true;
  noStateRecord = false;
  countryId: number;

  checkedDeleteLoader = false;
  checkedId = [];
  isSearch = false;
  stateId: any;

  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router, public country: ManageCountryService,
    public dialog: MatDialog, public stateData: ListStateService, public siteSetting: MainSettingService) {

    }

    ngOnInit(): void {
      this.navBar = this.eventServices.navchange.emit(true);
      this.createForm();
      this.getCountries();
    }


    ngAfterViewInit() {
      this.SettingSubscription = this.siteSetting.getSetting().subscribe((data) => {
        this.countryId = data.countryId;
        this.getStates();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      });

      this.subscriptions.push(this.getCountriesSubscription);
    }

    // convenience getter for easy access to form fields
    get f() { return this.searchForm.controls; }

    getCountries() {
      this.getCountriesSubscription = this.country.getCountriesList().subscribe((data) => {
         this.countries = data;
      });

      this.subscriptions.push(this.getCountriesSubscription);
    }

    getStates() {
      this.isLoadingResults = true;
      if(this.paginator.pageIndex) {
        this.setPageIndex = this.paginator.pageIndex + 1;
      } else {
        this.setPageIndex = 1;
      }

      this.getStatesSubscription = this.stateData.getStates(this.setPageIndex, this.paginator.pageSize, this.countryId).subscribe((data) => {
        this.states = data;
        this.resultsLength = data.paginationObject.totalRecord;
        this.dataSource = new MatTableDataSource<State>(data.result);
        this.dataSource.sort = this.sort;
        this.isLoadingResults = false;
        this.noStateRecord = false;
        this.selection.selected.length = 0;
      }, (error) => {
        this.noStateRecord = true;
        this.isLoadingResults = false;
        this.toastr.error('No record found!', 'No Record!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        })

      this.subscriptions.push(this.getStatesSubscription);
    });
  }

    createForm() {
      this.searchForm = this.formBuilder.group({
        countryName: [''],
        state: ['']
      });
    }

    getNext(event: PageEvent) {
      this.getStates();
    }


    onConfirmDeleteState() {
      let dialog = this.dialog.open(DialogBodyComponent, {
        data: {
          title: 'Confirm Remove State',
          message: 'Are you sure, you want to delete state '
        }
      });
      dialog.afterClosed()
        .subscribe(selection => {
          if (selection) {
            this.onDeleteChecked();
          }
        });
    }

    deleteSingleState(data) {
      this.checkedId.push(data.stateId);
      this.onConfirmDeleteState();
    }


    onDeleteChecked() {
      this.checkedDeleteLoader = true;
      this.isSearch = false;
      const getChecked = this.selection.selected;

      getChecked.map((data) => {
        console.log(data);
        this.checkedId.push(data.stateId);
      });

      this.deleteCheckedSubscription = this.stateData.deleteCheckedStates(this.checkedId).subscribe((data) => {
        this.getStatesSubscription = this.stateData.getStates(
          this.setPageIndex, this.paginator.pageSize, this.countryId)
          .subscribe((data: any) => {
            this.resultsLength = data.paginationObject.totalRecord;
            this.dataSource = new MatTableDataSource<State>(data.result);
            this.dataSource.sort = this.sort;
            this.selection.clear();

            this.toastr.success('Deleted successful!', 'Success!', {
              enableHtml: true,
              closeButton: true,
              timeOut: 3000
            });

            this.checkedDeleteLoader = false;
            this.checkedId.length = 0;
          });
      })

      this.subscriptions.push(this.getCountriesSubscription);
      this.subscriptions.push(this.deleteCheckedSubscription);
    }

    editState(element) {
      this.showEditCountryPanel = true;
      this.stateModel.state = element.stateName;
      this.stateId = element.stateId;
    }


    onUpdateCountry() {
      this.submitted = true;
        this.updateLoading = true;
        this.updateSubscription = this.stateData.updateState(this.stateModel, this.stateId).subscribe((data: any) => {
          this.toastr.success('Updated successful!', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });
          this.updateLoading = false;
          this.showEditCountryPanel = false;
          this.getStates();
        });

        this.subscriptions.push(this.updateSubscription);

    }

    getCountry(countryId) {
      this.selection.clear()
      this.countryId = countryId;
      if(this.paginator.pageIndex) {
        this.setPageIndex = this.paginator.pageIndex + 1;
      } else {
        this.setPageIndex = 1;
      }

      this.getStatesSubscription = this.stateData.getStates(1, this.paginator.pageSize, this.countryId).subscribe((data) => {
        this.states = data;
        this.resultsLength = data.paginationObject.totalRecord;
        this.dataSource = new MatTableDataSource<State>(data.result);
        this.dataSource.sort = this.sort;
      });

      this.subscriptions.push(this.getStatesSubscription);
    }


    /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    if (numSelected > 0) {
       this.checkedBtn = true;
    } else {
       this.checkedBtn = false;
    }
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  // /** The label for the checkbox on the passed row */
  checkboxLabel(row?: any): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  navigateTo(row: any) {
    this.router.navigate(['/admin/'+row.id]);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }


}
