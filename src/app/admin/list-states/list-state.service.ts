import { delay } from 'rxjs/internal/operators/delay';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
@Injectable({
  providedIn: 'root'
})
export class ListStateService {
  constructor(private http: HttpClient) { }

  getStates(pageNo?, pageSize?, countryId?) {
    return this.http.get<any>(`${environment.baseUrl}/Location/states?pageNo=${pageNo}&pageSize=${pageSize}&countryId=${countryId}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getCountriesList() {
    return this.http.get<any>(`${environment.baseUrl}/Location/getcountrylist`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  getStateSearch(pageNo?, pageSize?, name?, countryCode?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Location/searchstates?pageNo=${pageNo}&pageSize=${pageSize}&name=${name}&countryCode=${countryCode}`, {})
    .pipe( map(result => {
        return result;
    }));
  }

  deleteCheckedStates(checked) {
    return this.http.post<any>(`${environment.baseUrl}/Location/deleteCheckedState`, {
      'checked': checked
    })
    .pipe(map(result => {
        return result;
    }));
  }

  getSearch(pageNo?, pageSize?, name?, countryId?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Location/searchstate?pageNo=${pageNo}&pageSize=${pageSize}&name=${name}&countryId=${countryId}`, {})
    .pipe(map(result => {
        return result;
    }));
  }


  updateState(payload, stateId) {
    return this.http.post<any>(`${environment.baseUrl}/Location/updatestate`, {
      'name': payload.state,
      'state_id': stateId
    })
    .pipe(map(result => {
        return result;
    }));
  }

}

export class State {
  countryCode: string;
  countryId: number;
  countryName: string;
  id: number;
  stateId: number;
  stateName: string;
}
