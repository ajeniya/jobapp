import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnapprovedJobsComponent } from './unapproved-jobs.component';

describe('UnapprovedJobsComponent', () => {
  let component: UnapprovedJobsComponent;
  let fixture: ComponentFixture<UnapprovedJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnapprovedJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnapprovedJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
