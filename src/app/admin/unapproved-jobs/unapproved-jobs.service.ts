import { Injectable } from '@angular/core';
import { Job, CompanyLists } from './unapproved.model';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})
export class UnapprovedJobsService {
  jobs: Job[] = [
    {
     'job_id': '1',
     'company_name': 'Prindex',
     'title': 'Business Manager',
     'location': ['Lagos', 'Oyo'],
     'date_created': '1 Week Ago'
    },
    {
     'job_id': '2',
     'company_name': 'Akete Studio',
     'title': 'Devops',
     'location': ['Lagos', 'Kogi', 'Abuja'],
     'date_created': '1 Week Ago'
    },
    {
     'job_id': '3',
     'company_name': 'IBM',
     'title': 'Developer Backend',
     'location': ['Lagos'],
     'date_created': '1 Week Ago'
    },
    {
      'job_id': '2',
      'company_name': 'Akete Studio',
      'title': 'Devops',
      'location': ['Lagos', 'Kogi', 'Abuja','Lagos', 'Kogi', 'Abuja'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '3',
      'company_name': 'IBM',
      'title': 'Developer Backend',
      'location': ['Lagos'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '1',
      'company_name': 'Prindex',
      'title': 'Business Manager',
      'location': ['Lagos', 'Oyo'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '2',
      'company_name': 'Akete Studio',
      'title': 'Devops',
      'location': ['Lagos', 'Kogi', 'Abuja'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '3',
      'company_name': 'IBM',
      'title': 'Developer Backend',
      'location': ['Lagos'],
      'date_created': '1 Week Ago'
     },
     {
       'job_id': '2',
       'company_name': 'Akete Studio',
       'title': 'Devops',
       'location': ['Lagos', 'Kogi', 'Abuja'],
       'date_created': '1 Week Ago'
      },
      {
       'job_id': '3',
       'company_name': 'IBM',
       'title': 'Developer Backend',
       'location': ['Lagos'],
       'date_created': '1 Week Ago'
      }, {
        'job_id': '1',
        'company_name': 'Prindex',
        'title': 'Business Manager',
        'location': ['Lagos', 'Oyo'],
        'date_created': '1 Week Ago'
       },
       {
        'job_id': '2',
        'company_name': 'Akete Studio',
        'title': 'Devops',
        'location': ['Lagos', 'Kogi', 'Abuja'],
        'date_created': '1 Week Ago'
       },
       {
        'job_id': '3',
        'company_name': 'IBM',
        'title': 'Developer Backend',
        'location': ['Lagos'],
        'date_created': '1 Week Ago'
       },
       {
         'job_id': '2',
         'company_name': 'Akete Studio',
         'title': 'Devops',
         'location': ['Lagos', 'Kogi', 'Abuja'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '3',
         'company_name': 'IBM',
         'title': 'Developer Backend',
         'location': ['Lagos'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '1',
         'company_name': 'Prindex',
         'title': 'Business Manager',
         'location': ['Lagos', 'Oyo'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '2',
         'company_name': 'Akete Studio',
         'title': 'Devops',
         'location': ['Lagos', 'Kogi', 'Abuja'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '3',
         'company_name': 'IBM',
         'title': 'Developer Backend',
         'location': ['Lagos'],
         'date_created': '1 Week Ago'
        },
        {
          'job_id': '2',
          'company_name': 'Akete Studio',
          'title': 'Devops',
          'location': ['Lagos', 'Kogi', 'Abuja'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '3',
          'company_name': 'IBM',
          'title': 'Developer Backend',
          'location': ['Lagos'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '1',
          'company_name': 'Prindex',
          'title': 'Business Manager',
          'location': ['Lagos', 'Oyo'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '2',
          'company_name': 'Akete Studio',
          'title': 'Devops',
          'location': ['Lagos', 'Kogi', 'Abuja'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '3',
          'company_name': 'IBM',
          'title': 'Developer Backend',
          'location': ['Lagos'],
          'date_created': '1 Week Ago'
         },
         {
           'job_id': '2',
           'company_name': 'Akete Studio',
           'title': 'Devops',
           'location': ['Lagos', 'Kogi', 'Abuja'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '3',
           'company_name': 'IBM',
           'title': 'Developer Backend',
           'location': ['Lagos'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '1',
           'company_name': 'Prindex',
           'title': 'Business Manager',
           'location': ['Lagos', 'Oyo'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '2',
           'company_name': 'Akete Studio',
           'title': 'Devops',
           'location': ['Lagos', 'Kogi', 'Abuja'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '3',
           'company_name': 'IBM',
           'title': 'Developer Backend',
           'location': ['Lagos'],
           'date_created': '1 Week Ago'
          },
          {
            'job_id': '2',
            'company_name': 'Akete Studio',
            'title': 'Devops',
            'location': ['Lagos', 'Kogi', 'Abuja'],
            'date_created': '1 Week Ago'
           },
           {
            'job_id': '3',
            'company_name': 'IBM',
            'title': 'Developer Backend',
            'location': ['Lagos'],
            'date_created': '1 Week Ago'
           }, {
             'job_id': '1',
             'company_name': 'Prindex',
             'title': 'Business Manager',
             'location': ['Lagos', 'Oyo'],
             'date_created': '1 Week Ago'
            },
            {
             'job_id': '2',
             'company_name': 'Akete Studio',
             'title': 'Devops',
             'location': ['Lagos', 'Kogi', 'Abuja'],
             'date_created': '1 Week Ago'
            },
            {
             'job_id': '3',
             'company_name': 'IBM',
             'title': 'Developer Backend',
             'location': ['Lagos'],
             'date_created': '1 Week Ago'
            },
            {
              'job_id': '2',
              'company_name': 'Akete Studio',
              'title': 'Devops',
              'location': ['Lagos', 'Kogi', 'Abuja'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '3',
              'company_name': 'IBM',
              'title': 'Developer Backend',
              'location': ['Lagos'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '1',
              'company_name': 'Prindex',
              'title': 'Business Manager',
              'location': ['Lagos', 'Oyo'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '2',
              'company_name': 'Akete Studio',
              'title': 'Devops',
              'location': ['Lagos', 'Kogi', 'Abuja'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '3',
              'company_name': 'IBM',
              'title': 'Developer Backend',
              'location': ['Lagos'],
              'date_created': '1 Week Ago'
             },
             {
               'job_id': '2',
               'company_name': 'Akete Studio',
               'title': 'Devops',
               'location': ['Lagos', 'Kogi', 'Abuja'],
               'date_created': '1 Week Ago'
              },
              {
               'job_id': '3',
               'company_name': 'IBM',
               'title': 'Developer Backend',
               'location': ['Lagos'],
               'date_created': '1 Week Ago'
              }
  ];

  companyLists: CompanyLists[] = [
    {
    'company_id': '1',
    'company_name': 'Invitea'
   },
   {
    'company_id': '1',
    'company_name': 'Testfit'
   },
   {
    'company_id': '1',
    'company_name': 'Itemize'
   },
   {
    'company_id': '1',
    'company_name': 'Teampay'
   },
   {
    'company_id': '1',
    'company_name': 'PepsiCo'
   },
   {
    'company_id': '1',
    'company_name': 'Speedway'
   },
   {
    'company_id': '1',
    'company_name': 'Lyft'
   },
   {
    'company_id': '1',
    'company_name': 'Coney Island prep',
   },
   {
    'company_id': '1',
    'company_name': 'Aramark'
   }
  ];

   constructor() { }

   getJob() {
     return of(this.jobs);
   }

   getCompanies() {
    return of(this.companyLists);
  }

}
