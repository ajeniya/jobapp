import { TestBed } from '@angular/core/testing';

import { UnapprovedJobsService } from './unapproved-jobs.service';

describe('UnapprovedJobsService', () => {
  let service: UnapprovedJobsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UnapprovedJobsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
