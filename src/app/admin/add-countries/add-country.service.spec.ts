import { TestBed } from '@angular/core/testing';

import { AddCountryService } from './add-country.service';

describe('AddCountryService', () => {
  let service: AddCountryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddCountryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
