import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AddCountryService {

  constructor(private http: HttpClient) { }


  addCountry(formData) {
    return this.http.post<any>(`${environment.baseUrl}/Location/addCountry`,
    {
      'sortnamecode': formData.sortnamecode,
      'country': formData.country,
      'phoneCode': formData.phoneCode
    }).pipe(map(result => {
            return result;
      }));
  }
}
