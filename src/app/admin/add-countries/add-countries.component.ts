import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { ToastrService } from 'ngx-toastr';
import { AddCountryService } from './add-country.service';

@Component({
  selector: 'app-add-countries',
  templateUrl: './add-countries.component.html',
  styleUrls: ['./add-countries.component.scss']
})
export class AddCountriesComponent implements OnInit, OnDestroy {
  addCountrySubscription: Subscription;
  navBar: void;
  countryModel: any = {};
  submitted = false;
  subscriptions: Subscription[] = [];
  loading = false;

  constructor(private eventServices: EventServicesService,
    public data: AddCountryService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    this.addCountrySubscription = this.data.addCountry(this.countryModel).subscribe((data: any) => {
      this.toastr.success('Updated successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.addCountrySubscription);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
