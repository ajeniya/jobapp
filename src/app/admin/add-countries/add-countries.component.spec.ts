import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCountriesComponent } from './add-countries.component';

describe('AddCountriesComponent', () => {
  let component: AddCountriesComponent;
  let fixture: ComponentFixture<AddCountriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCountriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCountriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
