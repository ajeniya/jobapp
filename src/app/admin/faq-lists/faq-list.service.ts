import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class FaqListService {

  constructor(private http: HttpClient) { }

  getFaqs(pageNo?, pageSize?) {
    return this.http.get<any>(`${environment.baseUrl}/Faq/getfaq?pageNo=${pageNo}&pageSize=${pageSize}`, {})
    .pipe(map(result => {
        return result;
    }));
  }

  deleteCheckedFaqs(checked) {
    return this.http.post<any>(`${environment.baseUrl}/Faq/deletefaq`, {
      'checked': checked
    })
    .pipe(map(result => {
        return result;
    }));
  }

  updateFaq(payload, faq_id) {
    return this.http.post<any>(`${environment.baseUrl}/Faq/updatetfaq`, {
      'faq_id': faq_id,
      'question': payload.question,
      'answer': payload.answer,
      'status': payload.status
    })
    .pipe(map(result => {
        return result;
    }));
  }

  getSearch(pageNo?, pageSize?, question?, status?) {
    return this.http
    .get<any>(`${environment.baseUrl}/Faq/searchfaqs?pageNo=${pageNo}&pageSize=${pageSize}&question=${question}&status=${status}`, {})
    .pipe(map(result => {
        return result;
    }));
  }
}

export class FAQs {
  faq_id: string;
  question: string;
  answer: string;
  status?: string;
  created_date?: string;
  updated_date?: string;
}
