export class FAQs {
  faq_id: string;
  question: string;
  answer: string;
  status?: string;
  created_date?: string;
  updated_date?: string;
}
