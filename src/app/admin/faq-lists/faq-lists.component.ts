import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable, PageEvent, MatDialog } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { FAQs, FaqListService } from './faq-list.service';
import { DialogBodyComponent } from 'src/app/_components/search/dialog-body/dialog-body.component';
import { trigger, transition, style, animate } from '@angular/animations';

@Component({
  selector: 'app-faq-lists',
  templateUrl: './faq-lists.component.html',
  styleUrls: ['./faq-lists.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(90%)'}),
        animate('400ms ease-in', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('400ms ease-in', style({transform: 'translateX(100%)'}))
      ])
    ])
  ]
})
export class FaqListsComponent implements OnInit, OnDestroy {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  getFaqSubscription: Subscription;
  deleteFaqSubscription: Subscription;
  updateSubscription: Subscription;
  getSearchSubscription: Subscription;
  jobFaqs: FAQs[];
  searchForm: FormGroup;
  editFaqsForm: FormGroup;

  displayedColumns: string[] = ['select','question', 'answer', 'status', 'created_date','updated_date','action'];
  selection = new SelectionModel<FAQs>(true, []);

  public dataSource = new MatTableDataSource<FAQs>();

  resultsLength: number;
  paginationMeta: any;
  submitted: boolean;
  dateValidationError: string;
  checkedBtn: boolean;

  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]
  };

  checkedId = [];
  setPageIndex: any;
  isLoadingResults: boolean;
  searchFound: boolean;
  noFaqRecord: boolean;
  checkedDeleteLoader: boolean;
  isSearch: boolean;
  showEditFaqPanel: boolean;
  faq_id: any;
  editFaqForm: any;
  loading: boolean;
  updateLoading: boolean;
  searchLoading: boolean;


  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router, public dialog: MatDialog,
    public data: FaqListService) {

    }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.createForm();
    this.loadFaqForm();
  }


  ngAfterViewInit() {
    this.getFaqs();
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  // convenience getter for easy access to form fields
  get f() { return this.searchForm.controls; }
  get edit() { return this.editFaqForm.controls; }

  getNext(event: PageEvent) {
    this.getFaqs();
  }

  getFaqs() {
    this.isLoadingResults = true;
    this.searchFound = false;
    if(this.paginator.pageIndex) {
      this.setPageIndex = this.paginator.pageIndex + 1;
    } else {
      this.setPageIndex = 1;
    }
    this.getFaqSubscription = this.data.getFaqs(this.setPageIndex, this.paginator.pageSize)
     .subscribe((data: any) => {
      this.resultsLength = data.paginationObject.totalRecord;
      this.dataSource = new MatTableDataSource<FAQs>(data.result);
      this.dataSource.sort = this.sort;
      this.selection.clear();
      this.searchFound = true;
      this.isLoadingResults = false;
      this.noFaqRecord = false;
      this.selection.selected.length = 0;
    }, (error) => {
      this.noFaqRecord = true;
      this.isLoadingResults = false;
      this.toastr.error('No record found!', 'No Record!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
    });

    this.subscriptions.push(this.getFaqSubscription);
  }

  createForm() {
    this.searchForm = this.formBuilder.group({
      question: [''],
      status: ['']
    });
  }

  clearSearch() {
    this.isSearch = false;
    this.setPageIndex = 1;
    this.getFaqs();
    this.searchForm.reset();
  }

  onSearch() {
    this.submitted = true;
    this.searchLoading = true;
    this.setPageIndex = 1;
    this.selection.selected.length = 0;
    this.getSearchSubscription =
    this.data.getSearch(this.setPageIndex, this.paginator.pageSize,
      this.searchForm.value.question, this.searchForm.value.status ).subscribe((data: any) => {
      this.resultsLength = data.paginationObject.totalRecord;
      this.dataSource = new MatTableDataSource<FAQs>(data.result);
      this.dataSource.sort = this.sort;
      this.selection.clear();
      this.searchLoading = false;
      this.isSearch = true;
      this.searchFound = true;
    }, (error) => {
      this.searchFound = false;
      this.searchLoading = false;
      this.isSearch = true;
      this.resultsLength = 0;
    });

    this.subscriptions.push(this.getSearchSubscription);

  }

  deleteSingleFaq(element) {
    this.checkedId.push(element.testimonial_id);
    this.onConfirmDelete();
  }

  onConfirmDelete() {
    let dialog = this.dialog.open(DialogBodyComponent, {
      data: {
        title: 'Confirm Remove Faq',
        message: 'Are you sure, you want to delete faq '
      }
    });
    dialog.afterClosed()
      .subscribe(selection => {
        if (selection) {
          this.onDeleteChecked();
        }
      });
  }


  onDeleteChecked() {
    this.checkedDeleteLoader = true;
    this.isSearch = false;
    const getChecked = this.selection.selected;

    getChecked.map((data) => {
      this.checkedId.push(data.faq_id);
    });

    this.deleteFaqSubscription = this.data.deleteCheckedFaqs(this.checkedId).subscribe((data) => {
      this.getFaqSubscription = this.data.getFaqs(
        this.setPageIndex, this.paginator.pageSize)
        .subscribe((data: any) => {
          this.resultsLength = data.paginationObject.totalRecord;
          this.dataSource = new MatTableDataSource<FAQs>(data.result);
          this.dataSource.sort = this.sort;
          this.selection.clear();
          this.searchForm.reset();
          this.noFaqRecord = false;

          this.toastr.success('Deleted successful!', 'Success!', {
            enableHtml: true,
            closeButton: true,
            timeOut: 3000
          });

          this.checkedDeleteLoader = false;
          this.checkedId.length = 0;
    });
    })

    this.subscriptions.push(this.getFaqSubscription);
    this.subscriptions.push(this.deleteFaqSubscription);
  }

  editFaq(element) {
    this.showEditFaqPanel = true;
    this.faq_id = element.faq_id;
    this.editFaqForm.patchValue({
      question: element.question,
      answer: element.answer,
      status: element.status
    })

  }

  loadFaqForm() {
    this.editFaqForm  =  this.formBuilder.group({
      question: ['', Validators.required],
      answer: ['', Validators.required],
      status: ['', Validators.required],
    });
  }

  updateFaq() {
    this.submitted = true;
      this.updateLoading = true;
      this.updateSubscription = this.data.updateFaq(this.editFaqForm.value, this.faq_id).subscribe((data: any) => {
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
        this.updateLoading = false;
        this.showEditFaqPanel = false;
        this.getFaqs();
      });

      this.subscriptions.push(this.updateSubscription);

  }


  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    if (numSelected > 0) {
      this.checkedBtn = true;
    } else {
      this.checkedBtn = false;
    }
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: FAQs): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.question + 1}`;
  }

  navigateTo(row: any) {
    this.router.navigate(['/admin/'+row.id]);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
