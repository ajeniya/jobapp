import { Component, OnInit, OnDestroy } from '@angular/core';
import { Country, ManageCountryService } from '../manage-country/manage-country.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { EventServicesService } from 'src/app/services/event-services.service';
import { ListStateService } from '../list-states/list-state.service';
import { AddCitiesService } from './add-cities.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-cities',
  templateUrl: './add-cities.component.html',
  styleUrls: ['./add-cities.component.scss']
})
export class AddCitiesComponent implements OnInit, OnDestroy {
  navBar: void;
  stateModel: any = {};
  countries: Country[];
  states: any[];
  subscriptions: Subscription[] = [];
  getCountriesSubscription: Subscription;
  getStateSubscription: Subscription;
  addCitySubscription: Subscription;
  selectedstateId: number;
  submitted = false;
  loading: boolean;

  constructor(private eventServices: EventServicesService, public citiesData: AddCitiesService,
    public state: ListStateService, private toastr: ToastrService, public country: ManageCountryService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.getCountries();

  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    this.addCitySubscription = this.citiesData.addCity(this.stateModel).subscribe((data: any) => {
      this.toastr.success('Added successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.addCitySubscription);
  }


  getCountries() {
    this.getCountriesSubscription = this.country.getCountriesList().subscribe((data: Country[]) => {
       this.countries = data;

       //this.getState();
    });
    this.subscriptions.push(this.getCountriesSubscription);
  }

  getCountriesId(event) {
    this.getState(event);
  }

  getStateId(event) {
    this.selectedstateId = event;
    console.log(event);
  }

  getState(countryId){
    this.getStateSubscription = this.citiesData.getStates(countryId).subscribe((data: any[]) => {
      this.states = data;
   });

   this.subscriptions.push(this.getStateSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
