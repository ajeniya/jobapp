import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AddCitiesService {

  constructor(private http: HttpClient) { }

  addCity(formData){
    return this.http.post<any>(`${environment.baseUrl}/Location/addcity`,
    {
      'name': formData.city,
      'stateID': formData.state
    }).pipe(map(result => {
            return result;
      }));
  }

  getStates(countryId){
    return this.http.get<any>(`${environment.baseUrl}/Location/getstatelist?countryID=${countryId}`,{})
    .pipe(map(result => {
            return result;
      }));
  }
}
