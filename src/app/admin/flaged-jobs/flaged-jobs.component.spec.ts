import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FlagedJobsComponent } from './flaged-jobs.component';

describe('FlagedJobsComponent', () => {
  let component: FlagedJobsComponent;
  let fixture: ComponentFixture<FlagedJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlagedJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlagedJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
