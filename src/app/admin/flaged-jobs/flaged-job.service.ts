import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { Job, CompanyLists } from './flaged-job.model';

@Injectable({
  providedIn: 'root'
})
export class FlagedJobService {

  jobs: Job[] = [
    {
     'job_id': '1',
     'company_name': 'Prindex',
     'reasons': 'We are an equal opportunity employer and value diversity at our company. We do not discriminate on the basis of race, religion, colour, national origin, gender, sexual orientation, age, marital status, veteran status, or disability status,',
     'title': 'Business Manager',
     'location': ['Lagos', 'Oyo'],
     'date_created': '1 Week Ago'
    },
    {
     'job_id': '2',
     'company_name': 'Akete Studio',
     'reasons': 'The recruter is not real',
     'title': 'Devops',
     'location': ['Lagos', 'Kogi', 'Abuja'],
     'date_created': '1 Week Ago'
    },
    {
     'job_id': '3',
     'company_name': 'IBM',
     'reasons': 'We are an equal opportunity employer and value diversity at our company. We do not discriminate on the basis of race, religion, colour, national origin, gender, sexual orientation, age, marital status, veteran status, or disability status. ',
     'title': 'Developer Backend',
     'location': ['Lagos'],
     'date_created': '1 Week Ago'
    },
    {
      'job_id': '2',
      'company_name': 'Akete Studio',
      'reasons': 'The recruter is not real',
      'title': 'Devops',
      'location': ['Lagos', 'Kogi', 'Abuja','Lagos', 'Kogi', 'Abuja'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '3',
      'company_name': 'IBM',
      'reasons': 'Andela is a network of technology leaders dedicated to advancing human potential. We help companies build high-performing distributed engineering teams by investing in Africa’s most talented software engineers Based in NYC, SF, Kigali, Cairo, Lagos, Nairobi, Accra and Kampala, Andela is catalyzing the growth tech ecosystems across the African continent while solving the global technical talent shortage.',
      'title': 'Developer Backend',
      'location': ['Lagos'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '1',
      'company_name': 'Prindex',
      'reasons': 'We are an equal opportunity employer and value diversity at our company. We do not discriminate on the basis of race, religion, colour, national origin, gender, sexual orientation, age, marital status, veteran status, or disability status. ',
      'title': 'Business Manager',
      'location': ['Lagos', 'Oyo'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '2',
      'company_name': 'Akete Studio',
      'reasons': 'The recruter is not real',
      'title': 'Devops',
      'location': ['Lagos', 'Kogi', 'Abuja'],
      'date_created': '1 Week Ago'
     },
     {
      'job_id': '3',
      'company_name': 'IBM',
      'reasons': 'The recruter is not real',
      'title': 'Developer Backend',
      'location': ['Lagos'],
      'date_created': '1 Week Ago'
     },
     {
       'job_id': '2',
       'company_name': 'Akete Studio',
       'reasons': 'The recruter is not real',
       'title': 'Devops',
       'location': ['Lagos', 'Kogi', 'Abuja'],
       'date_created': '1 Week Ago'
      },
      {
       'job_id': '3',
       'company_name': 'IBM',
       'title': 'Developer Backend',
       'reasons': 'The recruter is not real',
       'location': ['Lagos'],
       'date_created': '1 Week Ago'
      }, {
        'job_id': '1',
        'company_name': 'Prindex',
        'reasons': 'The recruter is not real',
        'title': 'Business Manager',
        'location': ['Lagos', 'Oyo'],
        'date_created': '1 Week Ago'
       },
       {
        'job_id': '2',
        'company_name': 'Akete Studio',
        'reasons': 'The recruter is not real',
        'title': 'Devops',
        'location': ['Lagos', 'Kogi', 'Abuja'],
        'date_created': '1 Week Ago'
       },
       {
        'job_id': '3',
        'company_name': 'IBM',
        'reasons': 'The recruter is not real',
        'title': 'Developer Backend',
        'location': ['Lagos'],
        'date_created': '1 Week Ago'
       },
       {
         'job_id': '2',
         'company_name': 'Akete Studio',
         'reasons': 'The recruter is not real',
         'title': 'Devops',
         'location': ['Lagos', 'Kogi', 'Abuja'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '3',
         'company_name': 'IBM',
         'reasons': 'The recruter is not real',
         'title': 'Developer Backend',
         'location': ['Lagos'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '1',
         'company_name': 'Prindex',
         'reasons': 'The recruter is not real',
         'title': 'Business Manager',
         'location': ['Lagos', 'Oyo'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '2',
         'company_name': 'Akete Studio',
         'reasons': 'The recruter is not real',
         'title': 'Devops',
         'location': ['Lagos', 'Kogi', 'Abuja'],
         'date_created': '1 Week Ago'
        },
        {
         'job_id': '3',
         'company_name': 'IBM',
         'reasons': 'The recruter is not real',
         'title': 'Developer Backend',
         'location': ['Lagos'],
         'date_created': '1 Week Ago'
        },
        {
          'job_id': '2',
          'company_name': 'Akete Studio',
          'reasons': 'The recruter is not real',
          'title': 'Devops',
          'location': ['Lagos', 'Kogi', 'Abuja'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '3',
          'company_name': 'IBM',
          'reasons': 'The recruter is not real',
          'title': 'Developer Backend',
          'location': ['Lagos'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '1',
          'company_name': 'Prindex',
          'reasons': 'The recruter is not real',
          'title': 'Business Manager',
          'location': ['Lagos', 'Oyo'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '2',
          'company_name': 'Akete Studio',
          'reasons': 'The recruter is not real',
          'title': 'Devops',
          'location': ['Lagos', 'Kogi', 'Abuja'],
          'date_created': '1 Week Ago'
         },
         {
          'job_id': '3',
          'company_name': 'IBM',
          'reasons': 'The recruter is not real',
          'title': 'Developer Backend',
          'location': ['Lagos'],
          'date_created': '1 Week Ago'
         },
         {
           'job_id': '2',
           'company_name': 'Akete Studio',
           'reasons': 'The recruter is not real',
           'title': 'Devops',
           'location': ['Lagos', 'Kogi', 'Abuja'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '3',
           'company_name': 'IBM',
           'reasons': 'The recruter is not real',
           'title': 'Developer Backend',
           'location': ['Lagos'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '1',
           'company_name': 'Prindex',
           'reasons': 'The recruter is not real',
           'title': 'Business Manager',
           'location': ['Lagos', 'Oyo'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '2',
           'company_name': 'Akete Studio',
           'title': 'Devops',
           'location': ['Lagos', 'Kogi', 'Abuja'],
           'date_created': '1 Week Ago'
          },
          {
           'job_id': '3',
           'company_name': 'IBM',
           'title': 'Developer Backend',
           'location': ['Lagos'],
           'date_created': '1 Week Ago'
          },
          {
            'job_id': '2',
            'company_name': 'Akete Studio',
            'title': 'Devops',
            'location': ['Lagos', 'Kogi', 'Abuja'],
            'date_created': '1 Week Ago'
           },
           {
            'job_id': '3',
            'company_name': 'IBM',
            'title': 'Developer Backend',
            'location': ['Lagos'],
            'date_created': '1 Week Ago'
           }, {
             'job_id': '1',
             'company_name': 'Prindex',
             'title': 'Business Manager',
             'location': ['Lagos', 'Oyo'],
             'date_created': '1 Week Ago'
            },
            {
             'job_id': '2',
             'company_name': 'Akete Studio',
             'title': 'Devops',
             'location': ['Lagos', 'Kogi', 'Abuja'],
             'date_created': '1 Week Ago'
            },
            {
             'job_id': '3',
             'company_name': 'IBM',
             'title': 'Developer Backend',
             'location': ['Lagos'],
             'date_created': '1 Week Ago'
            },
            {
              'job_id': '2',
              'company_name': 'Akete Studio',
              'title': 'Devops',
              'location': ['Lagos', 'Kogi', 'Abuja'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '3',
              'company_name': 'IBM',
              'title': 'Developer Backend',
              'location': ['Lagos'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '1',
              'company_name': 'Prindex',
              'title': 'Business Manager',
              'location': ['Lagos', 'Oyo'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '2',
              'company_name': 'Akete Studio',
              'title': 'Devops',
              'location': ['Lagos', 'Kogi', 'Abuja'],
              'date_created': '1 Week Ago'
             },
             {
              'job_id': '3',
              'company_name': 'IBM',
              'title': 'Developer Backend',
              'location': ['Lagos'],
              'date_created': '1 Week Ago'
             },
             {
               'job_id': '2',
               'company_name': 'Akete Studio',
               'title': 'Devops',
               'location': ['Lagos', 'Kogi', 'Abuja'],
               'date_created': '1 Week Ago'
              },
              {
               'job_id': '3',
               'company_name': 'IBM',
               'title': 'Developer Backend',
               'location': ['Lagos'],
               'date_created': '1 Week Ago'
              }
  ];

  companyLists: CompanyLists[] = [
    {
    'company_id': '1',
    'company_name': 'Invitea'
   },
   {
    'company_id': '1',
    'company_name': 'Testfit'
   },
   {
    'company_id': '1',
    'company_name': 'Itemize'
   },
   {
    'company_id': '1',
    'company_name': 'Teampay'
   },
   {
    'company_id': '1',
    'company_name': 'PepsiCo'
   },
   {
    'company_id': '1',
    'company_name': 'Speedway'
   },
   {
    'company_id': '1',
    'company_name': 'Lyft'
   },
   {
    'company_id': '1',
    'company_name': 'Coney Island prep',
   },
   {
    'company_id': '1',
    'company_name': 'Aramark'
   }
  ];

   constructor() { }

   getJob() {
     return of(this.jobs);
   }

   getCompanies() {
    return of(this.companyLists);
  }
}
