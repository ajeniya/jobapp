import { TestBed } from '@angular/core/testing';

import { FlagedJobService } from './flaged-job.service';

describe('FlagedJobService', () => {
  let service: FlagedJobService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FlagedJobService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
