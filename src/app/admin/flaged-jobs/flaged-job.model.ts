export class Job {
  job_id?: string;
  company_name?: string;
  title?: string;
  reasons?: string;
  location?: string[];
  date_created?: string;
  status?: string;
}

export class CompanyLists {
  company_id: string;
  company_name: string;
}
