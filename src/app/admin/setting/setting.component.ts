import { Component, OnInit } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  navBar: void;

  constructor(private eventServices: EventServicesService) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
  }

}
