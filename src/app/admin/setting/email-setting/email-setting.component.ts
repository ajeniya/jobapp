import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailSettingService } from './email-setting.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-setting',
  templateUrl: './email-setting.component.html',
  styleUrls: ['./email-setting.component.scss']
})
export class EmailSettingComponent implements OnInit {
  getSettingSubscription: Subscription;
  updateSendGridSubscription: Subscription;
  updateSmtpSubscription: Subscription;
  subscriptions: Subscription[] = [];
  emailSettingData: any;
  smtpForm: FormGroup;
  sendGridForm: FormGroup;
  submitted = false;
  mailDriver: any;
  loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private cd: ChangeDetectorRef,
    private toastr: ToastrService,
    public data: EmailSettingService ) { }

  ngOnInit(): void {
    this.loadFormSmtp();
    this.getSetting();
    this.loadFormSendGrid();
  }

  loadFormSmtp() {
    this.smtpForm = this.formBuilder.group({
      mailHostSmtp: ['', Validators.required],
      mailPortSmtp: ['', Validators.required],
      usernameSmtp: ['', Validators.required],
      passwordSmtp: ['', Validators.required]
    });
  }

  loadFormSendGrid() {
    this.sendGridForm = this.formBuilder.group({
      mailHostSendGrid: ['', Validators.required],
      mailPortSendGrid: ['', Validators.required],
      usernameSendGrid: ['', Validators.required],
      passwordSendGrid: ['', Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get smtp() { return this.smtpForm.controls; }

  get sendGrid() { return this.sendGridForm.controls; }


  onSubmitSmtp() {
    this.submitted = true;
    this.loading = true;
    this.updateSmtpSubscription = this.data.updateSmtpGrid(this.smtpForm.value, this.mailDriver).subscribe((result: any) => {
      this.mailDriver = result.mailType;
      this.smtpForm.patchValue({
        mailHostSendGrid: result.sendGridhost,
        mailPortSendGrid: result.sendGridport,
        usernameSendGrid: result.sendGridusername,
        passwordSendGrid: result.sendGridpassword
      })
      this.toastr.success('Updated successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.updateSmtpSubscription);
  }

  onSubmitSendGrid() {
    this.submitted = true;
    this.loading = true;
    this.updateSendGridSubscription = this.data.updateSendGrid(this.sendGridForm.value, this.mailDriver).subscribe((result: any) => {
      this.mailDriver = result.mailType;
      this.sendGridForm.patchValue({
        mailHostSendGrid: result.sendGridhost,
        mailPortSendGrid: result.sendGridport,
        usernameSendGrid: result.sendGridusername,
        passwordSendGrid: result.sendGridpassword
      })
      this.toastr.success('Updated successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.updateSendGridSubscription);
  }



  getSetting() {
    this.getSettingSubscription = this.data.getMailSetting().subscribe((result: any) => {
      this.mailDriver = result.data.mailType;
      this.smtpForm.patchValue({
        mailHostSmtp: result.data.smtphost,
        mailPortSmtp: result.data.smtpport,
        usernameSmtp: result.data.smtpusername,
        passwordSmtp: result.data.smtppassword
      });

      this.sendGridForm.patchValue({
        mailHostSendGrid: result.data.sendGridhost,
        mailPortSendGrid: result.data.sendGridport,
        usernameSendGrid: result.data.sendGridusername,
        passwordSendGrid: result.data.sendGridpassword
      })
   });

   this.subscriptions.push(this.getSettingSubscription);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }


}

