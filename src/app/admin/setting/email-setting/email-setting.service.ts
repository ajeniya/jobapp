import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class EmailSettingService {

  constructor(private http: HttpClient) { }

  getMailSetting() {
    return this.http.post<any>(`${environment.baseUrl}/MailDriver/getMailSetting`, {})
        .pipe(map(setting => {
            return setting;
        }));
  }

  updateSendGrid(formData, mailDriver) {
    return this.http.post<any>(`${environment.baseUrl}/MailDriver/updateSendgridMailSetting`,
    {
      'mailDriver': mailDriver,
      'mailHostSendGrid': formData.mailHostSendGrid,
      'mailPortSendGrid': formData.mailPortSendGrid,
      'usernameSendGrid': formData.usernameSendGrid,
      'passwordSendGrid': formData.passwordSendGrid
    }).pipe(map(setting => {
            return setting;
      }));
  }


  updateSmtpGrid(formData, mailDriver) {
    return this.http.post<any>(`${environment.baseUrl}/MailDriver/updateSmtpMailSetting`,
    {
      'mailDriver': mailDriver,
      'mailHostSmtp': formData.mailHostSmtp,
      'mailPortSmtp': formData.mailPortSmtp,
      'usernameSmtp': formData.usernameSmtp,
      'passwordSmtp': formData.passwordSmtp
    }).pipe(map(setting => {
            return setting;
      }));
  }
}

