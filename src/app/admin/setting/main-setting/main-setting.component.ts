import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { Country, ManageCountryService } from '../../manage-country/manage-country.service';
import { MainSettingService, CurrencyCode, SettingData } from './main-setting.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-main-setting',
  templateUrl: './main-setting.component.html',
  styleUrls: ['./main-setting.component.scss']
})
export class MainSettingComponent implements OnInit {
  getCountriesSubscription: Subscription;
  getCurrencyCodeSubscription: Subscription;
  getSettingSubscription: Subscription;
  getStatesSubscription: Subscription;
  updateSetting: Subscription;
  subscriptions: Subscription[] = [];
  countries: Country[];
  currencyCodes: CurrencyCode[];
  settingData: SettingData;
  settingForm: FormGroup;
  submitted = false;
  loading = false;
  setLogo: any;

  selectedLogoFile = null;
  states: any;
  selectedCountryId: any;

  constructor(public country: ManageCountryService,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef,
    public data: MainSettingService ) { }

  ngOnInit(): void {
    this.getCountries();
    this.getCurrencyCode();
    this.loadForm();
    this.getSetting();
  }

  loadForm() {
    this.settingForm = this.formBuilder.group({
      logo: ['', Validators.required],
      siteName: ['', Validators.required],
      siteSlogan: [''],
      phone: [''],
      fromEmail: ['', Validators.required],
      fromEmailName: [''],
      toEmail: [''],
      emailName: [''],
      defaultCountry: ['', Validators.required],
      defaultState: ['', Validators.required],
      specificCountry: ['', Validators.required],
      defaultCurrency: ['', Validators.required],
      currencyCode: ['', Validators.required],
      address: [],
      domain: [],
      aboutSite: []
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.settingForm.controls; }


  onFileSelected(event) {
    this.selectedLogoFile = event.target.files[0];
  }

  onSubmit() {
    this.submitted = true;
    this.loading = true;

    const domain = this.settingForm.controls['domain'].value.replace(/(^\w+:|^)\/\//, '');
    const formData = new FormData();
    if(this.selectedLogoFile) {
      formData.append('logo', this.selectedLogoFile, this.selectedLogoFile.name);
    }
    formData.append('siteName', this.settingForm.get('siteName').value);
    formData.append('siteSlogan', this.settingForm.get('siteSlogan').value);
    formData.append('phone', this.settingForm.get('phone').value);
    formData.append('fromEmail', this.settingForm.get('fromEmail').value);
    formData.append('fromEmailName', this.settingForm.get('fromEmailName').value);
    formData.append('toEmail', this.settingForm.get('toEmail').value);
    formData.append('emailName', this.settingForm.get('emailName').value);
    formData.append('defaultCountry', this.settingForm.get('defaultCountry').value);
    formData.append('defaultState', this.settingForm.get('defaultState').value);
    formData.append('specificCountry', this.settingForm.get('specificCountry').value);
    formData.append('defaultCurrency', this.settingForm.get('defaultCurrency').value);
    formData.append('currencyCode', this.settingForm.get('currencyCode').value);
    formData.append('address', this.settingForm.get('address').value);
    formData.append('domain', domain);
    formData.append('aboutSite', this.settingForm.get('aboutSite').value);

    this.http.post(`${environment.baseUrl}/SuperAdmin/updatesitesetting`, formData ).subscribe((data: any) => {
      this.loading = false;
        this.setLogo = data.logo;
        this.settingForm.patchValue({
          logo: data.logo,
          siteName: data.siteName,
          siteSlogan: data.siteName,
          phone: data.phone,
          fromEmail: data.fromEmail,
          fromEmailName: data.fromEmailName,
          toEmail: data.toEmail,
          emailName: data.emailName,
          defaultCountry: data.defaultCountry,
          specificCountry: data.specificCountry,
          defaultCurrency: data.defaultCurrency,
          currencyCode: data.currencyCode,
          address: data.address,
          domain: data.domain,
          aboutSite: data.aboutSite
        })
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
    });
  }

  getCountries() {
    this.getCountriesSubscription = this.country.getCountriesList().subscribe((data: Country[]) => {
       this.countries = data;
    });

    this.subscriptions.push(this.getCountriesSubscription);
  }

  getSelectedCountryId(event) {
    this.selectedCountryId = event.target.value;
    this.getStates(this.selectedCountryId);
  }


  getStates(stateId) {
    this.getStatesSubscription = this.data.getStateList(stateId).subscribe((data: any) => {
      this.states = data;
    });

    this.subscriptions.push(this.getStatesSubscription);
  }

  getSelectedCurrency(currency) {
    console.log(currency);
  }

  getSetting() {
    this.getSettingSubscription = this.data.getSetting().subscribe((data: SettingData) => {
      this.settingData = data;
      this.setLogo = data.logo;
      this.settingForm.patchValue({
        logo: data.logo,
        siteName: data.siteName,
        siteSlogan: data.siteName,
        phone: data.phone,
        fromEmail: data.fromEmail,
        fromEmailName: data.fromEmailName,
        toEmail: data.toEmail,
        emailName: data.emailName,
        defaultCountry: data.defaultCountry,
        defaultState: data.defaultState,
        specificCountry: data.specificCountry,
        defaultCurrency: data.defaultCurrency,
        currencyCode: data.currencyCode,
        address: data.address,
        domain: data.domain,
        aboutSite: data.aboutSite
      })

      this.getStates(data.defaultCountry);
   });
   this.subscriptions.push(this.getSettingSubscription);
  }

  getCurrencyCode() {
    this.getCurrencyCodeSubscription = this.data.getCurrencyCode().subscribe((data: CurrencyCode[]) => {
      this.currencyCodes = data;
    });
    this.subscriptions.push(this.getCurrencyCodeSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
