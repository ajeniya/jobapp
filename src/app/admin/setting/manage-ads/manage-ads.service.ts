import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ManageAdsService {
 constructor(private http: HttpClient) { }

 getAdsSetting() {
  return this.http.post<any>(`${environment.baseUrl}/Ads/getads`, {})
  .pipe(map(setting => {
      return setting;
  }));
 }


 updateSendGrid(formData: adsSettingData) {
  return this.http.post<any>(`${environment.baseUrl}/Ads/updateads`,
  {
    'homePageAdstop': formData.homePageAdstop,
    'homePageAdsbottom': formData.homePageAdsbottom,
    'jobListPage': formData.jobListPage,
    'jobDetailsPageRightAds': formData.jobDetailsPageRightAds,
    'jobDetailsPage': formData.jobDetailsPage,
    'companyTopAds': formData.companyTopAds,
    'companyBottomAds': formData.companyBottomAds,
    'companyDetailAds': formData.companyDetailAds,
    'jobAppliedAds': formData.jobAppliedAds,
  }).pipe(map(setting => {
          return setting;
    }));
}
}

export class adsSettingData {
  homePageAdstop: string;
  homePageAdsbottom: string;
  jobListPage: string;
  jobDetailsPageRightAds: string;
  jobDetailsPage: string;
  companyTopAds: string;
  companyBottomAds: string;
  companyDetailAds: string;
  jobAppliedAds: string;
}
