import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ManageAdsService, adsSettingData } from './manage-ads.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-manage-ads',
  templateUrl: './manage-ads.component.html',
  styleUrls: ['./manage-ads.component.scss']
})
export class ManageAdsComponent implements OnInit, OnDestroy {
  AdsSettingForm: FormGroup;
  getAdsSettingSubscription: Subscription;
  updateAdsSubscription: Subscription;
  submitted = false;
  subscriptions: Subscription[] = [];
  loading = false;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public data: ManageAdsService) { }

  ngOnInit(): void {
    this.loadForm();
    this.getSetting();
  }

  loadForm() {
    this.AdsSettingForm = this.formBuilder.group({
      homePageAdstop: [''],
      homePageAdsbottom: [''],
      jobListPage: [''],
      jobDetailsPageRightAds: [''],
      jobDetailsPage: [''],
      companyTopAds: [],
      companyBottomAds: [''],
      companyDetailAds: [''],
      jobAppliedAds: ['']
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.AdsSettingForm.controls; }

  getSetting() {
    this.getAdsSettingSubscription = this.data.getAdsSetting().subscribe((data: adsSettingData) => {
      this.AdsSettingForm.patchValue({
        homePageAdstop: data.homePageAdstop,
        homePageAdsbottom: data.homePageAdsbottom,
        jobListPage: data.jobListPage,
        jobDetailsPageRightAds: data.jobDetailsPageRightAds,
        jobDetailsPage: data.jobDetailsPage,
        companyTopAds: data.companyTopAds,
        companyBottomAds: data.companyBottomAds,
        companyDetailAds: data.companyDetailAds,
        jobAppliedAds: data.jobAppliedAds
      })
   });

   this.subscriptions.push(this.getAdsSettingSubscription);
  }


  onSubmitAds() {
    this.submitted = true;
    this.loading = true;
    this.updateAdsSubscription = this.data.updateSendGrid(this.AdsSettingForm.value).subscribe((data: any) => {
      this.AdsSettingForm.patchValue({
        homePageAdstop: data.homePageAdstop,
        homePageAdsbottom: data.homePageAdsbottom,
        jobListPage: data.jobListPage,
        jobDetailsPageRightAds: data.jobDetailsPageRightAds,
        jobDetailsPage: data.jobDetailsPage,
        companyTopAds: data.companyTopAds,
        companyBottomAds: data.companyBottomAds,
        companyDetailAds: data.companyDetailAds,
        jobAppliedAds: data.jobAppliedAds
      })
      this.toastr.success('Updated successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.updateAdsSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
