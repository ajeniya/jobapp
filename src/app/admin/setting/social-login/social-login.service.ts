import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})

export class SocialLoginService {

  settingData: SocialLogin = {
    'googleAppId': '3726862332y23y82y3',
    'googleAppSecret': '124612715t762652863'
  }


  constructor() { }

  getSetting() {
    return of(this.settingData);
  }
}

export class SocialLogin {
  googleAppId: string;
  googleAppSecret: string;
}

