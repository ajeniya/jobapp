import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs/internal/Subscription';
import { SocialLogin, SocialLoginService } from './social-login.service';

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.scss']
})
export class SocialLoginComponent implements OnInit, OnDestroy {
  getSocialSubscription: Subscription;
  subscriptions: Subscription[] = [];
  socialLoginData: SocialLogin;
  socialForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder, public data: SocialLoginService) { }

  ngOnInit(): void {
    this.loadForm();
    this.getSocialLogin();
  }

  loadForm() {
    this.socialForm = this.formBuilder.group({
      googleAppId: [''],
      googleAppSecret: ['']
    });
  }

  onSubmit() {
    this.submitted = true;

    // display form values on success
    console.log('SUCCESS!! :-)\n\n' + JSON.stringify(this.socialForm.value, null, 4));
  }

  getSocialLogin() {
    this.getSocialSubscription = this.data.getSetting().subscribe((data: SocialLogin) => {
       this.socialForm.patchValue({
        googleAppId: data.googleAppId,
        googleAppSecret: data.googleAppSecret
      })
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.socialForm.controls; }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
