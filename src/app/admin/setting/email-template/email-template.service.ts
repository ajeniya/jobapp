import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class EmailTemplateService {
  constructor(private http: HttpClient) { }

  getEmailTemplateSetting() {
    return this.http.post<any>(`${environment.baseUrl}/EmailTemplate/setTemplate`, {})
        .pipe(map(setting => {
            return setting;
        }));
  }


  updateEmailTemplate(formData: EmailTemplateSettingData) {
    return this.http.post<any>(`${environment.baseUrl}/EmailTemplate/updateEmailTemplate`,
    {
      'createAccountSubject': formData.createAccountSubject,
      'createAccountMessage': formData.createAccountMessage,
      'forgotPasswordSubject': formData.forgotPasswordSubject,
      'forgotPasswordMessage': formData.forgotPasswordMessage,
      'reportJobSubject': formData.reportJobSubject,
      'reportJobMessage': formData.reportJobMessage,
      'approvedJobTitle': formData.applyJobTitle,
      'approvedJobMessage': formData.approvedJobMessage,
      'applyJobTitle': formData.applyJobTitle,
      'applyJobMessage': formData.applyJobMessage,
      'newJobNotificationToSubscriberTitle': formData.newJobNotificationToSubscriberTitle,
      'newJobNotificationToSubscriberMessage': formData.newJobNotificationToSubscriberMessage
    }).pipe(map(setting => {
            return setting;
      }));
  }
}

 export class EmailTemplateSettingData {
   createAccountSubject: string;
   createAccountMessage: string;
   forgotPasswordSubject: string;
   forgotPasswordMessage: string;
   reportJobSubject: string;
   reportJobMessage: string;
   approvedJobTitle: string;
   approvedJobMessage: string;
   applyJobTitle: string;
   applyJobMessage: string;
   newJobNotificationToSubscriberTitle: string;
   newJobNotificationToSubscriberMessage: string;
 }
