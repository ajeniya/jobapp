import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmailTemplateService, EmailTemplateSettingData } from './email-template.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-email-template',
  templateUrl: './email-template.component.html',
  styleUrls: ['./email-template.component.scss']
})
export class EmailTemplateComponent implements OnInit, OnDestroy {
  getEmailTemplateSubscription: Subscription;
  updateEmailTemplateSubscription: Subscription;
  subscriptions: Subscription[] = [];
  emailTemplateForm: FormGroup;
  submitted = false;
  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo']
  };
  error: any;
  loading = false;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public data: EmailTemplateService) { }

  ngOnInit(): void {
    this.loadFormEmailTemplate();
    this.getEmailTemplateSetting();
  }

  loadFormEmailTemplate() {
    this.emailTemplateForm = this.formBuilder.group({
      createAccountSubject: ['', Validators.required],
      createAccountMessage: ['', Validators.required],
      forgotPasswordSubject: ['', Validators.required],
      forgotPasswordMessage: ['', Validators.required],
      reportJobSubject: ['', Validators.required],
      reportJobMessage: ['', Validators.required],
      approvedJobTitle: ['', Validators.required],
      approvedJobMessage: ['', Validators.required],
      applyJobTitle: ['', Validators.required],
      applyJobMessage: ['', Validators.required],
      newJobNotificationToSubscriberTitle: ['', Validators.required],
      newJobNotificationToSubscriberMessage: ['', Validators.required],
    });
  }

  // convenience getter for easy access to form fields
  get emailTemplateFormGetter() { return this.emailTemplateForm.controls; }

  onSubmit() {
    this.submitted = true;
    this.loading = true;
    // display form values on success
    this.updateEmailTemplateSubscription = this.data.updateEmailTemplate(this.emailTemplateForm.value).subscribe((data: any) => {
      this.emailTemplateForm.patchValue({
        createAccountSubject: data.account_confirmation_title,
        createAccountMessage: data.account_confirmation_body,
        forgotPasswordSubject: data.forgot_password_email_title,
        forgotPasswordMessage: data.forgot_password_email_body,
        reportJobSubject: data.report_email_title,
        reportJobMessage: data.report_email_body,
        approvedJobTitle: data.job_approval_email_title,
        approvedJobMessage: data.job_approval_email_body,
        applyJobTitle: data.apply_for_job_email_title,
        applyJobMessage: data.apply_for_job_email_body,
        newJobNotificationToSubscriberTitle: data.job_notification_to_subscriber_title,
        newJobNotificationToSubscriberMessage: data.job_notification_to_subscriber_body
      })
      this.toastr.success('Updated successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.updateEmailTemplateSubscription);
  }

  getEmailTemplateSetting() {
    this.getEmailTemplateSubscription = this.data.getEmailTemplateSetting().subscribe((data: any) => {
      this.emailTemplateForm.patchValue({
        createAccountSubject: data.account_confirmation_title,
        createAccountMessage: data.account_confirmation_body,
        forgotPasswordSubject: data.forgot_password_email_title,
        forgotPasswordMessage: data.forgot_password_email_body,
        reportJobSubject: data.report_email_title,
        reportJobMessage: data.report_email_body,
        approvedJobTitle: data.job_approval_email_title,
        approvedJobMessage: data.job_approval_email_body,
        applyJobTitle: data.apply_for_job_email_title,
        applyJobMessage: data.apply_for_job_email_body,
        newJobNotificationToSubscriberTitle: data.job_notification_to_subscriber_title,
        newJobNotificationToSubscriberMessage: data.job_notification_to_subscriber_body
      })
   });

   this.subscriptions.push(this.getEmailTemplateSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
