import { Component, OnInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PaymentGatewayService, SettingPaypalDataObject,
        SettingStripeDataObject, BankTranfer } from './payment-gateway.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-payment-gateway',
  templateUrl: './payment-gateway.component.html',
  styleUrls: ['./payment-gateway.component.scss']
})
export class PaymentGatewayComponent implements OnInit, OnDestroy {
  getPaypalSettingSubscription: Subscription;
  getStripeSettingSubscription: Subscription;
  getPaymentSettingSubscription: Subscription;
  updatePaymentTypeValues: Subscription;
  updatePaypal: Subscription;
  updateStripe: Subscription;
  updateBankTransfer: Subscription;
  subscriptions: Subscription[] = [];
  emailSettingData: any;
  paypalForm: FormGroup;
  stripeForm: FormGroup;
  bankTransferForm: FormGroup;
  submittedPaypal = false;
  submittedStripe = false;
  submittedBankTransfer = false;
  paypal = false;
  stripe = false;
  bankTransfer = false;
  loadingPaypal = false;
  loadingStripe = false;
  loadingBankTransfer = false;

  constructor(
    private formBuilder: FormBuilder,
    public data: PaymentGatewayService,
    private toastr: ToastrService,
    private cd: ChangeDetectorRef ) { }

  ngOnInit(): void {
    this.loadFormPaypal();
    this.loadFormStripe();
    this.loadFormBankTransfer();
    this.getPaymentSetting();
  }

  loadFormPaypal() {
    this.paypalForm = this.formBuilder.group({
      paypalAccount: ['', Validators.required],
      paypalClentId: ['', Validators.required],
      paypalSecret: ['', Validators.required],
      isPaypalSandbox: ['', Validators.required],
      isPaypalActive: ['', Validators.required]
    });
  }

  loadFormStripe() {
    this.stripeForm = this.formBuilder.group({
      stripePublisedKey: ['', Validators.required],
      stripeSecrectKey: ['', Validators.required],
      isStripeActive: ['', Validators.required],
    });
  }

  loadFormBankTransfer() {
    this.bankTransferForm = this.formBuilder.group({
      bankTranser: ['', Validators.required]
    });
  }


  getPaymentSetting() {
    this.getPaymentSettingSubscription = this.data.getPaymentSetting().subscribe((data: any) => {
    const bankTranfer = JSON.parse(data.bank_transfer);
    const Paypal = JSON.parse(data.paypal);
    const Stripe = JSON.parse(data.stripe);
    const PaymentType = JSON.parse(data.payment_type);

    this.paypal = PaymentType.paypal;
    this.stripe = PaymentType.stripe;
    this.bankTransfer = PaymentType.bankTranfer;

    this.paypalForm.patchValue({
      paypalAccount: Paypal.paypalAccount,
      paypalClentId: Paypal.paypalClentId,
      paypalSecret: Paypal.paypalSecret,
      isPaypalSandbox: Paypal.isPaypalSandbox,
      isPaypalActive: Paypal.isPaypalActive
    })

    this.stripeForm.patchValue({
      stripePublisedKey: Stripe.stripePublisedKey,
      stripeSecrectKey: Stripe.stripeSecrectKey,
      isStripeActive: Stripe.isStripeActive
    })

    this.bankTransferForm.patchValue({
      bankTranser: bankTranfer.bankTranser
    })
   });

   this.subscriptions.push(this.getPaymentSettingSubscription);
  }

  addPaymentMethod() {
    const PaymentChecked =  {
        paypal: this.paypal,
        stripe: this.stripe,
        bankTranfer: this.bankTransfer
      }
    this.updatePaymentTypeValues = this.data.updatePaymentType(PaymentChecked).subscribe((data) => {
    });

    this.subscriptions.push(this.updatePaymentTypeValues);

  }

    // convenience getter for easy access to form fields
    get paypalFormGetter() { return this.paypalForm.controls; }

    get stripeFormGetter() { return this.stripeForm.controls; }

    get bankTransferFormGetter() { return this.bankTransferForm.controls; }



    onSubmitPaypal() {
      this.submittedPaypal = true;
      this.loadingPaypal = true;
      // display form values on success
      this.updatePaypal = this.data.updatePaypal(this.paypalForm.value).subscribe((data) => {
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
        this.loadingPaypal = false;
      });

      this.subscriptions.push(this.updatePaypal);
    }

    onSubmitStripe() {
      this.submittedStripe = true;
      this.loadingStripe = true;
      // display form values on success
      this.updateStripe = this.data.updateStripe(this.stripeForm.value).subscribe((data) => {
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
        this.loadingStripe = false;
      });

      this.subscriptions.push(this.updateStripe);
    }

    onSubmitBankTransfer() {
      this.submittedBankTransfer = true;
      this.loadingBankTransfer = true;
      // display form values on success
      this.updateBankTransfer = this.data.updateBanktransfer(this.bankTransferForm.value).subscribe((data) => {
        this.toastr.success('Updated successful!', 'Success!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
        });
        this.loadingBankTransfer = false;
      });

      this.subscriptions.push(this.updateBankTransfer);
    }


    ngOnDestroy() {
      this.subscriptions.forEach((subscription) => subscription.unsubscribe());
    }

}
