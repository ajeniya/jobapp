import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class PaymentGatewayService {

  constructor(private http: HttpClient) { }

  getPaymentSetting() {
    return this.http.post<any>(`${environment.baseUrl}/Payment/getpaymentsetting`, {})
    .pipe(map(setting => {
        return setting;
    }));
   }

  updatePaymentType(formData) {
    return this.http.post<any>(`${environment.baseUrl}/Payment/updatepaymenttype`,
    { 'paymentType': formData }).pipe(map(setting => {
            return setting;
      }));
  }

  updatePaypal(formData) {
    return this.http.post<any>(`${environment.baseUrl}/Payment/updatepaypal`,
    { 'paypal': formData }).pipe(map(setting => {
            return setting;
      }));
  }

  updateStripe(formData) {
    return this.http.post<any>(`${environment.baseUrl}/Payment/updatestripe`,
    { 'stripe': formData }).pipe(map(setting => {
            return setting;
      }));
  }

  updateBanktransfer(formData) {
    return this.http.post<any>(`${environment.baseUrl}/Payment/updatebanktransfer`,
    { 'banktransfer': formData }).pipe(map(setting => {
            return setting;
      }));
  }
}

export class SettingPaypalDataObject {
  paypalAccount: string;
  paypalClentId: string;
  paypalSecret: string;
  isPaypalSandbox: string;
  isPaypalActive: string;
}

export class SettingStripeDataObject {
  stripePublisedKey: string;
  stripeSecrectKey: string;
  isStripeActive: string;
}

export class BankTranfer {
  bankTransfer: string
}
