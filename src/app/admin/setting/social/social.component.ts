import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { Country, ManageCountryService } from '../../manage-country/manage-country.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SocialService, SocialSettingData } from './social.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.scss']
})
export class SocialComponent implements OnInit {
  subscriptions: Subscription[] = [];
  getSocialSettingSubscription: Subscription;
  updateSocialSubscription: Subscription;
  socialSettingForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(private formBuilder: FormBuilder,
    private toastr: ToastrService,
    public data: SocialService) { }

  ngOnInit(): void {
    this.loadForm();
    this.getSetting();
  }

  loadForm() {
    this.socialSettingForm = this.formBuilder.group({
      facebook: [''],
      pinterest: [''],
      twitter: [''],
      instagram: [''],
      linkdin: [''],
      tumblr: [],
      flickr: ['']
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.socialSettingForm.controls; }

  getSetting() {
    this.getSocialSettingSubscription = this.data.getSetting().subscribe((data: SocialSettingData) => {
      this.socialSettingForm.patchValue({
        facebook: data.facebook,
        pinterest: data.pinterest,
        twitter: data.twitter,
        instagram: data.instagram,
        linkdin: data.linkdin,
        tumblr: data.tumblr,
        flickr: data.flickr
      })
   });
   this.subscriptions.push(this.getSocialSettingSubscription);
  }


  onSubmitSocial() {
    this.submitted = true;
    this.loading = true;
    this.updateSocialSubscription = this.data.updatesocial(this.socialSettingForm.value).subscribe((data: any) => {
      this.socialSettingForm.patchValue({
        facebook: data.facebook,
        pinterest: data.pinterest,
        twitter: data.twitter,
        instagram: data.instagram,
        linkdin: data.linkdin,
        tumblr: data.tumblr,
        flickr: data.flickr
      })
      this.toastr.success('Updated successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
    });

    this.subscriptions.push(this.updateSocialSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}
