import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class SocialService {


  constructor(private http: HttpClient) { }

  getSetting() {
    return this.http.post<any>(`${environment.baseUrl}/Social/getsocial`, {})
    .pipe(map(setting => {
        return setting;
    }));
  }

  updatesocial(formData: SocialSettingData) {
    return this.http.post<any>(`${environment.baseUrl}/Social/updatesocial`,
    {
      'facebook': formData.facebook,
      'pinterest': formData.pinterest,
      'twitter': formData.twitter,
      'instagram': formData.instagram,
      'linkdin': formData.linkdin,
      'youtube': formData.youtube,
      'tumblr': formData.tumblr,
      'flickr': formData.flickr
    }).pipe(map(setting => {
            return setting;
      }));
  }
}


export class SocialSettingData {
  facebook?: string;
  pinterest?: string;
  twitter?: string;
  instagram?: string;
  linkdin?: string;
  youtube?: string;
  tumblr?: string;
  flickr?: string;
}
