export class Job {
  job_id: string;
  company_name: string;
  job_title: string;
  applied_user?: number;
  location: string[];
  formated_location?: string[];
  created_at: string;
  status: string;
}


export class User {
  user_id: string;
  user_name: string;
  email: string;
  location?: string[];
  state?: string;
  country?: string;
}

export class CompanyLists {
  company_id: string;
  company_name: string;
}
