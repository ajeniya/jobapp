import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatTableDataSource, MatTable, MatPaginator, MatSort, MatSortable } from '@angular/material';
import { EventServicesService } from 'src/app/services/event-services.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SelectionModel } from '@angular/cdk/collections';
import { Router } from '@angular/router';
import { AdminHomeService } from './admin-home.service';
import { Subscription } from 'rxjs/internal/Subscription';
import { Job, User, CompanyLists } from './admin-dashboard.model';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  navBar: void;
  subscriptions: Subscription[] = [];
  getJobSubscription: Subscription;
  getUserSubscription: Subscription;
  jobLists: Job[];
  companyList: CompanyLists[];

  getCompaniesSubscription: Subscription;
  searchForm: FormGroup;

  displayedColumns: string[] = ['select','title', 'company_name', 'location', 'date_created','status','action'];
  selection = new SelectionModel<Job>(true, []);

  dataSource: any;
  resultsLength: number;
  paginationMeta: any;
  submitted: boolean;
  dateValidationError: string;
  checkedBtn: boolean;

  constructor(private eventServices: EventServicesService, private formBuilder: FormBuilder,
    private toastr: ToastrService, private router: Router,
    private cdref: ChangeDetectorRef, public mockdata: AdminHomeService) { }

  ngOnInit() {
    this.navBar = this.eventServices.navchange.emit(true);
    this.createForm() ;
    this.getJobs();
    this.getCompaniesForDropdown();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  // convenience getter for easy access to form fields
  get f() { return this.searchForm.controls; }

  getJobs() {
    this.getJobSubscription = this.mockdata.getJob().subscribe((data) => {
      this.jobLists = data;
      this.dataSource = new MatTableDataSource<Job>(this.jobLists);
    });

    this.subscriptions.push(this.getJobSubscription);
  }

  getCompaniesForDropdown() {
    this.getCompaniesSubscription = this.mockdata.getCompanies().subscribe((data) => {
      this.companyList = data;
    });

    this.subscriptions.push(this.getCompaniesSubscription);
  }

  createForm() {
    this.searchForm = this.formBuilder.group({
      jobTitle: [''],
      company: [''],
      status: [''],
      discription: ['',],
      city: [''],
      dateFrom: [''],
      dateTo: ['']
    });
  }

  compareTwoDates(){
    if( this.searchForm.controls['dateTo'].touched && this.searchForm.controls['dateFrom'].touched) {
      if ( this.searchForm.controls['dateTo'].value > this.searchForm.controls['dateFrom'].value) {
        this.dateValidationError = 'isPassed';
      } else {
        this.toastr.error('You inputed date is invalid!', 'Date Error!', {
          enableHtml: true,
          closeButton: true,
          timeOut: 3000
      });
        this.dateValidationError = 'isError';
      }
    }
  }


  onSearch() {
    this.submitted = true;
    this.compareTwoDates();
    // display form values on success
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.searchForm.value, null, 4));
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    if (numSelected > 0) {
      this.checkedBtn = true;
    } else {
      this.checkedBtn = false;
    }
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Job): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.company_name + 1}`;
  }

  navigateTo(row: any) {
    this.router.navigate(['/admin/'+row.id]);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }


}
