import { Injectable } from '@angular/core';
import { Job, User, CompanyLists } from './admin-dashboard.model';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})
export class AdminHomeService {

  jobs: Job[] = [
    {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi', 'Abuja'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi', 'Abuja'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '1',
    'company_name': 'Prindex',
    'job_title': 'Business Manager',
    'location': ['Lagos', 'Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '2',
    'company_name': 'Akete Studio',
    'job_title': 'Devops',
    'location': ['Lagos', 'Kogi', 'Abuja'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '3',
    'company_name': 'IBM',
    'job_title': 'Developer Backend',
    'location': ['Lagos'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   },
   {
    'job_id': '4',
    'company_name': 'Prudent Bank',
    'job_title': 'Branch Manager',
    'location': ['Oyo'],
    'created_at': '1 Week Ago',
    'status': 'approved'
   }
  ];

  companyLists: CompanyLists[] = [
    {
    'company_id': '1',
    'company_name': 'Invitea'
   },
   {
    'company_id': '1',
    'company_name': 'Testfit'
   },
   {
    'company_id': '1',
    'company_name': 'Itemize'
   },
   {
    'company_id': '1',
    'company_name': 'Teampay'
   },
   {
    'company_id': '1',
    'company_name': 'PepsiCo'
   },
   {
    'company_id': '1',
    'company_name': 'Speedway'
   },
   {
    'company_id': '1',
    'company_name': 'Lyft'
   },
   {
    'company_id': '1',
    'company_name': 'Coney Island prep',
   },
   {
    'company_id': '1',
    'company_name': 'Aramark'
   }
  ];

  constructor() { }

  getJob() {
    return of(this.jobs);
  }

  getCompanies() {
    return of(this.companyLists);
  }
}
