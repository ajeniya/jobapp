import { ListPlanComponent } from './list-plan/list-plan.component';
import { ManageCountryComponent } from './manage-country/manage-country.component';
import { FlagedJobsComponent } from './flaged-jobs/flaged-jobs.component';
import { UnapprovedJobsComponent } from './unapproved-jobs/unapproved-jobs.component';
import { ListTestimonialsComponent } from './list-testimonials/list-testimonials.component';
import { AddTestimonialsComponent } from './add-testimonials/add-testimonials.component';
import { FaqListsComponent } from './faq-lists/faq-lists.component';
import { ListJobsComponent } from './list-jobs/list-jobs.component';
import { AddAdminUserComponent } from './add-admin-user/add-admin-user.component';
import { ListAdminUserComponent } from './list-admin-user/list-admin-user.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminHomeComponent } from './admin-home/admin-home.component';
import { TranslateModule } from '@ngx-translate/core';
import { EditAdminUserComponent } from './edit-admin-user/edit-admin-user.component';
import { ListCompanyComponent } from './list-company/list-company.component';
import { AddFaqComponent } from './add-faq/add-faq.component';
import { AddCountriesComponent } from './add-countries/add-countries.component';
import { AddStatesComponent } from './add-states/add-states.component';
import { ListStatesComponent } from './list-states/list-states.component';
import { ListCitiesComponent } from './list-cities/list-cities.component';
import { AddCitiesComponent } from './add-cities/add-cities.component';
import { SettingComponent } from './setting/setting.component';
import { BannedJobComponent } from './banned-job/banned-job.component';
import { ExpiredJobsComponent } from './expired-jobs/expired-jobs.component';
import { AddPlanComponent } from './add-plan/add-plan.component';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { adminRole } from '../_helpers/role';
import { AddJobComponent } from '../company/add-job/add-job.component';
import { CreateCompanyComponent } from '../company/create-company/create-company.component';

const routes: Routes = [
  { path: '', component: AdminHomeComponent },
  { path: 'admin', component: AdminHomeComponent },
  { path: 'admin-user', component: ListAdminUserComponent },
  { path: 'add-admin', component: AddAdminUserComponent },
  { path: 'edit-admin-user', component: EditAdminUserComponent },
  { path: 'jobs-list', component: ListJobsComponent },
  { path: 'add-job', component: AddJobComponent },
  { path: 'companies-list', component:  ListCompanyComponent},
  { path: 'add-company', component: CreateCompanyComponent },
  { path: 'faq-list', component:  FaqListsComponent},
  { path: 'add-faq', component:  AddFaqComponent},
  { path: 'add-testimonies', component: AddTestimonialsComponent},
  { path: 'list-testimonies', component: ListTestimonialsComponent},
  { path: 'add-countries', component: AddCountriesComponent},
  { path: 'manage-country', component: ManageCountryComponent},
  { path: 'add-states', component: AddStatesComponent},
  { path: 'list-states', component: ListStatesComponent},
  { path: 'list-cities', component: ListCitiesComponent},
  { path: 'add-cities', component: AddCitiesComponent},
  { path: 'plan-list', component: ListPlanComponent},
  { path: 'add-plan', component: AddPlanComponent},
  { path: 'unapproved-jobs', component: UnapprovedJobsComponent},
  { path: 'banned-jobs', component: BannedJobComponent},
  { path: 'expired-jobs', component: ExpiredJobsComponent},
  { path: 'flaged-jobs', component: FlagedJobsComponent},
  { path: 'settings', component: SettingComponent},
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    TranslateModule
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
