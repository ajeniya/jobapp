import { AddPlanService } from './add-plan.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventServicesService } from 'src/app/services/event-services.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import { MainSettingService } from '../setting/main-setting/main-setting.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs/internal/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-plan',
  templateUrl: './add-plan.component.html',
  styleUrls: ['./add-plan.component.scss']
})
export class AddPlanComponent implements OnInit, OnDestroy {
  subscriptions: Subscription[] = [];
  addPlanSubscription: Subscription;
  navBar: void;
  addPlanForm: FormGroup;
  submitted = false;
  public tools: object = {
    items: [
        'Bold', 'Italic', '|', 'OrderedList', 'UnorderedList', '|',
        'LowerCase', 'UpperCase', 'CreateLink', '|', 'Undo', 'Redo',]
  };
  currencySign: string;
  formattedAmount: any;
  loading: boolean;

  constructor(private eventServices: EventServicesService,
    private mainsetting: MainSettingService,
    private currencyPipe : CurrencyPipe,
    private toastr: ToastrService,
    private router: Router,
    private data: AddPlanService,
    private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.navBar = this.eventServices.navchange.emit(true);
    this.loadForm();
    this.getAdminMainSetting();
  }

  getAdminMainSetting() {
    this.mainsetting.getSetting().subscribe((data) => {
      this.currencySign = data.defaultCurrency;
    })
  }

  loadForm() {
    this.addPlanForm  =  this.formBuilder.group({
      plan_name: ['', Validators.required],
      plan_expired: ['', Validators.required],
      plan_limit: ['', Validators.required],
      job_expired: ['', Validators.required],
      featured: ['', Validators.required],
      urgent: ['', Validators.required],
      highlight: ['', Validators.required],
      on_top_of_search: ['', Validators.required],
      on_home_page: ['', Validators.required],
      amount: ['', Validators.required],
      amount_number: ['', Validators.required],
      activate: ['', Validators.required],
      isfree: ['', Validators.required],
      recommended: [''],
    });
  }

  get f() { return this.addPlanForm.controls; }

  addPlans() {
    this.submitted = true;
    const value = this.addPlanForm.value;
    this.loading = true;
    this.addPlanSubscription = this.data.addPlan(value).subscribe((data: any) => {
      this.toastr.success('Added successful!', 'Success!', {
        enableHtml: true,
        closeButton: true,
        timeOut: 3000
      });
      this.loading = false;
      this.router.navigate(['/admin/plan-list']);
    });

    this.subscriptions.push(this.addPlanSubscription);
  }

  transformAmountMin(){
    const inputedValue = this.f.amount.value;
    const removeText = inputedValue.replace(/[^0-9\.]+/g, "");
    this.formattedAmount = this.f.amount.value;
    this.formattedAmount = this.currencyPipe.transform(removeText, this.currencySign);
    this.addPlanForm.controls['amount'].setValue(this.formattedAmount);
    this.addPlanForm.controls['amount_number'].setValue(removeText);
  }


  ngOnDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe());
  }

}

