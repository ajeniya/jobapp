import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class AddPlanService {

  constructor(private http: HttpClient) { }

  addPlan(formData){
    return this.http.post<any>(`${environment.baseUrl}/Plan/addplan`,
    {
      'activate': formData.activate,
      'amount': formData.amount,
      'amount_number': formData.amount_number,
      'featured': formData.featured,
      'highlight': formData.highlight,
      'job_expired': formData.job_expired,
      'on_home_page': formData.on_home_page,
      'on_top_of_search': formData.on_top_of_search,
      'plan_expired': formData.plan_expired,
      'plan_limit': formData.plan_limit,
      'plan_name': formData.plan_name,
      'recommended': formData.recommended,
      'urgent': formData.urgent,
      'isfree': formData.isfree

    }).pipe(map(result => {
            return result;
      }));
  }
}
