import { browser } from 'protractor';
import { Component, OnInit, Inject, PLATFORM_ID, Optional } from '@angular/core';
import { EventServicesService } from './services/event-services.service';
import { TranslateService } from '@ngx-translate/core';
import { SpinnerService } from './services/spinnerService';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  adminNav: any;
  showSpinner: boolean;

  constructor(private eventServices: EventServicesService,
    public spinnerService: SpinnerService,
    public translate: TranslateService) {
    this.eventServices.isAdmin.subscribe((data) => {
      this.adminNav = data;
    });
    translate.addLangs(['en', 'nl']);
    translate.setDefaultLang('en');

  }

  switchLang(lang: string) {
    this.translate.use(lang);
  }

}
