import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { PagenotfoundComponent } from './ui/pagenotfound/pagenotfound.component';
import { SignUpComponent } from './ui/sign-up/sign-up.component';
import { SignInComponent } from './ui/sign-in/sign-in.component';
import { ProfileComponent } from './ui/profile/profile.component';
import { HomeComponent } from './ui/home/home.component';
import { ActivateEmailComponent } from './ui/activate-email/activate-email.component';
import { ForgotPasswordComponent } from './ui/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './ui/reset-password/reset-password.component';
import { ConfirmEmailComponent } from './ui/confirm-email/confirm-email.component';
import { AuthGuard } from './_helpers/auth.guard';
import { CompanyGuard } from './_helpers/company.guard';
import { AdminGuard } from './_helpers/admin.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'sign-up', component: SignUpComponent },
  { path: 'sign-in', component: SignInComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'confirm-email', component: ConfirmEmailComponent
  },
  { path: 'home', component: HomeComponent, },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'reset-password', component: ResetPasswordComponent },
  {
    path: 'user',
    canActivate: [AuthGuard],
    loadChildren: () => import('../app/user/user.module').then(m => m.UserModule)
  },
  {
    path: 'job',
    loadChildren: () => import('../app/job/job.module').then(m => m.JobModule)
  },
  {
    path: 'article',
    loadChildren: () => import('../app/article/article.module').then(m => m.ArticleModule)
  },
  {
    path: 'company',
    loadChildren: () => import('../app/company/company.module').then(m => m.CompanyModule)
  },
  {
    path: 'admin',
    canActivate: [AdminGuard],
    loadChildren: () => import('../app/admin/admin.module').then(m => m.AdminModule)
  },
  { path: 'activate', component: ActivateEmailComponent },
  { path: '**', component: SignInComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    { preloadingStrategy: PreloadAllModules, scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
